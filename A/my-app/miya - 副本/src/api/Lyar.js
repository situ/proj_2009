import request from "../utils/axios";

// 渲染
export const getShoops = (params) => {
    return request({
        url: "/Gshoops/getShoops",
        methods: "get",
        params
    })
}