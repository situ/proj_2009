import request from "../utils/axios";

// 渲染类别
export const getClassify = (params) => {
    return request({
        url: "/classify/list",
        methods: "get",
        params
    })
}

// 渲染类别 二级
export const getContent = (params) => {
    return request({
        url: "/content/list",
        methods: "get",
        params
    })
}

// 渲染类别 三级
export const getList = (id) => {
    return request({
        url: "/Gthreelevel/getThreelevel",
        methods: "get",
        params: {
            id
        }
    })
}

