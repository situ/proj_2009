import request from "../utils/axios"

// 抢购数据获取
export let getRules = (params) => {
    return request({
        method: "get",
        url: "rules/list",
        params
    })
}

// 通过id集合获取数据
export let getShoop = (params) => {
    return request({
        method: "get",
        url: "Gshoops/accessGoods",
        params
    })
}


// 获取推荐商品
export let recommendShp = (params) => {
    return request({
        method: "get",
        url: "Gshoops/getRecommend",
        params
    })
}


// // 用token 获取数据
// export let getinfo = (token) => {
//     return request({
//         method: "get",
//         url: "appLogin/takeinfo",
//         params: {
//             token
//         }
//     })
// }