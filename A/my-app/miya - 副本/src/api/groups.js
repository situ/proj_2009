import request from "../utils/axios"

// 获取推荐拼团数据
export let getgroups = (data) => {
    return request({
        method: "get",
        url: "Gshoops/getgroups",
        data
    })
}


// 新增拼团
export let addgroups = (data) => {
    return request({
        method: "post",
        url: "group/addGroup",
        data
    })
}

// 修改拼团
export let updategroups = (data) => {
    return request({
        method: "post",
        url: "group/updateGroup",
        data
    })
}

// 获取当前商品拼团数据
export let getmass = (params) => {
    return request({
        method: "get",
        url: "group/getmass",
        params
    })
}

