import request from "../utils/axios"

// 登录和注册
export let login = (data) => {
    return request({
        method: "post",
        url: "appLogin/applogin",
        data
    })
}


// 用token 获取数据
export let getinfo = (token) => {
    return request({
        method: "get",
        url: "appLogin/takeinfo",
        params: {
            token
        }
    })
}


// 用uid  获取头像 name
export let getName = (id) => {
    return request({
        method: "get",
        url: "appLogin/getinfo",
        params: {
            id
        }
    })
}