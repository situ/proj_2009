import request from "../utils/axios";

// 获取
// export const getDate = (params) => {
//     return request({
//         url : "/site/getSiteList",
//         methods : "get",
//         params
//     })
// }

// 新增 修改
export const setDate = (data) => {
    return request({
        url : "/site/addSite",
        method : "post",
        data
    })
}

// 删除
export const deleteDate = (id) => {
    return request({
        url : "/site/deleteSite",
        methods : "get",
        params : {
            id
        }
    })
}

// 修改默认地址
export const updateSite = (data) => {
    return request({
        url : "/site/updateSite",
        method : "post",
        data
    })
}

// uid
export const updateSiteUid = (uid) => {
    return request({
        url : "/site/getSite",
        methods : "get",
        params : {
            uid
        }
    })
}

// 获取默认地址
export const mrSite = (params) => {
    return request({
        url: "/site/getSite",
        method: "get",
        params
    })
}