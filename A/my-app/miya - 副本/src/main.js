import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import "./assets/css/all.css"

import Vant from 'vant';
import 'vant/lib/index.css';

import "./utils/rem"

Vue.use(Vant);

// 全局注册组件
const files = require.context('./components', true, /\.vue$/)
files.keys().forEach(key => {
  let componentConfig = files(key).default
  const name = files(key).default.name
  // 全局注册组件
  Vue.component(
    name,
    componentConfig
  )
})


// import VueSocketio from 'vue-socket.io';

// import socketio from 'socket.io-client';


// Vue.use(new VueSocketio({
//   debug: true,
//   connection: socketio('http://localhost:3001'),
// }))



Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
