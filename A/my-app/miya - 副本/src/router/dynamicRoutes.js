
import layout from "../components/layout"

let content = require.context('../views', false, /.vue$/)

let routesArr = []

content.keys().forEach(v => {
    let item = content(v).default
    let obj = {}
    if (item.name) {
        if (item.layout) {
            obj.component = layout
            obj.path = `/${item.name}`
            obj.children = [{
                component: item,
                name: item.name,
                path: `/${item.name}`
            }]
        } else {
            obj.component = item
            obj.name = item.name
            obj.path = `/${item.name}`
        }
    } else {
        console.warn("name属性必填")
    }


    routesArr.push(obj)
});

export default routesArr
