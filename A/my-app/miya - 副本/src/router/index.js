import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import routesArr from "./dynamicRoutes"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    redirect: "/home"
  },
  ...routesArr,
  {
    path: "*",
    redirect: '/found'
  }
]



const router = new VueRouter({
  routes,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})



router.beforeEach((to, from, next) => {
  next()
})

export default router
