import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)



// vuex  模块自动引入
const path = require('path')
const files = require.context('./module', false, /\.js$/)
const modules = {}
files.keys().forEach(key => {
  const name = path.basename(key, '.js')    // 回key的最后一部分
  modules[name] = files(key).default || files(key)
})



export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules
})
