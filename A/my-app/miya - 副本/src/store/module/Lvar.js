import { getShoops } from "../../api/Lyar";

export default {
    namespaced: true,
    state: {
        shoops : []
    },
    mutations: {
        getShoops(state,data){
            state.shoops = data
        }
    },
    actions: {
        async getShoops({commit}) {
           let r =  await getShoops()
           commit("getShoops",r.data)
        }
    }
}