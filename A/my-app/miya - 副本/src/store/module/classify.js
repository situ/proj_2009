import { getClassify , getContent ,getList } from "@/api/classify";

export default {
    namespaced: true,
    state: {
        classifyArr : [],
        contentArr : [],
        listArr : []
    },
    mutations: {
        getClassify(state,data) {
            state.classifyArr = data
        },
        getContennt(state,data) {
            state.contentArr = data
        },
        getList(state,data) {
            state.listArr = data
        }
    },
    actions: {
        // 渲染
        async getClassify({commit}) {
           let r =  await getClassify()
           commit("getClassify",r.data)
        },
        // 渲染二级
        async getContent({commit}) {
           let r =  await getContent()
           commit("getContennt",r.data)
           commit("getList",r.classify)
           console.log(r);
        },
        // 渲染三级
        async getList({commit},{id}) {
            console.log(id);
            let r =  await getList(id)
            return r
         },
    }
}