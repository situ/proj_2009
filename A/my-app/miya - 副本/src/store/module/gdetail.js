import { getRules, getShoop } from "../../api/rules"
import { recommendShp } from "../../api/detail"
export default {
    namespaced: true,
    state: {

    },
    mutations: {

    },
    actions: {
        // 获取商品
        async getShoop({ commit }, res) {
            let r = await getShoop(res)
            console.log(r.data)
            return r.data
        },
        // 猜你喜欢
        async recommendShp({ commit }, res) {
            let r = await recommendShp()
            return r.data
        }
    }
}