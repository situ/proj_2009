import { getgroups, addgroups, getmass, updategroups } from "../../api/groups"
import { getName } from "../../api/login"
export default {
    namespaced: true,
    state: {
        groupsArr: []
    },
    mutations: {
        SET_GROUPSARR(state, res) {
            state.groupsArr = res
        }
    },
    actions: {
        // 新增拼团
        async addgroups({ commit }, res) {
            await addgroups(res)
        },
        // 获取商品拼团
        async getmass({ commit }, res) {
            let r = await getmass(res)
            commit("SET_GROUPSARR", r.data)
            return r.data
        },
        // 获取团长信息
        async getName({ commit }, res) {
            let r = await getName(res)
            console.log(r)
        },
        // 修改拼团
        async updategroups({ commit }, res) {
            await updategroups(res)
        }
    }
}