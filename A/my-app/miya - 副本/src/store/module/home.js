import { getRules, getShoop } from "../../api/rules"
import { getgroups } from "../../api/groups"
export default {
    namespaced: true,
    state: {
        rulesArr: []
    },
    mutations: {
        SET_RULESARR(state, res) {
            state.rulesArr = res
        }
    },
    actions: {
        // 获取抢购商品
        async getRules({ commit }, res) {
            let r = await getRules()
            commit("SET_RULESARR", r.data)
        },
        // 获取商品信息
        async getShoop({ commit }, res) {
            let r = await getShoop(res)
            return r.data
        },
        // 获取拼团商品
        async getgroups() {
            let r = await getgroups()
            return r.data
        }
    }
}