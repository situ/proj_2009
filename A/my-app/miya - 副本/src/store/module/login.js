import { login, getinfo } from "../../api/login"
import { tokenCook } from "../../utils/cookies"
export default {
    namespaced: true,
    state: {
        name: "grr",
        info: {}
    },
    mutations: {
        SET_INFO(state, res) {
            state.info = res
        }
    },
    actions: {
        // 登录与注册
        async Applogin({ commit }, res) {
            let r = await login(res)
            if (r.success) {
                tokenCook.set(r.data)
            }
            return r.success
        },
        // 获取用户信息
        async getinfo({ commit }, res) {
            let r = await getinfo(res)
            commit("SET_INFO", r.data[0])
            return r.data[0]
        }
    }
}