import {getpicur} from '../../api/silder'
export default {
    namespaced: true,
    state: {
        name: "grr",
        picur:[]
    },
    mutations:{
        SET_PICUR(state,res){
            state.picur=res
        }
    },
    actions:{
        //图片
        async getpicurs({commit}){
            let r=await getpicur()
            commit('SET_PICUR',r.data)
        }
    },
}