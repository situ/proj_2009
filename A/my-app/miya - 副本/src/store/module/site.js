import { setDate, deleteDate, updateSite, updateSiteUid, mrSite } from "../../api/site";

export default {
    namespaced: true,
    state: {
        list: [],
        uid: ''
    },
    mutations: {
        getList(state, list) {
            state.list = list
        },
        sitUid(state, uid) {
            state.uid = uid
        }
    },
    actions: {
        // 渲染
        // async getList({ commit }) {
        //     const r = await getDate()
        //      commit("getList", r.data)
        // },
        // 新增
        async setList({ state, dispatch }, data) {
            const r = await setDate(data)
            await dispatch("updateSiteUid", state.uid)
        },
        // 删除 
        async deleteList({ state, dispatch }, id) {
            const r = await deleteDate(id)
            await dispatch("updateSiteUid", state.uid)
        },
        // 修改默认地址
        async updateSite({ state, dispatch }, data) {
            const r = await updateSite({
                oldId: data.oldId,
                newId: data.newId
            })
            await dispatch("updateSiteUid", state.uid)
        },
        // 获取
        async updateSiteUid({ dispatch, commit }, uid) {
            const r = await updateSiteUid(uid)
            commit("getList", r.data)
            commit("sitUid", uid)
            console.log(r.data);
        },
        // 获取默认地址
        async mrSite({ commit }, data) {
            let r = await mrSite(data)
            return r.data[0]
        }

    }
}