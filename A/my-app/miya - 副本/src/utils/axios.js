import axios from "axios"
import qs from "qs"




let instance = axios.create({
    baseURL: process.env.VUE_APP_BASE_API,
    headers: {
        'content-type': 'application/x-www-form-urlencoded'
    },
    timeout: 5000
})



instance.interceptors.request.use(
    config => {
        if (config.method == 'post') {
            config.data = qs.stringify({ ...config.data })
            console.log(config)
        }
        return config
    },
    err => {
        return Promise.reject(err)
    })



instance.interceptors.response.use(
    response => {
        return response.data
    },
    //接口错误状态处理，也就是说无响应时的处理
    error => {
        return Promise.reject(error) // 返回接口返回的错误信息
    })

// instance({
//     url: "Gthreelevel/getThreelevel",
//     method: "get",
//     params: {
//         id: 1
//     }
// }).then(res => {
//     console.log(res)
// })

export default instance