import Cookies from "js-cookie"



// token 
const TOKEN = 'token'
export let tokenCook = {
    set(data) {
        Cookies.set(TOKEN, data)
    },
    get() {
        return Cookies.get(TOKEN)
    },
    remove() {
        Cookies.remove(TOKEN)
    }
}