//  购物车列表本地存储
const GW = 'shoopcar'

export let gwlocal = {
    set(data) {
        localStorage.setItem(GW, JSON.stringify(data))
    },
    get() {
        return JSON.parse(localStorage.getItem(GW) || '[]')
    }
}


// 结算存储
const JZ = 'jiesuan'
export let jzlocal = {
    set(data) {
        localStorage.setItem(JZ, JSON.stringify(data))
    },
    get() {
        return JSON.parse(localStorage.getItem(JZ) || '[]')
    }
}