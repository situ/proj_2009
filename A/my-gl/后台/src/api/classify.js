import request from '@/utils/request'


//首次获取
export const getListFirst = (data) =>
  request({
    url: '/classify/firstlist',
    method: 'get',
    params: data
  })


//获取数据
export const getList = (data) =>
  request({
    url: '/classify/list',
    method: 'get',
    params: data
  })

//新增
export function addList(data) {
  request({
    url: '/classify/addList',
    method: 'post',
    data
  })
}

//修改
export function updateList(data) {
  request({
    url: '/classify/update',
    method: 'post',
    data
  })
}

//删除
export function deleteList(id) {
  request({
    url: '/classify/delete',
    method: 'get',
    params:{id}
  })
}
