import request from '@/utils/request'

  //首次获取数据
export const getListfirst = (data) =>
request({
  url: '/content/listfirst',
  method: 'get',
  params: data
})

//获取数据
export const getList = (data) =>
  request({
    url: '/content/list',
    method: 'get',
    params: data
  })

//新增数据
export const addList = (data) =>
  request({
    url: '/content/addList',
    method: 'post',
    data
  })

//修改数据
export const updateList = (data) =>
  request({
    url: '/content/update',
    method: 'post',
    data
  })

  //商品集合修改数据

  export function updateListShoop(data) {
    return request({
      url: '/content/updateShoop',
      method: 'post',
      data
    })
}



// export const updateListShoop = (data) =>
// // console.log(data);
// request({
//   url: '/content/updateShoop',
//   method: 'post',
//   data: qs.stringify(data)
// })

//删除数据
export const deleteList = (id) =>
  request({
    url: '/content/delete',
    method: 'get',
    params: {id}
  })
