import request from '@/utils/request'

//获取商品数据
export const getList = (data) =>
  request({
    url: 'Gshoops/accessGoods',
    method: 'get',
    params: data
  })

//新增商品
export function addList(data) {
  return request({
    url: 'Gshoops/newGoods',
    method: 'post',
    data
  })
}

//删除
export const delList = (data) =>
  request({
    url: 'Gshoops/removeProduct',
    method: 'get',
    params: {
      id: data
    }
  })