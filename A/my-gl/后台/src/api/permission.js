import request from '@/utils/request'


//获取数据
export const getper = (data) =>
    request({
        url: '/permission/getper',
        method: 'get',
        params: data
    })

// 新增角色
export const postRole = (data) =>
    request({
        url: '/permission/postRole',
        method: 'post',
        data
    })


// 获取角色权限getrole
export const getrole = (data) =>
    request({
        url: '/permission/getrole',
        method: 'get',
        params: data
    })

// 删除角色
export const removeRole = (id) =>
    request({
        url: '/permission/removeRole',
        method: 'get',
        params: {
            id
        }
    })