import request from '@/utils/request'
export function getList(params) {
    return request({
        url: '/rules/list',
        method: 'get',
        params
    })
}

// 新增
export function postRules(data) {
    return request({
        url: '/rules/addList',
        method: 'post',
        data
    })
}

// 修改
export function updateRules(data) {
    return request({
        url: '/rules/updateList',
        method: 'post',
        data
    })
}

// 删除
export function delRules(params) {
    return request({
        url: '/rules/delList',
        method: 'get',
        params
    })
}