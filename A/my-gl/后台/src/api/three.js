import request from '@/utils/request'


// 首次获取数据
export const getListfirst = (data) =>
  request({
    url: '/Gthreelevel/getThreelevelfirst',
    method: 'get',
    params: data
  })


// 获取数据
export const getList = (data) =>
  request({
    url: '/Gthreelevel/getAllshoops',
    method: 'get',
    params: data
  })

//新增商品
export function addList(data) {
  return request({
    url: 'Gthreelevel/newThreelevel',
    method: 'post',
    data
  })
}

//删除
export const delList = (data) =>
  request({
    url: 'Gthreelevel/removeThreelevel',
    method: 'get',
    params: {
      id: data
    }
  })
