import request from '@/utils/request'

//登录
export function login(data) {
    return request({
        url: '/glLogin/login',
        method: 'post',
        data
    })
}

export function getInfo(token) {
    return request({
        url: 'glLogin/info',
        method: 'get',
        params: { token }
    })
}
//退出
export function logout(data) {
    return request({
        url: 'glLogin/logout',
        method: 'post',
        data: {
            token: data
        }
    })
}