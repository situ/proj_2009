import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'


export const constantRoutes = [{
    path: '/login',
    component: () =>
        import('@/views/login/index'),
    hidden: true
},
{
    path: '/404',
    component: () =>
        import('@/views/404'),
    hidden: true
},
{
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
        path: 'dashboard',
        name: 'Dashboard',
        component: () =>
            import('@/views/dashboard/index'),
        meta: { title: '仪表盘', icon: 'dashboard' }
    }]
},


// 404 page must be placed at the end !!!
{ path: '*', redirect: '/404', hidden: true }
]



export const asyncRouter = [
    {
        path: '/rules',
        component: Layout,
        children: [{
            path: 'index',
            name: 'Rules',
            component: () =>
                import('@/views/rules/index'),
            meta: { title: '抢购管理', icon: 'el-icon-box' }
        }]
    },
    {
        path: '/details',
        component: Layout,
        children: [{
            path: 'index',
            name: 'Details',
            component: () =>
                import('@/views/details/index'),
            meta: { title: '商品管理', icon: 'form' }
        }]
    },
    {
        path: '/rank',
        component: Layout,
        name: 'Rank',
        meta: { title: '商品集合', icon: 'el-icon-s-help' },
        children: [{
            path: 'classify',
            name: 'Classify',
            component: () =>
                import('@/views/rank/classify'),
            meta: { title: '类别管理', icon: 'nested' }
        },
        {
            path: 'content',
            name: 'Content',
            component: () =>
                import('@/views/rank/content'),
            meta: { title: '商品列表', icon: 'nested' }
        }, {
            path: 'three',
            name: 'Three',
            component: () =>
                import('@/views/rank/three'),
            meta: { title: '三级分类', icon: 'nested' }
        }
        ]
    },
    {
        path: '/permission',
        component: Layout,
        children: [{
            path: 'index',
            name: 'Permission',
            component: () =>
                import('@/views/permission/index'),
            meta: { title: '权限管理', icon: 'el-icon-box' }
        }]
    },
]




const createRouter = () => new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router