import { getList, addList, updateList, deleteList, getListFirst } from '../../api/classify'

export default {
  namespaced: true,
  state: {
    clist: [],
    calist: [],
    total: 0,
    currentPage: 1,
    pageSize: 1,
    sort: 'id',
    sc: 'desc',
    tids: 1
  },
  mutations: {
    //总数据
    SET_LIST(state, res) {
      state.clist = res
    },
    SET_CALIST(state, res) {
      state.calist = res
    },
    //条数
    SET_TOTAL(state, res) {
      state.total = res
    },
    //当前页
    SET_CURRENT(state, res) {
      // console.log(res);
      state.currentPage = res
    },
    //每页几条
    SET_PAGESIZE(state, res) {
      // console.log(res);
      state.pageSize = res
    },

  },
  actions: {
    //首次获取
    async getListFirst({ commit,dispatch, state }) {
      let r = await getListFirst()
      commit('SET_LIST', r.data)

    },

    //获取数据
    async getListss({ commit, state }) {
      let r = await getList({
        currentPage: state.currentPage - 1,
        pageSize: state.pageSize,
        sort: state.sort,
        sc: state.sc
      });
      commit('SET_CALIST', r.classify)
      commit('SET_TOTAL', r.total[0])
      console.log(r);
    },
    //新增
    async addLists({ dispatch,commit }, res) {
      let r = await addList(res)
      console.log(res);
      dispatch('getListFirst')
      dispatch('getListss')
      console.log(res);
      // commit('SET_LIST',r.data)
    },
    //修改
    async updateLists({ dispatch }, res) {
      await updateList(res)
      dispatch('getListFirst')
      dispatch('getListss')
    },
    //删除
    async deleteLists({ dispatch }, id) {
      await deleteList(id)
      dispatch('getListFirst')
      dispatch('getListss')
    }
  }
}
