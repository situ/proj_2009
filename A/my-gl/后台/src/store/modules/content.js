import { getList, addList, updateList, deleteList,updateListShoop,getListfirst } from '../../api/content'

export default {
  namespaced: true,
  state: {
    cnlist: [],
    three:[],
    total: 0,
    currentPage: 1,
    pageSize: 1,
    sort: 'id',
    sc: 'desc',
  },
  mutations: {
    SET_LIST(state, res) {
      state.cnlist = res
    },
    //条数
    SET_TOTAL(state, res) {
      state.total = res
    },
     //当前页
     SET_CURRENT(state,res){
      // console.log(res);
      state.currentPage=res
    },
    //每页几条
    SET_PAGESIZE(state,res){
      // console.log(res);
      state.pageSize=res
    },
    SET_THREE(state,res){
      state.three=res
    }
  },
  actions: {
    
    //首次获取数据
    async getListsfirst({ commit, state }, res) {
      let r = await getListfirst();
      
      commit('SET_LIST',r.data)
    },
    //获取数据
    async getLists({ commit, state }, res) {
      let r = await getList({
        currentPage: state.currentPage-1,
        pageSize:  state.pageSize,
        sort: state.sort,
        sc: state.sc
      });
      commit('SET_TOTAL', r.total[0])
      commit('SET_THREE',r.classify)
    },
    //新增
    async addLists({ dispatch }, res) {
      await addList(res)
      dispatch('getListsfirst')
    },
    //修改
    async updateLists({ dispatch }, res) {
      console.log(res);
      await updateList(res)
      dispatch('getListsfirst')
    },
    //修改
    async deleteLists({ dispatch }, id) {
      await deleteList(id)
      dispatch('getListsfirst')
    },
    //商品集合
    async shoop ({dispatch},res){
      console.log();
      let cs=res.cnd.toString()
      await updateListShoop({id:res.id,cnd:cs})
      dispatch('getLists')
    }
  }
}
