import {
  getList
} from '../../api/dashboard'

export default {
  namespaced: true,
  state: {
    shoops: []
  },
  mutations: {
    SET_SHOOPS(state,data){
        state.shoops = data
    }

  },
  actions: {
    //获取数据
    async getList({
      commit,
      state
    }) {
      let r = await getList();
        commit('SET_SHOOPS', r.data)
    }
  }
}
