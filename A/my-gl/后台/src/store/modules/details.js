import {
  getList,
  addList,
  updList,
  delList
} from '../../api/details'

export default {
  namespaced: true,
  state: {
    dlist: [],
    arr: [1, 2]
  },
  mutations: {
    SET_LIST(state, data) {
      state.dlist = data
    }
  },
  actions: {
    //获取数据
    async getList({
      commit,
      state
    }, res) {
      let r = await getList(res);
      commit('SET_LIST', r.data)
      return r
    },
    //新增
    async addListFun({
      commit, dispatch
    }, data) {
      await addList(data);
      dispatch('getList')
    },
    //删除
    async delLists({ dispatch }, res) {
      delList(res)
      dispatch('getList')
    }
  }
}
