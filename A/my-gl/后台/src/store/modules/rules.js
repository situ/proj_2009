import { getList, postRules, updateRules, delRules } from '@/api/rules'
export default {
    namespaced: true,
    state: {
        rulesArr: []
    },
    mutations: {
        RUL_LIST(state, val) {
            state.rulesArr = val.data
        }
    },
    actions: {

        // 获取
        async getList({ commit }) {
            let r = await getList()
            commit('RUL_LIST', r)
        },
        // 新增
        async postRules({ dispatch, commit }, data) {
            if (data.id) {
                let r = await updateRules(data)
            } else {
                let r = await postRules(data)
            }
            dispatch("getList")
        },
        // 删除
        async delRules({ dispatch, commit }, data) {
            let r = await delRules(data)
            dispatch("getList")
        }
    }
}