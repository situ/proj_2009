import {
    getList,
    addList,
    delList,
    getListfirst
  } from '../../api/three'

  export default {
    namespaced: true,
    state: {
      tlist: [],
      cntitle:[]
    },
    mutations: {
      SET_LIST(state, data) {
        state.tlist = data
      },
      SET_LISTS(state,res){
        state.cntitle = res

      }
    },
    actions: {
      //获取首次数据
      
      async tgetListfirst({
        commit,
        state
      }, res) {
        let r = await getListfirst();
        commit('SET_LIST', r.data)
      },
      //获取数据
      async tgetList({
        commit,
        state
      }, res) {
        let r = await getList();
        console.log(r);
        commit('SET_LISTS',r.total)
        console.log(r);
      },
      //新增
      async addListFun({
        commit,
        dispatch
      }, data) {
        // console.log(data);
        
        // console.log(cd);
        await addList({
          title:data.title,
          imgurl:data.imgurl,
          id:data.id
        });
        dispatch('tgetListfirst')
      },
      //删除
      async delLists({
        dispatch
      }, res) {
        delList(res)
        dispatch('tgetListfirst')
      }
    }
  }
