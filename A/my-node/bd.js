var mysql = require('mysql');

// 创建 mysql 连接池资源
var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'miya',
    timezone: '08:00'
}); 

function dosql(sql, params=[]) {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            if (err) throw error
            //将链接返回到连接池中，准备由其他人重复使用
            connection.query(sql, params, (error, results) => {
                connection.release();
                if (error) {
                    reject(error)
                }
                resolve(results)
            })
        })
    })
}

function handerResult(data, total, classify) { //数据请求成功
    return JSON.stringify({
        data: data, //数据 
        success: true, // 是否请求成功
        message: "数据请求成功",
        errorCode: 0,
        // three:three,
        total: total, // 总条数 
        code: 20000,
        // three,
        classify,

    })
}

function handleResults(results) { //管理登录成功
    return JSON.stringify({
        success: true,
        errorCode: 0,
        code: 20000,
        msg: '数据请求成功',
        data: results
    })
}

function backText(data, total) { // 做分页,渲染成功时返回的数据
    return {
        data: data, //数据
        success: true, // 是否请求成功
        message: "数据请求成功",
        errorCode: 0,
        total: total, // 总条数
        code: 20000
    }
}

function postText(data) { // 新增成功返回的数据
    return {
        data: data,
        success: true,
        message: "数据增加成功",
        errorCode: 0,
        code: 20000,
    }
}

function error() { // 失败时返回的数据
    return {
        success: false,
        message: "数据请求失败",
        errorCode: 0,
        code: 404,
    }
}

//登录成功返回
function sfp(data){
    return {
        success: true,
        code: 20000,
        messeng: "成功",
        data: data
    }
}

function delText() { // 删除数据成功时返回的数据
    return {
        success: true,
        message: "数据删除成功",
        errorCode: 0,
        code: 20000,
    }           
}

function setText(data) { // 修改数据成功时返回的数据
    return {
        data: data,
        success: true,
        message: "数据修改成功",
        errorCode: 0,
        code: 20000,
    }
}

function postInFo(data) { // 默认渲染数据成功返回的数据
    return {
        data: data,
        success: true,
        message: "数据请求成功",
        errorCode: 0,
        code: 20000,
    }
}

function setPass() { // 修改密码成功时返回的数据
    return {
        success: true,
        message: "密码修改成功",
        errorCode: 0,
        code: 20000,
    }
}


exports.handerResult = handerResult
exports.backText = backText
exports.delText = delText
exports.setText = setText
exports.postInFo = postInFo
exports.postText = postText
exports.setPass = setPass
exports.error = error
exports.dosql = dosql
exports.doSQL = dosql
exports.sfp=sfp
exports.handleResults=handleResults