var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()


//examples 获取每日推荐商品
router.get("/getShoopsExamples", async function (req, res) {
    let sql = ` select * from shoops order by rand() limit 7 `
    const r = await db.doSQL(sql)
    console.log(r)
    res.send(db.handerResult(r))
})

// 获取数据
router.get("/getShoops", async function (req, res) {
    let sql = ` select * from  shoops order by id asc `
    console.log(sql);
    const r = await db.doSQL(sql)
    console.log(r)
    res.send(db.handerResult(r))
})

//通过三级id获取商品
router.get("/threeshoops", async (req, res) => {
    let {
        id
    } = req.query
    sql = `select * from shoops  where tid in (${id})`
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 获取商品列表
router.get("/accessGoods", async (req, res) => {
    let {
        ids
    } = req.query // 用id集合获取数据
    let sql = "select * from shoops order by id desc "
    if (ids) {
        sql = `select * from shoops  where id in (${ids})`
    }
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})  

// 获取推荐商品列表
router.get("/getRecommend", async (req, res) => {
    let {
        limit = 0
    } = req.query // 随机获取的条数
    sql = `select * from shoops  where recommend = 1 order by id desc limit ${limit*6},6`
    let r = await db.doSQL(sql)
    
    res.send(db.handerResult(r))
})

// 获取拼团商品列表
router.get("/getgroups", async (req, res) => {
    let {
        limit = 20
    } = req.query // 随机获取的条数
    sql = `select * from shoops  where recommend = 0 order by rand() limit ${limit}`
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 新增
router.post("/newGoods", urlencodedParser, async (req, res) => {
    let {
        id,
        title,
        info,
        newPrice,
        oldPrice,
        specification,
        inventory,
        imgurl,
        pattern,
        message,
        tid,
        cid,
        ImageCollection,
        isRush,
        isGroup,
        recommend,
        allNum
    } = req.body
    let sql = `INSERT INTO shoops (title, info, newPrice, oldPrice, specification, inventory, imgurl, pattern, message, tid, cid, ImageCollection, isRush, isGroup, recommend, allNum) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ?, ?,?, ?,?)`
    if (id) { // 传id 就修改
        sql = `UPDATE shoops SET title= ?, info =  ?, newPrice =  ?, oldPrice =  ?, specification =  ?, inventory =  ?, imgurl = ?, pattern = ?, message =  ?, tid =  ?, cid = ?, ImageCollection =  ?, isRush = ?, isGroup = ?, recommend = ?, allNum =?   WHERE id= ${id};`
    }
    let r = await db.doSQL(sql, [title, info, newPrice, oldPrice, specification, inventory, imgurl, pattern, message, tid, cid, ImageCollection, isRush, isGroup, recommend, allNum])
    res.send(db.handerResult(r))
})

//修改商品存货
router.post('/updateInventory', urlencodedParser, async function (req, res) {
    var {
        inventory,
        id
    } = req.body
    var sql = `update shoops set inventory='${inventory}' where id in (${id})`
    const r = await db.doSQL(sql);
    res.send(db.postText(r))
})

// 删除
router.get("/removeProduct", async (req, res) => {
    let {
        id
    } = req.query
    let sql = `DELETE FROM shoops  WHERE id in (${id})`
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})


module.exports = router;