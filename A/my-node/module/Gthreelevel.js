var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()

// 首次获取三级分类
router.get("/getThreelevelfirst", async (req, res) => {
    let sql = "select * from threelevel order by id desc"
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 获取三级分类
router.get("/getThreelevel", async (req, res) => {
    let { id } = req.query  // 根据id集合获取
    let sql = `select * from threelevel  where id in (${id})`
    let sql2 = `select * from content   `
    let r = await db.doSQL(sql)
    let r1 = await db.doSQL(sql2)
    // console.log(r1);
    res.send(db.handerResult(r, r1))
})


// 获取三级分类所有数据
router.get("/getAllshoops", async (req, res) => {
    let sql = "select * from threelevel"
    let sql2 = `select c.id,c.title from content as c   `
    let r = await db.doSQL(sql)
    let r1 = await db.doSQL(sql2)
    // console.log(r1,27);
    res.send(db.handerResult(r,r1))
})


// 新增三级分类
router.post("/newThreelevel", urlencodedParser, async (req, res) => {
    let { id, title, imgurl } = req.body
    let sql = `INSERT INTO threelevel (title,imgurl) VALUES ( ?, ? )`
    // sql2 = ` insert into content  (tids ) values ('${id}') where id in ('${cnd}')`

    if (id) {  // 传id 就修改
        sql = `UPDATE threelevel SET title= ? , imgurl= ?  WHERE id= ${id};`
    }
    let r = await db.doSQL(sql, [title, imgurl])
    res.send(db.handerResult(r))
})


// 删除
router.get("/removeThreelevel", async (req, res) => {
    let { id } = req.query
    let sql = `DELETE FROM threelevel  WHERE id in (${id})`
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})


module.exports = router;