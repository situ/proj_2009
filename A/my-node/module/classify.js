var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()

// 首次--获取
router.get('/firstlist', async function (req, res) {
    let sql = `select * from classify  order by id desc `
    const r=await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 左侧类别--获取
router.get('/list', async function (req, res) {
    let {currentPage,pageSize,sort,sc}=req.query
    
    if(!currentPage){
        currentPage=0
    }
    if(!pageSize){
        pageSize=2
    }
    let sql = `select * from classify  order by id asc `
    let sql1=` select count(*) as total  from classify `
    let sql2 = `select * from classify  order by id asc `
    let num=currentPage *pageSize
    // console.log(num,23);
    if(sc){
        sql+=` limit ${num},${pageSize}`
    }
    const r=await db.doSQL(sql)
    const r1=await db.doSQL(sql1)
    const r2=await db.doSQL(sql2)
// console.log(r1,15);
    res.send(db.handerResult(r,r1,r2))
})

//左侧类别--新增
router.post('/addList',urlencodedParser, async function (req, res) {
    let {classify,type}=req.body
    // console.log(classify,type,20);
    var sql = `insert into classify (classify,type) values ('${classify}','${type}')`
    const r = await db.doSQL(sql)
    res.send(db.postText(r))
})

//左侧类别--修改
router.post('/update',urlencodedParser,async function(req,res){
    let {id,classify,type}=req.body
    let sql=` update classify set classify='${classify}', type="${type}" where id='${id}'`
    let r=await db.doSQL(sql)
    res.send(db.setText(r))
})

//左侧类别--删除
router.get('/delete', async function (req, res) {
    let {id}=req.query
    // console.log(id);
    let sql=` delete from classify where id in (${id})`
    let r=await db.doSQL(sql)
    res.send(db.delText(r))
})

// 获取内容
router.get('/content', async function (req, res) {
    // var sql = `select cn.id, cn.gatger, cn.cid, c.id, from content as cn ,classify as c where cn.cid=c.id  order by id asc`
    var sql = `select cn.id, cn.gatjer, cn.cid, c.id from content as cn ,classify as c  where cn.cid=c.id`
    const r=await db.doSQL(sql)
    // console.log(r,20);
    res.send(db.handerResult(r))
})

module.exports = router;