var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()

//二级--首次获取
router.get('/listfirst', async function (req, res) {
    let sql = `select cn.tids,cn.id,cn.title,cn.cid,c.id as claid,c.classify  from content as cn,classify as c where cn.cid=c.id order by cn.id desc  `
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})


//二级--获取
router.get('/list', async function (req, res) {
    let {currentPage, pageSize, sort, sc } = req.query
    if (!currentPage) {
        currentPage = 0
    }
    if (!pageSize) {
        pageSize = 2
    }
    let sql = `select cn.tids,cn.id,cn.title,cn.cid,c.id as claid,c.classify  from content as cn,classify as c where cn.cid=c.id order by cn.id asc  `
    let sql2 = ` select count(*) as total  from content `
    let sql3=` select * from threelevel`
    let num = currentPage * pageSize
    if (sc) {
        sql += ` limit ${num},${pageSize}`
    }
    let r = await db.doSQL(sql)
    let r2 = await db.doSQL(sql2)
    let r3 = await db.doSQL(sql3)
    res.send(db.handerResult(r, r2,r3))
})

//二级--新增
router.post('/addList', urlencodedParser, async function (req, res) {
    let { title, classify, tids=1, cid } = req.body
    let sql = ` insert into content  (tids,cid,title) values ('${tids}','${cid}','${title}')`
    let r = await db.doSQL(sql)
    res.send(db.postText(r))
})

//二级--修改
router.post('/update', urlencodedParser, async function (req, res) {
    let { id, tids, cid, title, } = req.body
    let sql = ` update content set tids='${tids}',cid='${cid}',title='${title}' where id='${id}' `
    let r = await db.doSQL(sql)
    res.send(db.setText(r))
})

//二级--删除
router.get('/delete', async function (req, res) {
    let { id } = req.query
    let sql = ` delete from content where id in (${id})`
    let r = await db.doSQL(sql)
    res.send(db.delText(r))
})

//商品集合
router.post('/updateShoop', urlencodedParser, async function (req, res) {
    let { id, cnd } = req.body
    console.log(id,cnd);
    let sql = ` update content set tids='${cnd}' where id='${id}' `
    // console.log(sql, 36);
    let r = await db.doSQL(sql)
    res.send(db.setText(r))
})


module.exports = router;