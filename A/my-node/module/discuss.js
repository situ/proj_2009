var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()

// 获取数据
router.get('/list', async function (req, res) {
    var sql = `select * from discuss order by id asc `
    const r=await db.doSQL(sql)
    // console.log(r,13);
    res.send(db.handerResult(r))
})

//获取评论数据
router.get('/getDiscuss', async function (req, res) {
    var sql = `select d.id,d.content,u.avatar,u.name,d.picur from discuss as d , user as u where d.uid = u.id `
    const r=await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 新增
router.post('/add', urlencodedParser, async function (req, res) {
	let {
        id,
		content,
		uid,
		sid,
		picture
	} = req.body
	let sql = `insert into discuss (content, uid, sid, picture) values ('${content}', '${uid}', '${sid}', ${picture})`
	const r=await db.doSQL(sql)
	res.send(db.handerResult(r))
})

module.exports = router;