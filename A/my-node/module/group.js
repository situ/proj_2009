var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()

// 用商品id 获取团长
router.get("/getmass", async (req, res) => {
    let { id, pid } = req.query
    let sql = ` select * , g.id as gid from  groups as g , appuser as a where sid = ${id} and a.id = g.regimental`
    if (pid) {
        sql += `   and g.id = ${pid} `
    }
    const r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 获取团长
router.get("/getGroup", async function (req, res) {
    let sql = ` select * from  groups order by id asc `
    const r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 新增
router.post("/addGroup", urlencodedParser, async function (req, res) {
    let {
        sid,
        uids,
        regimental
    } = req.body
    let sql = `insert into groups (sid , uids , regimental) values ("${sid}", "${uids}" , "${regimental}")`
    const r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 修改
router.post("/updateGroup", urlencodedParser, async function (req, res) {
    let {
        id,
        sid,
        uids,
        regimental
    } = req.body
    let sql = `update groups set sid='${sid}',uids='${uids}',regimental='${regimental}' where id='${id}'`
    const r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 删除
router.get("/deleteGroup", async function (req, res) {
    let { id } = req.query
    let sql = `delete from groups where id in (${id})`
    const r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})


module.exports = router