var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()

// 获取所有权限
router.get("/getper", async (req, res) => {
    let sql = `select * from  roles`
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 通过角色获取权限
router.get("/getrole", async (req, res) => {
    let { roles } = req.query
    let sql = `select * from  roles where role = '${roles}'`
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))
})

// 新增  修改角色
router.post("/postRole", urlencodedParser, async (req, res) => {
    let { id, role, title, items } = req.body
    let sql = `INSERT INTO roles (role,title,items) VALUES ('${role}','${title}','${items}') `
    if (id) {
        sql = `UPDATE roles SET role='${role}', title='${title}', items='${items}' WHERE id=${id} `
    }
    console.log(sql)
    let r = await db.doSQL(sql)
    res.send(db.handerResult(r))

})

// 删除角色
router.get("/removeRole", async (req, res) => {
    let {
        id
    } = req.query
    var sql = `delete from roles where id in (${id})`
    const r = await db.doSQL(sql);
    res.send(db.delText(r))
})



module.exports = router