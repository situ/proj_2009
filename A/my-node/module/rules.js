var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()

//获取数据
router.get('/list', async function (req, res) {
 
    var sql = `select * from rules  order by time desc`
    const r = await db.doSQL(sql)
    // console.log(r, 13);
    res.send(db.handerResult(r))
})


//新增数据
router.post('/addList', urlencodedParser, async function (req, res) {
    var {
        time,
        sids
    } = req.body;
    var sql = `insert into rules (time, sids) values ('${time}','${sids}')`
    const r = await db.doSQL(sql)
    // console.log(r, 13);
    res.send(db.postText(r))
})

//修改数据
router.post('/updateList', urlencodedParser, async function (req, res) {
    var {
        id,
        time,
        sids
    } = req.body
    var sql = `update rules set time='${time}',sids='${sids}' where id='${id}'`
    const {
        r
    } = await db.doSQL(sql)
    res.send(db.setText(r))
})


// 删除数据
router.get('/delList', async function (req, res) {
    var ids = req.query.id;
    var sql = `delete from rules where id in (${ids})`;
    const {
        r
    } = await db.doSQL(sql)
    res.send(db.delText(r))
})

module.exports = router;