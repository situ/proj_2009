var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()

const multer = require('multer')
var upload = multer({ dest: 'uploads/' })

//配置diskStorage来控制文件存储的位置以及文件名字等
var storage = multer.diskStorage({
    //确定图片存储的位置
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    //确定图片存储时的名字,注意，如果使用原名，可能会造成再次上传同一张图片的时候的冲突
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
});

//生成的专门处理上传的一个工具，可以传入storage、limits等配置
var upload = multer({ storage: storage });
//接收上传图片请求的接口
router.post('/upload', upload.single('file'), function (req, res, next) {
    //图片已经被放入到服务器里,且req也已经被upload中间件给处理好了（加上了file等信息）
    //线上的也就是服务器中的图片的绝对地址
    var url = 'http://localhost:2000/uploads/' + req.file.filename
    res.json({
        code: 200,
        data: url
    })
});




module.exports = router