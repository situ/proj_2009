var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../bd.js')
var router = express.Router()

// 获取用户信息
router.get('/getUserinfo', async function (req, res) {
    let {
        loginName,
        loginKey
    } = req.query
    var sql = `select * from user where loginName='${loginName}' and loginKey='${loginKey}' `
    const r = await db.doSQL(sql)
    res.send(db.postInFo(r))
})

//删除用户
router.get('/deleteUser', async function (req, res) {
    let {
        id
    } = req.query
    var sql = `delete from user where id in (${id}) `
    const r = await db.doSQL(sql);
    res.send(db.delText(r))
})

// 新增用户
router.post('/addUser', urlencodedParser, async function (req, res) {
    let {
        name,
        loginName,
        loginKey,
        roles,
        avatar
    } = req.body
    var sql = `insert into user (name,loginName,loginKey,roles,avatar) values ('${name}','${loginName}','${loginKey}','${roles}','${avatar}') `
    const r = await db.doSQL(sql);
    res.send(db.postText(r))
})

module.exports = router;