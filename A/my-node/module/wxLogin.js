var express = require('express'); //引入框架

var router = express.Router();


var https = require('https')

router.get('/wx-login', function (req, res) {
    const { code } = req.query
    let url = 'https://api.weixin.qq.com/sns/jscode2session?appid=wxa185c37b1d4af1e1&secret=3f1b15828d28d128f7b7466b36ec60f9&js_code=' + code + '&grant_type=authorization_code'
    let r = https.request(url, function (res2) {
        res2.setEncoding('utf-8');
        res2.on('data', function (chunk) {
            var data = JSON.parse(chunk);
            res.send(data)
        })
    })
    r.end()
})

module.exports = router;