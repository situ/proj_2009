var express = require('express');
var app = express()
let group = require('./module/group')
// 商品shoops
let Gshoops = require('./module/Gshoops')
// 三级分类
let Gthreelevel = require('./module/Gthreelevel')
//  评论
let discuss = require('./module/discuss')
//  rules
let rules = require('./module/rules')
//一级--类别
let classify = require('./module/classify')
//二级--内容
let content = require('./module/content')
//用户
let user = require('./module/user');
//收货地址
let site = require('./module/site');
//微信登录
let wxLogin = require('./module/wxLogin');
// 手机app登录
let appLogin = require("./module/appLogin")
// 首页图片轮播获取
let slider = require("./module/slider")
// 管理系统登录退出
let glLogin = require("./module/glLogin")
// 权限
let permission = require("./module/permission")
// 上传图片
let upload = require("./module/upload")

// 头像获取
app.get('/uploads/*', function (req, res) {
    res.sendFile(__dirname + "/" + req.url)
    // console.log(req.url, 999);
    // res.sendFile(__dirname.split('server')[0] + req.url)
})


// 解决跨域问题
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Content-Type", "text/json; charset=utf-8");
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,x-token");
    next()
})

app.use('/api/group', group)
app.use('/api/Gshoops', Gshoops)
app.use('/api/Gthreelevel', Gthreelevel)
app.use('/api/discuss', discuss)
app.use('/api/rules', rules)
app.use('/api/classify', classify)
app.use('/api/content', content)
app.use('/api/user', user);
app.use('/api/site', site);
app.use('/api/wxLogin', wxLogin);
app.use('/api/appLogin', appLogin);
app.use('/api/slider', slider);
app.use('/api/glLogin', glLogin);
app.use('/api/permission', permission);
app.use('/api/upload', upload);



var server = app.listen(2000, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("应用实例，访问地址为 http://%s:%s", host, port)
})