var request = require("../utils/request.js")

//获取商品详情
let getSlider = (data) => {
  return request({
    url: "slider/getSlider",
    method: "get",
    data
  })
}

let getRules = (data) => {
  return request({
    url: "rules/list",
    method: "get",
    data
  })
}

let getShoop = (data) => {
  return request({
    url: "Gshoops/accessGoods",
    method: "get",
    data
  })
}

module.exports = {
  getSlider,
  getRules,
  getShoop
}