var request = require("../utils/request.js")

//获取classify商品详情
let getShops = (data) => {
    return request({
        url: "classify/list",
        method: "get",
        data
    })
}

//获取   二级
let getTwo = (data) => {
    return request({
        url: "content/list",
        method: "get",
        data
    })
}

//获取   3级
let getThree = (data) => {
    return request({
        url: "Gthreelevel/getThreelevel",
        method: "get",
        data
    })
}



module.exports = {
    getShops,
    getTwo,
    getThree
}