var request = require("../utils/request.js")

//获取classify商品详情
let getShops = (data) => {
    return request({
        url: "Gshoops/threeshoops",
        method: "get",
        data
    })
}

module.exports = {
  getShops,
}