var request = require("../utils/request.js")

//获取商品详情
let getShops = (data) => {
  return request({
    url: "Gshoops/accessGoods",
    method: "get",
    data
  })
}

// 获取推荐商品
let getRecommend = (data) => {
  return request({
    url: "Gshoops/getRecommend",
    method: "get",
    data
  })
}

// 获取评论数据
let getDiscuss = (data) => {
  return request({
    url: "discuss/getDiscuss",
    method: "get",
    data
  })
}

module.exports = {
  getShops,
  getRecommend,
  getDiscuss
}