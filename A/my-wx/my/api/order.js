var request = require("../utils/request.js")

//获取classify商品详情
let getSite = (data) => {
    return request({
        url: "site/getSite",
        method: "get",
        data
    })
}

let updateInventory = (data) => {
  return request({
      url: "Gshoops/updateInventory",
      method: "post",
      data
  })
}

module.exports = {
  getSite,
  updateInventory
}