var request = require("../utils/request.js")


//获取地址信息
let getSite = (data) => {
  return request({
    url: "site/getSite",
    method: "get",
    data
  })
}
//新增地址
let addSite = (data) => {
  return request({
    url: "site/addSite",
    method: "post",
    data
  })
}

//修改
let updateSite = (data) => {
  return request({
      url: 'site/updateSite',
      method: "post",
      data
  })
}

//删除
let deleteSite = (data) => {
  return request({
      url: 'site/deleteSite',
      method: "get",
      data
  })
}

module.exports = {
    getSite,
    addSite,
    updateSite,
    deleteSite
}