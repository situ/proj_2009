// pages/classify/classify.js
let {
  getShops,
  getTwo,
  getThree
} = require("../../api/classify.js")

Page({
  /**
   * 页面的初始数据
   */
  data: {
    activeKey: 0,
    classify: [], //一级分类内容
    content: [], //二级
    cidx: 0, //一级对应二级的索引
    firstContent: [], //一级对应二级的内容
    threeids: '', //二级对应三级的id集合
  },

  //三级 跳转 商品页
  skip(e){
    console.log(e.currentTarget.dataset.id);
    wx.navigateTo({
      url: '/pages/classifyZy/classifyZy?id='+e.currentTarget.dataset.id,
    })
  },

  //监听切换事件
  async onChange(event) {
    let cidx = event.detail; //切换的索引
    this.setData({
      cidx
    })
    // console.log(this.data.cidx);

    //二级 id
    let cid = this.data.classify[this.data.cidx].id
    let r = this.data.content.filter(v => {
      return v.cid == cid
    })
    this.setData({
      firstContent: r
    })

    //获取三级
    let arr = []
    for(let i =0;i<this.data.firstContent.length;i++){
      let r = await getThree({id:this.data.firstContent[i].tids})
      arr.push(r.data)
      this.setData({
        a:arr
      })
    }
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let r = await getShops(); //获取一级分类
    let r2 = await getTwo(); //获取二级
    this.setData({
      classify: r.classify,
      content: r2.data
    })
    //分类页面首次加载 “独家甄选”  的内容
    let first = this.data.content.filter(v => {
      return v.cid == 1
    })
    this.setData({
      firstContent: first
    })

    //三级id
    let tids = this.data.firstContent.map(v => {
      return v.tids
    })
    this.setData({
      threeids: tids
    })
    //获取三级
    let arr = []
    for(let i =0;i<this.data.firstContent.length;i++){
      let r = await getThree({id:this.data.firstContent[i].tids})
      arr.push(r.data)
      this.setData({
        a:arr
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})