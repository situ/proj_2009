// pages/details/details.js
let site = require("../../utils/Ysite.js");
var appInfo = getApp();
let {
  getShops,
  getRecommend,
  getDiscuss
} = require("../../api/details.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    background: ['https://img.miyabaobei.com/d1/p6/2020/09/01/bc/29/bc298038002cdb29ecb41fb12ef7598c752784566.jpg'],
    indicatorDots: true, //图片轮播
    vertical: false,
    autoplay: true,
    interval: 2000,
    duration: 500,
    active: 5,
    isShow: false,//产品信息展开关闭
    show: false,//选中规格遮罩层
    color: "",//选择规格
    Siteshow: false,//三级联动遮罩层
    siteList: site.province_list,//三级联动数据
    siteData: [],//三级联动点击数据
    siteDatas: [],//三级联动选择完毕数据
    list: {},//详情数据
    recommendList: [],//商品推荐数据
    discussList: [],//评论数据
    isRush: false,
    time: 30 * 60 * 60 * 1000,
    timeData: {},
    num: 1, //步进器的值
    shoop: [], //加入购物车存到本地
    isInventory: false
  },
  onChange1(e) {
    this.setData({
      timeData: e.detail,
    });
  },
  //点击猜你喜欢
  goDetails(e) {
    // console.log(e.currentTarget.dataset.id);
    wx.navigateTo({
      url: '/pages/details/details?id=' + e.currentTarget.dataset.id,
    })
  },
  //三级联动地址
  onSite(e) {
    let num = 0;
    let obj = {};
    console.log(this.data.siteList);
    if (this.data.siteData.length) {
      for (const key in this.data.siteList) {
        if (this.data.siteList[key] == e.currentTarget.dataset.item) {
          num = key.substr(0, 4);
        }
      }
      for (const key in site.county_list) {
        if (key.substr(0, 4) == num) {
          obj[key] = site.county_list[key]
        }
      }
    } else {
      for (const key in this.data.siteList) {
        if (this.data.siteList[key] == e.currentTarget.dataset.item) {
          num = key.substr(0, 2);
        }
      }
      for (const key in site.city_list) {
        if (key.substr(0, 2) == num) {
          obj[key] = site.city_list[key]
        }
      }
    }
    this.data.siteData.push(e.currentTarget.dataset.item)
    this.setData({
      siteList: obj,
      siteData: this.data.siteData
    })
    if (this.data.siteData.length == 3) {
      this.setData({
        siteDatas: this.data.siteData
      })
      this.onClose1()
    }
  },
  //确定商品加入购物车
  sure() {
    if (!this.data.color) {
      wx.showToast({
        title: "请选择规格",
        icon: "none"
      })
      return
    }
    let shoop = wx.getStorageSync('shoop') || []
    let item = shoop.find(v => {
      return v.id == this.data.list.id && v.info == this.data.color
    })
    if (item) {
      item.num += this.data.num
    } else {
      shoop.push({
        title: this.data.list.title,
        newPrice: this.data.list.newPrice,
        num: this.data.num,
        img: this.data.list.imgurl,
        id: this.data.list.id,
        info: this.data.color,
        is: false
      })
    }
    wx.showToast({
      title: "加入购物车成功"
    })
    this.setData({
      show: false,
      num: 1
    })
    wx.setStorageSync('shoop', shoop);
  },
  //步进器的值
  onChange(e) {
    this.setData({
      num: e.detail
    })
  },
  //规格点击
  shadecolor(e) {
    this.setData({
      color: e.currentTarget.dataset.colors
    })
  },
  //三级联动显示
  SiteshowFn() {
    this.setData({
      Siteshow: true
    });
  },
  //三级联动隐藏
  onClose1() {
    this.setData({
      Siteshow: false,
      siteData: [],
      siteList: site.province_list
    });
  },
  //X关闭
  onClose() {
    this.setData({
      show: false
    });
  },
  //打开弹窗
  showPopup() {
    if (appInfo.globalData.userInfo) {
      this.setData({
        show: true
      });
    } else {
      wx.showToast({
        title: '请登录',
        icon: "none"
      })
    }
  },
  //商品图片轮播滑动
  onSwiper(e) {
    this.getRecommend(e.detail.current)
  },
  //展开关闭
  onIsShow() {
    this.setData({
      isShow: !this.data.isShow
    })
  },
  //首页
  goIndex() {
    wx.switchTab({
      url: "/pages/index/index"
    })
  },
  //购物车
  goShop() {
    wx.switchTab({
      url: "/pages/shop/shop"
    })
  },
  //获取推荐商品
  async getRecommend(e) {
    let { data } = await getRecommend({ limit: e ? e : 0 })
    this.setData({
      recommendList: data
    })
  },
  async getDiscuss() {
    let { data } = await getDiscuss()
    this.setData({
      discussList: data
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let { data } = await getShops({ ids: options.id })
    if (!(data[0].inventory * 1)) {
      console.log(data.inventory);
      this.setData({
        isInventory: true
      })
    }

    if (options.isRush) {
      this.setData({
        isRush: true
      })
    }
    await this.getRecommend();
    await this.getDiscuss();
    let obj = { ...data[0] };
    obj.specification = JSON.parse(obj.specification)
    obj.ImageCollection = JSON.parse(obj.ImageCollection)
    obj.message = JSON.parse(obj.message)
    this.setData({
      list: obj
    })
    // console.log(this.data.list);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})