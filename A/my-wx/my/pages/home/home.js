// pages/home/home.js
var appInfo = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show: false,
    userInfo: {}
  },
  goSite() {
    if(appInfo.globalData.userInfo){
      wx.navigateTo({
        url: '/pages/site/site',
      })
    }
  },
  goUser(e) {
    appInfo.globalData.userInfo = e.detail.userInfo
    wx.login({
      complete: (res) => {
        if (res.code) {
          wx.request({
            url: 'http://localhost:2000/api/wxLogin/wx-login',
            data: {
              code: res.code
            },
            success: r => {
              appInfo.globalData.openid = r.data.openid;
              this.setData({
                userInfo: appInfo.globalData.userInfo
              })
            }
          })
        } else {
          console.log('登录失败' + res.errMsg)
        }
      },
    })
    this.setData({
      show: true
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    appInfo.userInfoReadyCallback = (res) => {
      this.setData({
        userInfo: res.userInfo
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (appInfo.globalData.userInfo) {
      this.setData({
        userInfo: appInfo.globalData.userInfo
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})