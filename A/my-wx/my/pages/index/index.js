let { getSlider, getRules, getShoop } = require("../../api/Yindex")
let { getRecommend } = require("../../api/details")
Page({
  /**
   * 页面的初始数据
   */
  data: {
    background: ['https://img.miyabaobei.com/d1/p6/2020/09/01/bc/29/bc298038002cdb29ecb41fb12ef7598c752784566.jpg'],
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 2000,
    duration: 500,
    active: 5,
    imageURL: 'https://img.miyabaobei.com/d1/p6/2020/09/01/bc/29/bc298038002cdb29ecb41fb12ef7598c752784566.jpg',
    recommendList: [],
    num: 0,
    rulesTime: [],//限时抢购时间段
    rulesData: [],
    rulesIndex: 0
  },
  goDetails(e) {
    wx.navigateTo({
      url: '/pages/details/details?id=' + e.currentTarget.dataset.id + '&isRush=true'
    })
  },
  goDetail(e) {
    wx.navigateTo({
      url: '/pages/details/details?id=' + e.currentTarget.dataset.id,
    })
  },
  //限时抢购时间点击
  clickMenu: async function (event) {
    var res = wx.getSystemInfoSync()
    this.properties.windowWidth = res.windowWidth;
    this.data.rulesTime.forEach((v, i) => {
      if (v.Date == "正在秒杀") {
        this.setData({
          rulesIndex:i
        })
      }
    })
    var current = event ? event.currentTarget.dataset.current : this.data.rulesIndex;
    var tabWidth = this.properties.windowWidth / 5
    this.setData({
      tabScroll: (current - 5 / 2) * tabWidth
    })
    if (this.properties.currentTab == current) {
      return false
    } else {
      this.setData({
        currentTab: current
      })
      this.triggerEvent('clickMenu', { current: current }, {})
    }

    let arr = event ? JSON.parse(event.currentTarget.dataset.sids).join(",") : JSON.parse(this.data.rulesTime[0].sids).join(",")
    let { data } = await getShoop({ ids: arr });
    data.forEach(v => {
      let bfb = ((v.allNum - v.inventory) / v.allNum).toFixed(2) * 100
      v.bfb = bfb
    })
    this.setData({
      rulesData: data
    })

  },
  onChange(event) {
    console.log(event);
  },
  //抢购数据
  async getRecommend() {
    if (this.data.recommendList.length) {
      this.data.num++;
    }
    let { data } = await getRecommend({ limit: this.data.num })
    // console.log(data)
    let arr = [...this.data.recommendList, ...data]
    this.setData({
      recommendList: arr
    })
  },
  //当前时间
  formatDate() {
    let date = new Date();
    //设置时间转换格式
    var y = date.getFullYear(); //获取年
    var m = date.getMonth() + 1; //获取月
    m = m < 10 ? "0" + m : m; //判断月是否大于10
    var d = date.getDate(); //获取日
    d = d < 10 ? "0" + d : d; //判断日期是否大10
    var s = date.getHours(); //获取时
    s = s < 10 ? "0" + s : s; //判断日期是否大10
    return y + "-" + m + "-" + d + "--" + s; //返回时间格式
  },
  //获取限时抢购时间段
  async getRules() {
    let arr = [];
    let { data } = await getRules();
    console.log(data)
    let time = this.formatDate();
    data.sort((a, b) => {
      return b.time - a.time
    }).reverse()
    let newData = [...data];
    console.log(newData)
    newData.forEach(v => {
      if (v.time.substr(0, 4) == time.substr(0, 4)) {
        if (v.time.substr(5, 2) == time.substr(5, 2)) {
          if (time.substr(8, 2) - v.time.substr(8, 2) == 1) {
            arr.push({ ...v, Date: '昨天精选', sj: v.time.substr(12) })
          } else if (time.substr(8, 2) - v.time.substr(8, 2) == -1) {
            arr.push({ ...v, Date: '明日预告', sj: v.time.substr(12) })
          } else if (time.substr(8, 2) - v.time.substr(8, 2) == 0) {
            if (time.substr(12, 2) == v.time.substr(12, 2)) {
              arr.push({ ...v, Date: '正在秒杀', sj: v.time.substr(12) })
            } else if (time.substr(12, 2) > v.time.substr(12, 2)) {
              arr.push({ ...v, Date: '抢购中', sj: v.time.substr(12) })
            } else {
              arr.push({ ...v, Date: '预热中', sj: v.time.substr(12) })
            }
          }
        }
      }
    })
    console.log(arr)
    this.setData({
      rulesTime: arr
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let { data } = await getSlider();
    console.log(data)
    await this.getRules();
    await this.clickMenu();
    await this.getRecommend();
    console.log(1)
    this.setData({
      background: data
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getRecommend()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})