// pages/shop/shop.js
let {
  getShops
} = require("../../api/details")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    allData: [],
    checked: false,
    allchecked: "",
    total: 0,
    isTrue: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
  },
  //删除
  del(e) {
    let index = e.currentTarget.dataset.id
    let shp = wx.getStorageSync('shoop')
    shp.splice(index, 1)
    this.data.allData.splice(index, 1)
    wx.setStorageSync('shoop', this.data.allData)
    this.setData({
      allData: this.data.allData
    })
    this.total()
  },
  //价格计算
  total() {
    let alltotal = 0
    this.data.allData.reduce((total, v) => {
      if (v.is) {
        return alltotal += v.newPrice * v.num
      }
    }, 0)
    this.setData({
      total: alltotal * 100
    })

  },
  //步进器
  onchange(e) {
    let buyArr = wx.getStorageSync('shoop')
    let index = e.currentTarget.dataset.id
    buyArr[index].num = e.detail
    wx.setStorageSync('shoop', buyArr)
    this.data.allData[index].num = e.detail
    this.total()
  },
  //结算
  onClickButton() {
    let buyArr = wx.getStorageSync('shoop');
    if(!buyArr.length){
      return
    }
    let arr = []
    this.data.allData.forEach((v, index) => {
      if (v.is) {
        arr.push(v)
        // buyArr.splice(index,1)
      }
    })
    wx.setStorageSync('buyList', arr)
    wx.setStorageSync('shoop', buyArr)

    wx.navigateTo({
      url: '/pages/order/order',
    })
  },
  //全选
  onCheckAll() {
    this.data.allchecked = !this.data.allchecked
    this.data.allData.map(v => v.is = this.data.allchecked)
    this.setData({
      allchecked: this.data.allchecked,
      allData: this.data.allData
    })
    this.total()

  },
  //单选
  onSingleChange(e) {
    let index = e.currentTarget.dataset.id
    let buyArr = wx.getStorageSync('shoop')
    this.data.allData[index].is = !this.data.allData[index].is
    if (this.data.allData.every(item => item.is)) {
      this.data.allchecked = true
    } else {
      this.data.allchecked = false
    }
    buyArr[index].is = this.data.allData[index].is
    this.setData({
      allData: this.data.allData,
      allchecked: this.data.allchecked
    })
    this.total()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: async function () {
    let shoop = wx.getStorageSync('shoop') || []
    let ids = []
    shoop.forEach(val => {
      if (!ids.includes(val.id)) {
        ids.push(val.id)
      }
    })
    let r = await getShops({
      id: ids.join(',')
    })

    let allData = []
    shoop.forEach(v => {
      let item = r.data.find(f => f.id == v.id)
      allData.push({
        ...item,
        ...v
      })
    })

    // this.total()
    this.setData({
      allData: allData,
      total: 0,
      allchecked: false
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})