let request = ({
  url,
  data,
  method
}) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: 'http://localhost:2000/api/' + url,
      data,
      method,
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success(res) {
        resolve(res.data)
      }
    })
  })
}

module.exports = request