-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2020 年 09 月 11 日 07:36
-- 服务器版本: 5.6.12-log
-- PHP 版本: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `miya3`
--
CREATE DATABASE IF NOT EXISTS `miya3` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `miya3`;

-- --------------------------------------------------------

--
-- 表的结构 `apptokens`
--

CREATE TABLE IF NOT EXISTS `apptokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(200) CHARACTER SET utf8 NOT NULL,
  `uid` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `apptokens`
--

INSERT INTO `apptokens` (`id`, `token`, `uid`) VALUES
(1, '9c87c964dadb0aca3df3766b4f81540e', '1');

-- --------------------------------------------------------

--
-- 表的结构 `appuser`
--

CREATE TABLE IF NOT EXISTS `appuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `loginName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `appuser`
--

INSERT INTO `appuser` (`id`, `name`, `loginName`, `avatar`) VALUES
(1, 'miya13753938781', '13753938781', '');

-- --------------------------------------------------------

--
-- 表的结构 `classify`
--

CREATE TABLE IF NOT EXISTS `classify` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `classify` varchar(20) CHARACTER SET utf8 NOT NULL,
  `type` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=22 ;

--
-- 转存表中的数据 `classify`
--

INSERT INTO `classify` (`id`, `classify`, `type`) VALUES
(1, '独家甄选', '独家甄选'),
(2, '超级工厂', '超级工厂'),
(9, '服饰鞋包', '服饰鞋包'),
(10, '手机数码', '手机数码'),
(11, '美妆护肤', '美妆护肤'),
(12, '个护清洁', '个护清洁'),
(13, '童装童鞋', '童装童鞋'),
(14, '食品保健', '食品保健'),
(15, '家具厨具', '家具厨具'),
(16, '玩具书籍', '玩具书籍'),
(17, '孕产母乳', '孕产母乳'),
(18, '汽车用品', '汽车用品'),
(20, 'ce', 'ce'),
(21, '0910', '0910');

-- --------------------------------------------------------

--
-- 表的结构 `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tids` varchar(20) CHARACTER SET utf8 NOT NULL,
  `cid` varchar(20) CHARACTER SET utf8 NOT NULL,
  `title` varchar(300) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=15 ;

--
-- 转存表中的数据 `content`
--

INSERT INTO `content` (`id`, `tids`, `cid`, `title`) VALUES
(4, '3,4,5,6,12,14', '1', '兔妈妈甄选'),
(5, '7,8', '1', '其他品牌'),
(6, '9', '2', '家居百货'),
(7, '10,11,12,13', '9', '女装。'),
(8, '14,15', '9', '男装。'),
(9, '16,17', '9', '鞋靴配饰'),
(10, '18,19,20,21,22,23', '10', '手机数码'),
(11, '24,25', '10', '数码配件'),
(13, '5,16,22,26', '20', '测试ccc'),
(14, '1,20,17,26', '21', '0910');

-- --------------------------------------------------------

--
-- 表的结构 `discuss`
--

CREATE TABLE IF NOT EXISTS `discuss` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `content` varchar(200) CHARACTER SET utf8 NOT NULL,
  `uid` varchar(20) CHARACTER SET utf8 NOT NULL,
  `sid` varchar(20) CHARACTER SET utf8 NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `picur` varchar(300) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `discuss`
--

INSERT INTO `discuss` (`id`, `content`, `uid`, `sid`, `time`, `picur`) VALUES
(2, '搞活动的时候买的，送锅和刀叉，质量不错的，特划算，牛排冰冻的，日前也是最近的，收到货马上就煎了一个来吃，牛排特嫩，味道很不错的，买的是黑椒牛排，黑胡椒的味道很好，都不需要加其他东西啦', '5', '50', '2020-09-07 15:31:23', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1599474029729&di=cfd978732d58a8f5e33b3a14a0a21ebe&imgtype=0&src=http%3A%2F%2Fa2.att.hudong.com%2F36%2F48%2F19300001357258133412489354717.jpg'),
(3, '昨天下的单，今天就收到货了，感谢贴心的商家，送的赠品也非常的好，解决了后顾之忧，到货就开吃了，味道棒棒哒，自己搭配了点土豆。', '6', '50', '2020-09-07 15:35:14', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1599474175852&di=218e23238aa83a21e83e23568185e8ad&imgtype=0&src=http%3A%2F%2Fcache.5ikfc.com%2Fimgs%2Fkfc%2F2014%2F05%2Fquanjiatong.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sid` varchar(20) CHARACTER SET utf8 NOT NULL,
  `uids` varchar(300) CHARACTER SET utf8 NOT NULL,
  `regimental` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=13 ;

--
-- 转存表中的数据 `groups`
--

INSERT INTO `groups` (`id`, `sid`, `uids`, `regimental`, `time`) VALUES
(1, '商品id', '用户id', '拼团团长', '2020-09-08 20:09:28'),
(2, '45', '[1]', '1', '2020-09-08 23:08:19'),
(3, '45', '[1]', '1', '2020-09-08 23:08:21'),
(4, '45', '[1]', '1', '2020-09-08 23:08:37'),
(5, '65', '[1]', '1', '2020-09-08 23:09:12'),
(6, '65', '[1]', '1', '2020-09-08 23:09:13'),
(7, '32', '[1]', '1', '2020-09-08 23:29:03'),
(8, '54', '[1]', '1', '2020-09-08 23:30:09'),
(9, '3', '[1]', '1', '2020-09-08 23:31:31'),
(10, '14', '[1]', '1', '2020-09-09 00:40:59'),
(11, '11', '[1,1]', '1', '2020-09-09 21:42:10'),
(12, '84', '[1,1,1]', '1', '2020-09-10 20:42:35');

-- --------------------------------------------------------

--
-- 表的结构 `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) CHARACTER SET utf8 NOT NULL,
  `title` varchar(20) CHARACTER SET utf8 NOT NULL,
  `items` varchar(5000) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `roles`
--

INSERT INTO `roles` (`id`, `role`, `title`, `items`) VALUES
(1, 'admin', '管理员', '["仪表盘","类别管理","商品列表","三级分类","权限管理","抢购管理","商品管理"]'),
(2, 'student', '学生', '["仪表盘","面试列表","分类管理","考试模式","学习模式","考试管理"]'),
(3, 'editor', '普通用户', '["仪表盘","类别管理","商品列表","三级分类","权限管理"]'),
(4, 'text', '测试', '["仪表盘"]');

-- --------------------------------------------------------

--
-- 表的结构 `rules`
--

CREATE TABLE IF NOT EXISTS `rules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `time` varchar(200) CHARACTER SET utf8 NOT NULL,
  `sids` varchar(1000) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=27 ;

--
-- 转存表中的数据 `rules`
--

INSERT INTO `rules` (`id`, `time`, `sids`) VALUES
(2, '2020-09-08--01:00', '[3,7,11,12]'),
(4, '2020-09-08--02:00', '[8,3,13,32,37]'),
(5, '2020-09-08--04:00', '[10,12,15,34,39,42]'),
(6, '2020-09-07--06:00', '[39,41,42]'),
(7, '2020-09-09--02:00', '[42,47,48]'),
(8, '2020-09-06--01:00', '[47,49]'),
(9, '2020-09-08--24:00', '[4,34]'),
(10, '2020-09-08--12:00', '[34,41]'),
(11, '2020-09-08--22:00', '[58,56,61,60,70,78]'),
(12, '2020-09-09--08:00', '[4,8]'),
(13, '2020-09-09--02:00', '[4,9]'),
(15, '2020-09-09--10:00', '[61,60,88]'),
(16, '2020-09-09--11:00', '[51,46,63]'),
(17, '2020-09-10--02:00', '[57,58,61,63]'),
(18, '2020-09-10--09:00', '[68,67,66,70]'),
(19, '2020-09-10--23:00', '[80,79,78,81]'),
(23, '2020-09-10--04:00', '[4,9]'),
(24, '2020-09-13--04:00', '[3,8]'),
(25, '2020-09-11--04:00', '[99,98,88]'),
(26, '2020-09-11--02:00', '[100,99,98]');

-- --------------------------------------------------------

--
-- 表的结构 `shoops`
--

CREATE TABLE IF NOT EXISTS `shoops` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8 NOT NULL,
  `info` varchar(300) CHARACTER SET utf8 NOT NULL,
  `newPrice` varchar(20) CHARACTER SET utf8 NOT NULL,
  `oldPrice` varchar(20) CHARACTER SET utf8 NOT NULL,
  `specification` varchar(300) CHARACTER SET utf8 NOT NULL,
  `inventory` varchar(20) CHARACTER SET utf8 NOT NULL,
  `imgurl` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `pattern` varchar(200) CHARACTER SET utf8 NOT NULL,
  `message` varchar(200) CHARACTER SET utf8 NOT NULL,
  `tid` varchar(20) CHARACTER SET utf8 NOT NULL,
  `cid` varchar(20) CHARACTER SET utf8 NOT NULL,
  `ImageCollection` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `isRush` varchar(200) CHARACTER SET utf8 NOT NULL,
  `isGroup` varchar(20) CHARACTER SET utf8 NOT NULL,
  `recommend` int(11) NOT NULL DEFAULT '0',
  `allNum` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=102 ;

--
-- 转存表中的数据 `shoops`
--

INSERT INTO `shoops` (`id`, `title`, `info`, `newPrice`, `oldPrice`, `specification`, `inventory`, `imgurl`, `pattern`, `message`, `tid`, `cid`, `ImageCollection`, `isRush`, `isGroup`, `recommend`, `allNum`) VALUES
(3, '兔头妈妈甄选纸尿裤', '兔头妈妈甄选大师系列自然肤感婴儿纸尿裤S60片', '129', '169', '["套餐一","套餐二","套餐三"]', '94', 'https://img09.miyabaobei.com/d1/p6/item/50/5092/5092974_normal_4_ec8cad7d.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '3', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(4, '兔头妈妈甄选纸尿裤', '兔头妈妈甄选大师系列自然肤感成长裤L42片', '129', '169', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p6/item/50/5092/5092974_normal_4_ec8cad7d.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '3', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(7, '兔头妈妈甄选', '兔头妈妈甄选柔薄婴儿纸尿裤便携装S*10片(4-8kg) 婴儿尿裤尿不湿', '15.96', '29.96', '["套餐一","套餐二","套餐三"]', '96', 'https://img09.miyabaobei.com/d1/p6/item/50/5092/5092974_normal_4_ec8cad7d.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '3', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(8, '兔头妈妈甄选硅胶奶嘴', '兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶', '29', '49', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p5/item/26/2603/2603694_normal_4_8ec9e549.jpg@base@tag=imgScale&w=400&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '4', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(9, '兔头妈妈甄选硅胶奶瓶手柄', '兔头妈妈甄选硅胶奶瓶手柄-绿色,宽口径防胀气奶瓶硅胶奶瓶手柄', '24.9', '63.8', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p5/item/26/2603/2603694_normal_4_8ec9e549.jpg@base@tag=imgScale&w=400&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '4', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(10, '兔头妈妈甄选母乳储存袋 ', '兔头妈妈甄选母乳储存袋 30个/盒 250ml', '27.9', '39', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p5/item/26/2603/2603694_normal_4_8ec9e549.jpg@base@tag=imgScale&w=400&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '4', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(11, '兔头妈妈甄选三层奶粉辅食分装盒', '兔头妈妈甄选三层奶粉辅食分装盒每层容量60g', '35.9', '49', '["套餐一","套餐二","套餐三"]', '99', 'https://img09.miyabaobei.com/d1/p5/item/26/2603/2603694_normal_4_8ec9e549.jpg@base@tag=imgScale&w=400&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '4', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(12, '手推车', '兔头妈妈甄选儿童轻便单手折叠秒收手推车', '899', '1399', '["套餐一","套餐二","套餐三"]', '93', 'https://img06.miyabaobei.com/d1/p6/item/26/2623/2623070_normal_4_19a3a397.jpg@base@tag=imgScale&w=800&q=90', '1', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '5', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(13, '手推车', '好孩子婴儿蝉翼推车儿童轻便折叠口袋车可坐可躺婴儿车升级款小情书', '699', '1999', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p5/2018/08/15/1a/92/1a9297523ff697e37407fd92ff3c10ab267099567.jpg@base@tag=imgScale&w=800&q=90', '1', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '5', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(14, '黑骑士三轮车', 'Pouch黑骑士三轮车手推车脚踏车多功能双向溜娃神器B06骑士黑', '699', '1999', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2019/07/31/9e/8c/9e8c30080716598a4afc01f7b890b041545555740.jpg@base@tag=imgScale&w=750&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '5', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(15, '酸奶溶豆', '兔头妈妈甄选酸奶溶豆添加益生菌猕猴桃味20g', '25.9', '52', '["套餐一","套餐二","套餐三"]', '100', 'https://img08.miyabaobei.com/d1/p6/item/55/5591/5591048_normal_4_fb248e07.jpg@base@tag=imgScale&w=400&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '6', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '', '', 0, '100'),
(32, '法蔓兰', '法蔓兰法蔓兰瑞研黄金紧颜系列套装 50mL+15mL+50g', '899', '1299', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p6/item/51/5132/5132927_normal_4_3c2ef9aa.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '7', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(33, '法蔓兰根源晶', '法蔓兰根源晶透4件套（洁面乳+精华水+乳液+眼霜）', '399', '599', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p6/item/56/5641/5641519_normal_4_74b876ba.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '7', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(34, '法蔓兰', '法蔓兰法蔓兰流光幻彩九色眼影盘-光影星河', '129', '180', '["套餐一","套餐二","套餐三"]', '95', 'https://img09.miyabaobei.com/d1/p6/item/51/5116/5116774_normal_4_2802c086.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '7', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(35, '法蔓兰瑞研黄金', '法蔓兰瑞研黄金紧颜化妆水50ml', '299', '450', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p6/item/56/5614/5614067_normal_4_3cf0ebf7.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '7', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(36, '法蔓兰', '法蔓兰法蔓兰碧萝芷玻尿酸深层保湿洁面乳', '59', '89', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/item/47/4752/4752037_normal_4_ddf43540.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '7', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(37, '优培农场', '优培农场胶原蛋白燕窝肽果味饮品', '129', '210', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/item/30/3039/3039863_normal_4_91a8fd90.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '8', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(38, '优培农场', '橘皮复合发酵果蔬饮品 25ml*10袋 其他/other', '139', '219', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/item/44/4422/4422070_normal_4_d9bb77b2.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '8', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(39, '优培农场', '胶原蛋白燕窝肽*3盒+品牌礼品袋*1个', '402', '707', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/item/56/5690/5690281_normal_4_f771c9ae.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '8', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(40, '优培农场', '雨生红球藻弹性蛋白复合粉45g/盒', '299', '580', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/item/57/5733/5733348_normal_4_78e673d3.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '8', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(41, '一次性使用防护口罩', '民用口罩1袋装30只 一次性口罩（3层）30只 款式1', '49.5', '69.5', '["套餐一","套餐二","套餐三"]', '89', 'https://img06.miyabaobei.com/d1/p6/2020/06/28/bd/ba/bdbaffa132ff95ac24e2f0121145c42f269172414.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '9', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(42, '家用背心垃圾袋', '背心式垃圾袋手提加厚厨房家用彩色塑料袋 混色5卷装', '11.9', '24', '["套餐一","套餐二","套餐三"]', '100', 'https://img08.miyabaobei.com/d1/p6/2020/06/28/56/de/56dec66a4d1fe43e62c958e8da624d5f399914215.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '9', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(43, '日式马卡龙软毛牙刷', '10支装带保护套中小头成人组合家庭装【绿+米】一盒10支', '9.9', '20', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/other/2071063950377_1.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '9', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(44, '挂钩粘胶门后上吸盘', '挂钩粘胶门后上吸盘衣勾子厨房壁挂墙壁承重无痕贴免打孔粘钩 20个装', '9.9', '19.9', '["套餐一","套餐二","套餐三"]', '100', 'https://img06.miyabaobei.com/d1/p6/2020/06/19/1f/59/1f59ffc2176ab44389ee49e9a3d33185464159192.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '9', '1', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(45, '骆驼珠穆朗玛系列', '骆驼珠穆朗玛系列冲锋衣男女春秋薄款单层外套旅行服亲子装', '329', '1398', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/06/15/ef/9b/ef9b9eefd778e3a429828dc2d4656df3181415494.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '10', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(46, 'COOLACNE【5件套】', '春夏瑜伽服健身服跑步服速干衣运动套装健身房T恤四五件套女', '149', '199', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/07/11/11/92/1192fb378fff5685848b98962901655e505539167.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '13', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(47, '骆驼户外抓绒衣', '男女防静电保暖摇粒绒抓绒亲子装外套', '139', '528', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/2020/09/04/6a/72/6a725aac8d2d4f805812e5092747f2b9001637879.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '10', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(48, '骆驼珠穆朗玛系列夹棉冲锋衣', '男女 秋冬新款加厚潮流百搭款外套', '398', '1398', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/09/02/a7/2b/a72b0fac1b84e6ac9d8108e83cdc569f369425316.jpg@base@tag=imgScale&w=750&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '11', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(49, '骆驼珠穆朗玛系列', '夹棉冲锋衣男女2020新款户外拼色防水外套防寒服', '398', '1398', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/2020/08/18/7c/3c/7c3cfb372670040b532b1e399707d82c477408959.jpg@base@tag=imgScale&w=750&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '12', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(50, '冰衿运动套装', '女奥代尔纯棉夏季2020年新款短袖休闲服洋气潮牌时尚两件套单件可选', '49', '89', '["套餐一","套餐二","套餐三"]', '88', 'https://img06.miyabaobei.com/d1/p6/2020/07/10/f5/23/f52371a1a1ad2e89273d28720df77d82498332135.jpg@base@tag=imgScale&w=750&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '10', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(51, '骆驼瑜伽服套装', '女专业运动服晨跑步衣健身房健身服薄款', '219', '829', '["套餐一","套餐二","套餐三"]', '100', 'https://img08.miyabaobei.com/d1/p6/2020/07/02/f6/85/f685085714dd067d295b434005ecd7d5585961386.jpg@base@tag=imgScale&w=750&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '13', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(52, '冰衿运动套装', '女纯棉夏季2020新款时尚潮牌休闲港味嘻哈休闲装洋气跑步两件套单件可选', '49', '89', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p6/2020/07/10/b1/14/b1143779f7a207d1dcb01a050c52c126663737182.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '10', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(53, '高梵2020年运动套装', '女休闲夏天潮牌时尚卫衣两件套装网红宽松韩版', '89', '499', '["套餐一","套餐二","套餐三"]', '100', 'https://img06.miyabaobei.com/d1/p6/2020/04/27/42/da/42daffd770274fc966e30a36aae67aca729715164.jpg@base@tag=imgScale&w=750&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '10', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(54, '森马夏季新款', '套装女宽松不规则下摆短袖T恤印花松紧腰短裤', '89.98', '239', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p6/2020/03/21/ee/73/ee735b8aac03138feab4bb234c8cb2d6984512756.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '12', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(55, '速干收腰显瘦', '速干收腰显瘦后背网纱反光条运动短袖T恤', '59', '199', '["套餐一","套餐二","套餐三"]', '100', 'https://img06.miyabaobei.com/d1/p6/2020/06/15/8f/b9/8fb904317ee13607ee43849c34c06c5a235906950.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '10', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(56, '阿迪达斯', '女装大LOGO跑步瑜伽运动透气文胸内衣 DU1283', '199', '369', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/2020/06/03/0b/c2/0bc2df835aeef442ea289f4304d15ca0481046645.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '13', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(57, '田运2020春季新款', '真皮铆钉鞋平底小皮鞋英伦风牛皮单鞋女布洛克乐福鞋女D1235 默认黑35', '399', '1896', '["套餐一","套餐二","套餐三"]', '100', 'https://img06.miyabaobei.com/d1/p6/2019/12/23/7d/d2/7dd254ddfba3a0efcfda7095fcccff06836435132.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '16', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(58, '春秋软底软皮妈妈鞋棕色', '单鞋女新款百搭时尚一字扣粗跟春秋软底软皮妈妈鞋棕色', '148', '998', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/08/23/21/08/2108aab234b9573af88939c7d12be048658722296.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '16', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(59, '女鞋尖头皮搭扣漆皮KD235', '田运新羊皮女皮鞋定大码鞋粗跟单鞋女真皮尖头浅口女鞋尖头皮搭扣漆皮KD235', '179', '969', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p6/2019/04/18/6b/34/6b34d7cbe3789fbdb9b216dd65be926c932608664.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '16', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(60, '性感水钻单鞋女', '新款真皮职业工作鞋高跟鞋女细跟百搭尖头性感水钻单鞋女', '128', '998', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/08/19/88/57/88572cb0c5296d1a0ce4d613f7010315332592109.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '16', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(61, '女士高跟厚底凉鞋', '新款百搭韩版学生坡跟女士高跟厚底凉鞋', '158', '579', '["套餐一","套餐二","套餐三"]', '92', 'https://img07.miyabaobei.com/d1/p6/2020/07/21/3c/c6/3cc6c6903cecf460f84c09c0cd7d0807229023442.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '17', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(62, 'crocs卡骆驰女鞋', '卡骆驰时尚皮质纯色秋季蕾丽系带坡跟厚底靴|203419', '319.6', '799', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p6/2019/07/23/c2/5f/c25fd162b8c977e669f13cf8fca35f8f606928077.jpg@base@tag=imgScale&w=750&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '16', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(63, '春夏新款网红小香风粗花呢', 'Allmelody2020春夏新款网红小香风粗花呢two-tone单鞋', '138', '399', '["套餐一","套餐二","套餐三"]', '100', 'https://img09.miyabaobei.com/d1/p6/2020/05/07/1c/fb/1cfb897358f39b09b0fc19ac2287bc25182393293.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '17', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(64, '女中跟秋鞋粗跟真皮方头妈妈鞋', '田运头层牛皮2020春季春鞋百搭复古单鞋女中跟秋鞋粗跟真皮方头妈妈鞋', '299', '1869', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/2020/04/03/c5/30/c5309229eec39482eea36121e88c0c1b057047432.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '17', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(65, '森马', '【玩具总动员系列】卫衣男针织圆领套头衫2020秋新款加绒上衣', '159.99', '199', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/08/25/f9/43/f9430498609c6a7886b507eed94ce625344428236.jpg@base@tag=imgScale&w=750&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '14', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(66, '海澜之家', '暴雪娱乐合作系列秋季新品守望先锋舒适卫衣男HNZWJ3R211A', '158', '258', '["套餐一","套餐二","套餐三"]', '100', 'https://img06.miyabaobei.com/d1/p6/2019/09/05/d4/c0/d4c0cf7c434ea3befc87965dacf7259d480288020.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '15', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(67, '森马卫衣', '森马卫衣男潮ins港风圆领上衣字母刺绣2020秋季新款加绒保暖衣服', '159.99', '179', '["套餐一","套餐二","套餐三"]', '100', 'https://img08.miyabaobei.com/d1/p6/2020/08/25/54/ce/54ce74b8229c7d59328bbabdb5fa6b6d348315247.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '15', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(68, '龙达飞', '商场同款纯棉春秋新款卫衣男潮流宽松舒适连帽卫衣', '139', '339', '["套餐一","套餐二","套餐三"]', '100', 'https://img06.miyabaobei.com/d1/p6/2020/09/02/01/b1/01b1a4452b8d0da09e1fdd1fdc23496f385882615.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '15', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(69, '森马情侣卫衣', '森马情侣卫衣2020秋新款oversize男女同穿潮流', '219.99', '319', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/2020/08/25/bb/a5/bba5bd0ffe2a785036015503fe63d4b2534575907.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '15', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(70, '森马卫衣男装', '2020秋季新款韩版潮牌撞色连帽上衣学生字母休闲外套', '159.99', '199', '["套餐一","套餐二","套餐三"]', '100', 'https://img08.miyabaobei.com/d1/p6/2020/07/29/a2/ec/a2ec7c8cbd57a0a46690b30a5ebcfa82017822611.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '14', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(71, '海澜之家', '连帽抽绳卫衣时尚满身印花舒适上衣男HNZWJ3R054A', '218', '298', '["套餐一","套餐二","套餐三"]', '100', 'https://img08.miyabaobei.com/d1/p6/2020/06/07/5d/d9/5dd9196ce801d0a36dbf6b9bb325e8ec969609544.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '14', '9', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(72, '苹果', '苹果AppleiPhone11(A2223) 移动联通电信4G手机', '4399', '5999', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2019/09/11/4b/03/4b03ab5273a4125de31e59a693482a8f738533654.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '18', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(73, '苹果AppleiPhoneXS', 'XS(A2100)移动联通电信4G手机', '4999', '10099', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p5/2018/09/13/e4/1e/e41e85a5c27fcd0788b7c4028db6516a103551322.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '18', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100');
INSERT INTO `shoops` (`id`, `title`, `info`, `newPrice`, `oldPrice`, `specification`, `inventory`, `imgurl`, `pattern`, `message`, `tid`, `cid`, `ImageCollection`, `isRush`, `isGroup`, `recommend`, `allNum`) VALUES
(74, '苹果AppleiPhoneXR', '移动联通电信4G手机双卡双待', '4129 ', '6999', '["套餐一","套餐二","套餐三"]', '100', 'https://img08.miyabaobei.com/d1/p5/2018/09/13/74/20/74205de1bfe825ed36838974708c4115127437767.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '18', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(75, '苹果AppleiPhone11Pro', '移动联通电信4G手机双卡双待', '6799', '8999', '["套餐一","套餐二","套餐三"]', '100', 'https://img06.miyabaobei.com/d1/p6/2019/09/12/ed/2e/ed2ed1a06391ac280f4dd5f3c35242a1782729458.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '18', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(76, '苹果AppleAirPods', '新款蓝牙无线耳机二代 新款AirPods', '969', '1348', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2019/09/10/ef/cc/efccdb0b6e63e5ccbd47e8f4a25e3a2b034234635.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '18', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(77, '苹果AppleiPadPro', '11英寸平板电脑2020年新款 ', '6099 ', '6599', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/2020/03/23/d3/e8/d3e87d99efbc73bb3ca42ed3c4ef0511371777695.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '18', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(78, '华为nova', '全网通双卡双待4G手机苏音蓝 ', '1699', '1999', '["套餐一","套餐二","套餐三"]', '99', 'https://img05.miyabaobei.com/d1/p6/2019/06/27/fe/56/fe5664cbf3dd80c699f9abc2cd4466f9320047211.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '19', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(79, '华为nova5Pro', '全网通双卡双待4G手机仲夏紫8GB+128GB', '2299 ', '2999', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2019/06/26/d6/a0/d6a05da2b24d956d7e0fe412daa1161e450945450.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '19', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(80, '华为畅享10', '极点全面屏全网通双4G手机幻夜黑4GB+64GB', '1148', '1199', '["套餐一","套餐二","套餐三"]', '99', 'https://img09.miyabaobei.com/d1/p6/2020/04/24/b0/62/b062dd68ed60b923816b17e3c5b6c979008911872.jpg@base@tag=imgScale&w=400&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '19', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(81, '华为Mate30Pro', '全网通版手机亮黑色8GB+128GB', '5899', '6389', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/01/09/2f/d9/2fd906b1e7cb7c76631a81def64058c2555471410.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '19', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(82, '华为P40', '5000万徕卡三摄全网通5G游戏智能商务手机', '4188', '5899', '["套餐一","套餐二","套餐三"]', '94', 'https://img06.miyabaobei.com/d1/p6/2020/03/27/e6/cd/e6cdb9d157a062dff1e146fe3735bc13883525868.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '19', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(83, '荣耀30青春版 ', '4800万高感光拍摄', '1899 ', '2189', '["套餐一","套餐二","套餐三"]', '100', 'https://img05.miyabaobei.com/d1/p6/2020/07/06/9d/8b/9d8bf3afe3f923cf78395d81309b6f8f159223626.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '20', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(84, 'OPPOAce2', '8GB+256GB月岩灰全网通月岩灰Ace28GB+256GB', '3999 ', '3999 ', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/04/27/16/8a/168a5f022dc0908830419793f01c65fa887492717.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '21', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(85, 'vivoX50pro', '双模5G60倍超级变焦微云台主摄防抖33W闪充轻薄曲面屏新品旗舰液氧8+128GB', '4298 ', '4298 ', '["套餐一","套餐二","套餐三"]', '81', 'https://img09.miyabaobei.com/d1/p6/2020/06/23/d1/ae/d1aec934742c99a8b28380eb16fc0f21966242813.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '22', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(86, '小米10纪念版', '20W快充智能商务游戏手机陶瓷黑 ', '5299 ', '5299 ', '["套餐一","套餐二","套餐三"]', '96', 'https://img05.miyabaobei.com/d1/p6/2020/09/01/2a/ce/2ace2f050774a71eb050e770622b59b9478806708.jpg@base@tag=imgScale&w=800&q=90', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '23', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(87, '小米小米10', '1亿像素双卡双待全网通', '3758 ', '3999', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/02/14/9f/6f/9f6f5467a3b457a6d690a582c5a47fa1611236825.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '23', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(88, '精准孔iPhone11手机壳', '磨砂苹果11ProMax撞色全包套xsmax简约', '12.9', '58', '["套餐一","套餐二","套餐三"]', '0', 'https://img07.miyabaobei.com/d1/p6/2020/08/21/00/f7/00f7db4eccff148a06cbfc1572f30352936892221.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '24', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 0, '100'),
(89, '冇心', '手机三合一数据线快速充电苹果安卓Type-c通用樱之花-1m-苹果+安卓+typec', '36', '68', '["套餐一","套餐二","套餐三"]', '60', 'https://img08.miyabaobei.com/d1/p6/2020/05/21/33/85/33852698ab2b151dcbaaeb27560b8e40472099952.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '25', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(97, '123', '123', '1231', '231', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/06/15/ef/9b/ef9b9eefd778e3a429828dc2d4656df3181415494.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '1', '2', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(98, '测试', '测试瓦大', '12', '15', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/06/15/ef/9b/ef9b9eefd778e3a429828dc2d4656df3181415494.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '1', '11', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(99, '1', '123', '13', '15', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/06/15/ef/9b/ef9b9eefd778e3a429828dc2d4656df3181415494.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '1', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(100, 'ce', 'aaa', '15', '111', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/06/15/ef/9b/ef9b9eefd778e3a429828dc2d4656df3181415494.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '1', '10', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100'),
(101, '0910', '11', '12', '141', '["套餐一","套餐二","套餐三"]', '100', 'https://img07.miyabaobei.com/d1/p6/2020/06/15/ef/9b/ef9b9eefd778e3a429828dc2d4656df3181415494.jpg', '0', '[{"title":"类别","name":"奶嘴"},{"title":"编码","name":"2603695"},{"title":"品牌","name":"兔头妈妈甄选"},{"title":"简介","name":"兔头妈妈甄选硅胶奶嘴(0-6月) 宽口径防胀气奶瓶硅胶奶瓶"}]', '27', '14', '["https://img05.miyabaobei.com/d1/p6/2020/03/01/cb/4b/cb4bcc803bc3774d214862b796370f76506439783.jpg@base@tag=imgScale&w=800&q=90","https://img08.miyabaobei.com/d1/p6/2020/07/21/45/82/45827a80484618c1f0a9bfa407832a64265179305.jpg@base@tag=imgScale&w=800&q=90","https://img06.miyabaobei.com/d1/p6/2019/01/14/18/dc/18dc7df9ebbb2de4cb83e979a9b6e324449777369.jpg@base@tag=imgScale&w=800&q=90"]', '1', '1', 1, '100');

-- --------------------------------------------------------

--
-- 表的结构 `site`
--

CREATE TABLE IF NOT EXISTS `site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `site` varchar(200) NOT NULL,
  `coll` varchar(200) NOT NULL,
  `Detailed` varchar(1000) NOT NULL,
  `uid` varchar(100) NOT NULL,
  `nickName` varchar(100) NOT NULL,
  `priority` int(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- 转存表中的数据 `site`
--

INSERT INTO `site` (`id`, `name`, `site`, `coll`, `Detailed`, `uid`, `nickName`, `priority`) VALUES
(41, 'yang', '[{"code":"220000","name":"吉林省"},{"code":"220100","name":"长春市"},{"code":"220112","name":"双阳区"}]', '18434', '12', '1', '良人', 0),
(46, '杨家豪', '山西省,运城市,闻喜县', '18434865404', '侯村', 'o1Vmn5COmHUeMN9Bz3AN4r0o2T-g', '良人', 1),
(49, '杨家豪', '山西省,运城市,闻喜县', '18434865404', '侯村', 'undefined', '良人', 1),
(50, '杨', '山西省,运城市,闻喜县', '1', '11', 'undefined', '良人', 0);

-- --------------------------------------------------------

--
-- 表的结构 `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(500) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `slider`
--

INSERT INTO `slider` (`id`, `img`, `title`, `url`) VALUES
(1, 'https://img.miyabaobei.com/d1/p6/2020/09/04/be/50/be5018948ca07ef70256536b1999e1a6910542173.jpg', '', ''),
(2, 'https://img.miyabaobei.com/d1/p6/2020/09/01/bc/29/bc298038002cdb29ecb41fb12ef7598c752784566.jpg', '', ''),
(3, 'https://img.miyabaobei.com/d1/p6/2020/09/03/0b/90/0b9015bab4c2e21afecf4f284adff6f5210513351.jpg', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `threelevel`
--

CREATE TABLE IF NOT EXISTS `threelevel` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8 NOT NULL,
  `imgurl` varchar(300) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=29 ;

--
-- 转存表中的数据 `threelevel`
--

INSERT INTO `threelevel` (`id`, `title`, `imgurl`) VALUES
(3, '纸尿裤', 'https://img.miyabaobei.com/d1/p5/2017/08/30/8e/77/8e77a8b9be595c9e1ab588af39ee320e655911217.jpg'),
(4, '母婴用品', 'https://img.miyabaobei.com/d1/p5/2017/06/14/9d/98/9d98cf19f40043409c696fef2b76a898139965922.jpg'),
(5, '出行', 'https://img.miyabaobei.com/d1/p5/2018/03/16/9f/ae/9fae908875b349aa660d3b0b9d09f7e3008753434.jpg'),
(6, '辅食', 'https://img.miyabaobei.com/d1/p5/2018/10/08/a9/f3/a9f39e312ab2066e85d2afb83475a704900447875.jpg'),
(7, '法蔓澜', 'https://img.miyabaobei.com/d1/p6/2019/03/13/b6/31/b63103b2fea68ea521d4900e1d10f9c3594448765.jpg'),
(8, '优培农村', 'https://img.miyabaobei.com/d1/p6/2019/03/21/72/f1/72f1998514d9a0f0225671e41bacf3f7672218365.png'),
(9, '日用百货', 'https://img.miyabaobei.com/d1/p6/2020/07/06/c1/3f/c13f5bd8ec1cc3d9ac0150150f461fdf286587759.png'),
(10, 'T血', 'https://img.miyabaobei.com/d1/p6/2019/10/23/6c/47/6c479200a5c51c9ceabec2b410f8eaeb252856103.jpg'),
(11, '半身裙', 'https://img.miyabaobei.com/d1/p6/2019/03/27/70/5b/705be8c71c9240b44b7afe702e94e218694433499.jpg'),
(12, '休闲裤', 'https://img.miyabaobei.com/d1/p5/2018/09/25/e3/70/e37082f4835d8190bfac72704f86c9b8104022142.jpg'),
(13, '运动套装', 'https://img.miyabaobei.com/d1/p6/2019/08/27/09/c3/09c32556a3af4a363b2048541a38bb0b898018979.jpg'),
(14, '西服', 'https://img.miyabaobei.com/d1/p6/2019/04/03/70/71/70717cb9fa01c3cd2013edd84f9e0315924882725.jpg'),
(15, '衬衫', 'https://img.miyabaobei.com/d1/p6/2019/04/03/57/6d/576d42000e42aff4873175aa8c5f90f3924315431.png'),
(16, '高跟鞋', 'https://img.miyabaobei.com/d1/p6/2019/04/03/c5/dd/c5dd86b6b10303e673ad66f683f77572932065638.jpg'),
(17, '凉鞋', 'https://img.miyabaobei.com/d1/p6/2019/04/03/d6/43/d643fa1406696f887beacc35a11b4be1934292371.jpg'),
(18, '苹果', 'https://img.miyabaobei.com/d1/p5/2018/10/07/88/b5/88b536ba3d593d816e2513ff34091b96261705670.jpg'),
(19, '华为', 'https://img.miyabaobei.com/d1/p5/2018/09/17/5b/87/5b87dcabd65c1f6f53628f9f422dc560849643611.jpg'),
(20, '荣耀', 'https://img.miyabaobei.com/d1/p5/2018/09/17/53/27/5327c84004e9da10301ec0a43eb52d5b850296833.jpg'),
(21, 'OPPO', 'https://img.miyabaobei.com/d1/p5/2018/09/17/c1/e6/c1e61673e0dd39a3960909efa1d5cabc853154467.jpg'),
(22, 'vivo', 'https://img.miyabaobei.com/d1/p6/2019/01/11/bc/4b/bc4bee175b5dfeafe92f0a1d3a679a05771324070.jpg'),
(23, '小米', 'https://img.miyabaobei.com/d1/p5/2018/09/17/05/80/0580ba4cebf9a6b84d0c1523d1a9d8b1850801909.jpg'),
(24, '手机壳', 'https://img.miyabaobei.com/d1/p6/2019/01/11/19/ea/19ea6175ee28a60f51e213694c5a0a96859131342.jpg'),
(25, '数据线', 'https://img.miyabaobei.com/d1/p6/2019/01/11/b0/42/b0422be811159912d93a4aa7a7373823861577200.jpg'),
(26, '测试1', 'https://img.miyabaobei.com/d1/p5/2017/06/14/9d/98/9d98cf19f40043409c696fef2b76a898139965922.jpg');

-- --------------------------------------------------------

--
-- 表的结构 `tokens`
--

CREATE TABLE IF NOT EXISTS `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(35) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `token` (`token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- 转存表中的数据 `tokens`
--

INSERT INTO `tokens` (`id`, `token`, `ts`, `uid`) VALUES
(9, '4f21fec9d607a72bbe9ca9d5678c1c66', '2020-02-10 11:42:53', 26),
(10, '9bf6601861d11debfafabe2e3b9fcdfd', '2020-02-10 12:58:26', 27),
(11, '60a4b31b1d45541f7ff5e023d10c6669', '2020-02-26 02:19:13', 31),
(12, '287f09a79c5e5fa5fa70340ba5b404c2', '2020-03-10 13:53:24', 72),
(13, '54a1ca33c4922b9af08dadacd989cb5b', '2020-03-11 02:40:58', 73),
(15, '25f6443a401b28a8966419845f3d3cdb', '2020-04-24 03:44:53', 74),
(16, '2a4baa3f66c51805b35f0a62288fe5d5', '2020-04-25 08:14:27', 75),
(17, 'b2204ac29fffc523e9c4bd8a319ca3be', '2020-04-25 08:16:29', 76),
(18, '7c03324728bd3d59120536a51db24f58', '2020-04-25 08:28:36', 78),
(40, '9c87c964dadb0aca3df3766b4f81540e', '2020-08-25 14:15:18', 1),
(50, 'd893a42d87d60a66d5ba790133c53ebc', '2020-09-10 12:32:13', 2);

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 NOT NULL,
  `loginName` varchar(20) CHARACTER SET utf8 NOT NULL,
  `loginKey` varchar(20) CHARACTER SET utf8 NOT NULL,
  `roles` varchar(20) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=7 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `name`, `loginName`, `loginKey`, `roles`, `avatar`) VALUES
(1, '网名', '用户名', '密码', '权限', '头像'),
(2, '杨', 'admin', '111111', 'admin', '1.png'),
(3, 'undefined', 'undefined', 'undefined', 'undefined', 'undefined'),
(4, 'y', 'editor', '111111', 'editor', '2.png'),
(5, '评论测试', 'plcs11', '111111', 'admin', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1599473954171&di=e41b8499fd3cedb97545acbd5ab1d932&imgtype=0&src=http%3A%2F%2Fb.hiphotos.baidu.com%2Fzhidao%2Fwh%253D450%252C600%2Fsign%3Da587b23df11f3a295a9dddcaac159007%2F500fd9f9d72a60590cfef2f92934349b023bba62.jpg'),
(6, '蜜芽测试哈哈', 'mytext', '111111', 'admin', 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1599474591666&di=a00eeedd6c2c656a9678f5faa05cc5f9&imgtype=0&src=http%3A%2F%2Fa1.att.hudong.com%2F05%2F00%2F01300000194285122188000535877.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
