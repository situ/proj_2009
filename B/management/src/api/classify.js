import request from '@/utils/request'
import qs from "qs"



export function getClassifyData() {
    return request({
        url: "/classify/list",
        method: "get",
        params: {},
    })
}


export function AddClassifyData(data) {
    return request({
        url: "/classify/addList",
        method: "post",
        data: qs.stringify(data)
    })
}




export function UpdateClassifyData(data) {
    return request({
        url: "/classify/updateList",
        method: "post",
        data: qs.stringify(data)
    })
}



export function DelClassifyData(id) {
    return request({
        url: "/classify/delList",
        method: "get",
        params: { id }
    })
}

