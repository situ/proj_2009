import request from '@/utils/request'
import qs from "qs"

export function getQuestions(data) {
    return request({
        url: '/question/getQuestions',
        method: 'get',
        params: data
    })
}


export function saveQuestions(data) {
    return request({
        url: '/question/saveQuestions',
        method: 'post',
        data: qs.stringify(data)
    })
}

export function xgQuestions(data) {
    return request({
        url: '/question/xgQuestions',
        method: 'post',
        data: qs.stringify(data)
    })
}


export function delQuestions(id) {
    return request({
        url: '/question/delQuestions',
        method: 'get',
        params: {
            id
        }
    })
}