import request from '@/utils/request'
import qs from "qs"

export function getData(data) {
    return request({
        url: "/jiesuan/list",
        method: "get",
        params: data
    })
}

export function DelteData(id) {
    return request({
        url: "/jiesuan/delList",
        method: "get",
        params: {
            id
        }
    })
}

export function UpData(data) {
    return request({
        url: "/jiesuan/xgList",
        method: "post",
        data: qs.stringify(data)
    })
}