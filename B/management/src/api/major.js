import request from '@/utils/request'
import qs from "qs"



export function getmajorData() {
    return request({
        url: "/major/list",
        method: "get",
        params: {},
    })
}


export function AddmajorData(data) {
    return request({
        url: "/major/addList",
        method: "post",
        data: qs.stringify(data)
    })
}




export function UpdatemajorData(data) {
    return request({
        url: "/major/updateList",
        method: "post",
        data: qs.stringify(data)
    })
}



export function DelmajorData(id) {
    return request({
        url: "/major/delList",
        method: "get",
        params: { id }
    })
}

