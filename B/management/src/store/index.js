import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import commodity from "./modules/commodity"
import classify from "./modules/classify"
import major from "./modules/major"
import jiezhang from "./modules/jiezhang"

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    commodity,
    classify,
    major,
    jiezhang
  },
  getters
})

export default store
