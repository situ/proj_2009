import { getClassifyData, AddClassifyData, UpdateClassifyData, DelClassifyData } from "../../api/classify";


export default ({
    namespaced: true,
    state: {
        calssify: [],
    },
    mutations: {
        getCls(state, data) {
            let datas = [{}]
            data = datas.concat(data)
            state.calssify = data
        },
    },
    actions: {
        //获取列表
        async getClassify({ state, commit }) {
            const res = await getClassifyData()
            commit('getCls', res.data)
        },
        //新增
        async AddClassify({ state, commit, dispatch }, data) {
            await AddClassifyData({ type: data })
            dispatch('getClassify')
        },
        //修改
        async UpdateClassify({ state, commit, dispatch }, data) {
            await UpdateClassifyData(data)
            dispatch('getClassify')
        },
        //删除
        async DelClassify({ state, commit, dispatch }, id) {
            await DelClassifyData(id)
            dispatch('getClassify')
        },

    }
})