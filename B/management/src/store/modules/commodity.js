import { getQuestions, saveQuestions, xgQuestions, delQuestions } from "../../api/commodity"

export default {
    namespaced: true,
    state: {
        tableData: [],
        search: '',
        pageSize: 5,
        curr: 1,
        total: 0
    },
    mutations: {
        setques(state, data) {
            state.tableData = data
        },
        setSearch(state, data) {
            state.search = data
        },
        setpageSize(state, data) {
            state.pageSize = data
        },
        setcurrentPage(state, data) {
            state.curr = data
        },
        setTotal(state, data) {
            state.total = data
        }
    },
    actions: {
        async getQuestions({ state, commit }) {
            let r = await getQuestions({
                keyword: state.search,
                currentPage: state.curr - 1,
                pageSize: state.pageSize
            })
            commit('setTotal', r.total[0].total)
            commit('setques', r.data)
        },
        async saveQuestions({ commit, dispatch }, r) {
            await saveQuestions(r)
            dispatch('getQuestions')
        },
        async xgQuestions({ commit, dispatch }, r) {
            await xgQuestions(r)
            dispatch('getQuestions')
        },
        async delQuestions({ commit, dispatch }, id) {
            await delQuestions(id)
            dispatch('getQuestions')
        }
    },
    getters: {

    }
}