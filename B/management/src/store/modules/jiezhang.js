import { getData, DelteData, UpData } from "../../api/jiesuan"
export default {
    namespaced: true,
    state: {
        list: [],
        page: {
            currentPage: 1, //当前页
            pageSize: 3, //每页几条
            keyword: '', //搜索
            by: "id",//默认根据id来排序
            order: "desc"//默认倒序
        },
        total: 1,
    },
    mutations: {
        SET_GRTDATA(state, val) {
            let arr = val.data
            arr.map(v => {
                if (v.zhuangtai == '0') {
                    v.zhuangtai = "未购买"
                } else {
                    v.zhuangtai = "已购买"
                }
            })
            state.list = arr
            state.total = val.total[0].total
        },
        SET_CURRENTPAGE(state, val) {
            state.page.currentPage = val
        },
        SET_PAGESIZE(state, val) {
            state.page.pageSize = val
        },
        SET_SEARCh(state, val) {
            state.page.keyword = val
        }
    },
    actions: {
        //拿数据
        async getData({ commit, state }) {
            let form = { ...state.page }
            form.currentPage = form.currentPage - 1
            let res = await getData(form)
            commit('SET_GRTDATA', res)
        },
        //修改当前页
        setCurrentPage({ commit, dispatch }, val) {
            commit("SET_CURRENTPAGE", val)
            dispatch('getData')
        },
        //修改当前页一页几条
        setPageSize({ commit, dispatch }, val) {
            commit("SET_PAGESIZE", val)
            dispatch('getData')
        },
        //删除
        async DelteData({ commit, state, dispatch }, id) {
            let res = await DelteData(id)
            commit("SET_CURRENTPAGE", 1)
            dispatch('getData')
        },
        //修改
        async UpData({ commit, state, dispatch }, form) {
            // console.log(form)
            let res = await UpData(form)
            dispatch('getData')
        },
        //搜索
        async search({ commit, dispatch }, txt) {
            commit('SET_SEARCh', txt)
            dispatch('getData')
        },
    },
}