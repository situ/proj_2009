import { getmajorData, AddmajorData, UpdatemajorData, DelmajorData } from "../../api/major";


export default ({
    namespaced: true,
    state: {
        major: [],
    },
    mutations: {
        getCls(state, data) {
            let datas = [{}]
            data = datas.concat(data)
            state.major = data
        },
    },
    actions: {
        //获取列表
        async getMajor({ state, commit }) {
            const res = await getmajorData()
            commit('getCls', res.data)
        },
        //新增
        async AddClassify({ state, commit, dispatch }, data) {
            await AddmajorData({ major: data })
            dispatch('getClassify')
        },
        //修改
        async UpdateClassify({ state, commit, dispatch }, data) {
            await UpdatemajorData(data)
            dispatch('getClassify')
        },
        //删除
        async DelClassify({ state, commit, dispatch }, id) {
            await DelmajorData(id)
            dispatch('getClassify')
        },

    }
})