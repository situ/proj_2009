var express = require('express');
var app = express()


var express = require('express');
var app = express()
var user = require('./module/user')
var question = require('./module/question')
var questions = require('./module/questions')
var img = require('./module/img')
var xcxuser = require('./module/xcxuser')
var classify = require('./module/classify')
var major = require('./module/major')
var jiesuan = require('./module/jiesuan')
var shopping = require('./module/shopping')
var course = require('./module/course')


// 解决跨域问题
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Content-Type", "text/json; charset=utf-8");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,x-token");
    next()
})


// 头像获取
app.get('/uploads/*', function (req, res) {
    res.sendFile(__dirname + "/" + req.url)
})


app.use('/user', user)
app.use('/question', question)
app.use('/questions', questions)
app.use('/img', img)
app.use('/xcxuser', xcxuser)
app.use('/classify', classify)
app.use('/major', major)
app.use('/jiesuan', jiesuan)
app.use('/shopping', shopping)
app.use('/course', course)


var server = app.listen(2020, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("应用实例，访问地址为 http://%s:%s", host, port)
})