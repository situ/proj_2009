var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../db.js')
var md5 = require('md5-nodejs')
var multer = require('multer');
var router = express.Router()
// 列表
router.get('/list', async function (req, res) {
    var sql = `select * from classify order by id desc`
    const data = await Promise.all([db.doSQL(sql)])
    res.send(db.handerResult(data[0].data))
})
// 新增
router.post('/addList', urlencodedParser, async function (req, res) {
    var {
        type
    } = req.body;
    var sql = `insert into classify (type) values ('${type}')`
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})
// 删除
router.get('/delList', async function (req, res) {
    var ids = req.query.id;
    var sql = `delete from classify where id in (${ids})`;
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})
// 修改
router.post('/updateList', urlencodedParser, async function (req, res) {
    var {
        id,
        type
    } = req.body
    var sql = `update classify set type='${type}' where id='${id}'`
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})
module.exports = router;