var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../db.js')
var md5 = require('md5-nodejs')
var multer = require('multer');
var router = express.Router()



//获取用户自己的课程数据
router.get('/getCourse', async function (req, res) {
    let {
        uid
    } = req.query
    var sql = `select * from course where uid=${uid}`
    const data = await Promise.all([db.doSQL(sql)])
    res.send(db.handerResult(data[0].data))
})


//查询用户
router.get('/getuser', async function (req, res) {
    let {
        uid
    } = req.query
    var sql = `select * from user where id=${uid}`
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})

// 删除
router.get('/delList', async function (req, res) {
    var {
        id
    } = req.query
    var sql = `delete from course where id in (${id})`
    const data = await db.doSQL(sql)
    res.send(db.handerResult(data))
})

// 修改数量
router.post('/upNum', urlencodedParser, async function (req, res) {
    var {
        cid,
        num,
    } = req.body
    var sql = `update course set num='${num}'  where cid='${cid}'`
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})




// 新增课程
router.post('/addCourse', urlencodedParser, async function (req, res) {
    let {
        name,  //课程的名字
        uid,   //登录的用户id
        cid,   //课程id
        price,  //课程价格
        majorcid,  //课程描述
        avatar  //课程图片路径
    } = req.body
    var sql = `insert into course (name, uid, cid, price, majorcid, avatar) values ('${name}', '${uid}', '${cid}', '${price}', '${majorcid}', '${avatar}')`;
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})



module.exports = router;