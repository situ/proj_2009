var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../db.js')
var md5 = require('md5-nodejs')
var multer = require('multer');
var router = express.Router()

// 列表
router.get('/list', async function (req, res) {
    var sql = `select * from major order by id desc`
    const data = await Promise.all([db.doSQL(sql)])
    res.send(db.handerResult(data[0].data))
})
// 新增
router.post('/addList', urlencodedParser, async function (req, res) {
    var {
        major
    } = req.body;
    var sql = `insert into major (major) values ('${major}')`
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})
// 删除
router.get('/delList', async function (req, res) {
    var ids = req.query.id;
    var sql = `delete from major where id in (${ids})`;
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})
// 修改
router.post('/updateList', urlencodedParser, async function (req, res) {
    var {
        id,
        major
    } = req.body
    var sql = `update major set major='${major}' where id='${id}'`
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})
module.exports = router;