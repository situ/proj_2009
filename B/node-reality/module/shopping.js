var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})
var express = require('express')
var db = require('../db.js')
var md5 = require('md5-nodejs')
var multer = require('multer');
var router = express.Router()



//查询商品
router.get('/getShoppoing', async function (req, res) {
    let {
        cid,
        uid
    } = req.query
    var sql = `select * from question where id=${cid}`
    var sql1 = `select * from user where id=${uid}`
    const data = await Promise.all([db.doSQL(sql), db.doSQL(sql1)])
    res.send(db.handeruser(data[0].data, data[1].data))
})



// 删除
router.get('/delList', async function (req, res) {
    var {
        id
    } = req.query
    var sql = `delete from shopping where id in (${id})`
    const data = await db.doSQL(sql)
    res.send(db.handerResult(data))
})

// 修改
router.post('/updateList', urlencodedParser, async function (req, res) {
    var {
        cid,
        num,
    } = req.body
    var sql = `update shopping set num='${num}'  where cid='${cid}'`
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})




// 新增题
router.post('/saveShopping', urlencodedParser, async function (req, res) {
    let {
        uid, num, cid
    } = req.body
    var sql = `insert into shopping (uid,num,cid) values ('${uid}','${num}','${cid}')`;
    const {
        data
    } = await db.doSQL(sql)
    res.send(db.handerResult(data))
})



module.exports = router;