
var express = require('express')
var db = require('../db.js')
var router = express.Router()
var md5 = require('md5-nodejs')
var multer = require('multer');

var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})

//新增地址
router.post("/saveAddress", urlencodedParser, function (req, res) {
    const { name, mobile, area, detail, openid } = req.body
    var sql = `insert into address(name,mobile,area,detail,openid)values ('${name}','${mobile}','${area}','${detail}','${openid}')`
    db.doSQL(sql).then(data => {
        res.send(db.handerResult(data))
    })
})

//获取数据
router.get('/getAddress', function (req, res) {
    var sql = `select * from address where openid='${req.query.openid}'`
    db.doSQL(sql).then(data => {
        res.send(db.handerResult(data))
    })
})

//修改数据
router.post("/updataAddress", urlencodedParser, function (req, res) {
    var {
        id,
        name,
        mobile,
        area,
        detail,
    } = req.body
    var sql = `update address set name='${name}', mobile='${mobile}', area='${area}', detail='${detail}' where id = '${id}' `

    db.doSQL(sql).then(data => {
        res.send(db.handerResult(data))
    })
})


//获取地址
router.get('/getdizhi', function (req, res) {
    var sql = `select * from address where moren=1`
    db.doSQL(sql).then(data => {
        res.send(db.handerResult(data))
    })
})


//获取修改
router.get('/getAddressItem', function (req, res) {
    var sql = `select * from address where id='${req.query.id}'`
    db.doSQL(sql).then(data => {
        res.send(db.handerResult(data))
    })
})

//删除
router.get('/deleteAddress', function (req, res) {
    var sql = `delete from address where id in (${req.query.id})`
    db.doSQL(sql).then(data => {
        res.send(db.handerResult(data))
    })
})

//小程序注册
router.post('/wx-registers', urlencodedParser, async function (req, res) {
    var {
        name,
        loginName,
        loginKey,
    } = req.body;
    // console.log(req.body);
    var sql = `insert into address (name,loginName, loginKey) values ('${name}','${loginName}','${loginKey}')`
    const data = await db.doSQL(sql)
    res.send(db.handerResult(data))
})


//修改默认值
router.post('/updataMoren', urlencodedParser, function (req, res) {
    let {
        oldId,
        newId
    } = req.body
    var sql1 = `update address set moren=0 where id = '${oldId}' `
    var sql2 = `update address set moren=1 where id = '${newId}' `

    Promise.all([db.doSQL(sql1), db.doSQL(sql2)])
        .then(data => {
            res.send(db.handerResult(data))
        })
})





// 小程序登录
var https = require('https')

router.get("/wx-login", function (req, res) {
    const { code } = req.query
    let url = 'https://api.weixin.qq.com/sns/jscode2session?appid=wxb957d3dd22ab9bb8&secret=70ca8d0a97ac94f19f8b69f487cc9c9c&js_code=' + code + '&grant_type=authorization_code'
    let r = https.request(url, function (res2) {
        res2.setEncoding('utf-8')
        res2.on('data', function (chunk) {
            var data = JSON.parse(chunk)
            res.send(data)
        })
    })
    r.end()
})




module.exports = router;