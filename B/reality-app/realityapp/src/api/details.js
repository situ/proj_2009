import request from '@/utils/axios'

// 新增
export let addCourse = (data) => {
    return request({
        url: "/course/addCourse",
        method: "post",
        data
    })
}

// 获取
export let getCourse = (uid) => {
    return request({
        url: "/course/getCourse",
        method: "get",
        params: { uid }
    })
}