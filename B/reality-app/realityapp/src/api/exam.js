import request from '@/utils/axios'

// 获取题库
export let getQuestions = (params) => {
    return request({
        url: "/questions/mode",
        method: "get",
        params
    })
}
