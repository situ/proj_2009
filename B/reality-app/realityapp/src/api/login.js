import request from '@/utils/axios'

//登录
export let userLogin = (data) => {
    return request({
        url: "/user/login",
        method: "post",
        data
    })
}

//获取个人信息
export let getInfo = (token) => {
    return request({
        url: "/user/info",
        method: "get",
        params: { token }
    })
}

//注册个人信息
export let getLogon = (data) => {
    return request({
        url: "/user/registers",
        method: "post",
        data
    })
}

//修改个人信息
export let upAccount = (data) => {
    return request({
        url: "/user/account",
        method: "post",
        data
    })
}

//修改地址信息
export let upArea = (data) => {
    return request({
        url: "/user/xgdizhi",
        method: "post",
        data
    })
}

//修改密码
export let upPass = (data) => {
    return request({
        url: "/user/newPassword",
        method: "post",
        data
    })
}

//退出登录
export let logoutFn = (token) => {
    return request({
        url: "/user/logout",
        method: "post",
        data: { token }
    })
}
