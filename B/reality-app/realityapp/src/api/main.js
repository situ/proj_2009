import request from '@/utils/axios'


//随机获取四条 推荐 数据
export let suggestData = (data) => {
    return request({
        url: "/question/mode",
        method: "get",
        data
    })
}

//随机获取四条 冲刺 数据
export let sprintData = (data) => {
    return request({
        url: "/question/mode",
        method: "get",
        data
    })
}

//随机获取四条 历史真题 数据
export let historyData = (data) => {
    return request({
        url: "/question/mode",
        method: "get",
        data
    })
}

//随机获取四条 核心考点 数据
export let coreData = (data) => {
    return request({
        url: "/question/mode",
        method: "get",
        data
    })
}

//过滤major
export let filterMajor = (data) => {
    // console.log(data);
    return request({
        url: "/question/filterMajor",
        method: "get",
        params: data
    })
}
