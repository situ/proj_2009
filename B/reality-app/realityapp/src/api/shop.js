import request from '@/utils/axios'

//获取用户自己的购物车数据
export let getShop = (uid) => {
    return request({
        url: "/shopping/getShoppoing",
        method: "get",
        params: { uid }
    })
}

//删除
export let delShop = (id) => {
    return request({
        url: "/shopping/delList",
        method: "get",
        params: { id }
    })
}

//改变数量
export let upnum = (data) => {
    return request({
        url: "/shopping/upNum",
        method: "post",
        data
    })
}

//获取用户自己的课程数据
export let getCourse = (uid) => {
    return request({
        url: "/course/getCourse",
        method: "get",
        params: { uid }
    })
}