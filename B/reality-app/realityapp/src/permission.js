import router from './router'
import store from './store'
import { getToken } from '@/utils/cookies'
//黑名单
const balckList = ['/user', '/user/userInfo', '/user/shopCart', '/user/course', '/exam', "/order"];
router.beforeEach(async (to, from, next) => {
    const hasToken = getToken()
    if (hasToken) {   //有token
        if (to.path === '/login') {
            next({ path: '/' })
        } else {
            const hasGetUserInfo = store.getters.user.name
            if (hasGetUserInfo) {
                next()
            } else {
                let res = await store.dispatch('login/getInfo')
                if (res) {
                    next()
                } else {
                    await store.dispatch('login/resetToken')
                    next(`/login?redirect=${to.path}`)
                }
            }
        }
    } else {
        if (balckList.indexOf(to.path) == -1) {
            next()
        } else {
            next(`/login?redirect=${to.path}`)
        }
    }
})