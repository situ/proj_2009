export default [
    { path: "/datas", name: "Datas", component: () => import('@/views/details/datas.vue') },
    { path: "/open", name: "Open", component: () => import('@/views/details/open.vue') },
    { path: "/order", name: "Order", component: () => import('@/views/details/Order.vue') },
]