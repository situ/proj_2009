export default [
    { path: "/exam", name: "Exam", component: () => import('@/views/exam/exam.vue') },
    { path: "/examPoint/:place", name: "ExamPoint", component: () => import('@/views/exam/examPoint.vue') },
    // { path: "/examError", name: "ExamError", component: () => import('@/views/exam/examError.vue') }
]