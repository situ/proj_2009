export default [
    { path: "/login", name: "Login", component: () => import('@/views/login/login.vue') },
    { path: "/logon", name: "Logon", component: () => import('@/views/login/logon.vue') },
    {
        path: "/user", name: "User", component: () => import('@/views/login/user.vue'),
        // children: [
        //     { path: "userInfo", name: "UserInfo", component: () => import('@/views/login/userInfo.vue') }
        // ]

    },
    { path: "/user/userInfo", name: "UserInfo", component: () => import('@/views/login/userInfo.vue') },
    { path: "/user/shopCart", name: "ShopCart", component: () => import('@/views/login/shop.vue') },
    { path: "/user/course", name: "Course", component: () => import('@/views/login/course.vue') }
]