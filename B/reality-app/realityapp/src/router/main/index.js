
export default [
    {
        path: '/',
        name: 'Main',
        component: () => import('@/views/main')
    },
    {
        path: '/mainMajor',
        name: 'MainMajor',
        component: () => import('@/views/main/mainMajor')
    },
    {
        path: '/mainMore',
        name: 'MainMore',
        component: () => import('@/views/main/mainmore')
    },

]