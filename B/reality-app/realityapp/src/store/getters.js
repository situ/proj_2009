const getters = {
    user: state => state.login.user,
    token: state => state.login.token
}
export default getters