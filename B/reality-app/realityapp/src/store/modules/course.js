import { getCourse } from '@/api/shop'

export default {
    namespaced: true,
    state: {
        list: [],
        uid: null
    },
    mutations: {
        SET_UID(state, uid) {
            state.uid = uid
        },
        SET_LIST(state, list) {
            state.list = list
        }
    },
    actions: {
        async getCourse({ commit, state }) {
            let r = await getCourse(state.uid)
            commit('SET_LIST', r.data.data)
            console.log(r)
        },
    },
    modules: {
    }
}