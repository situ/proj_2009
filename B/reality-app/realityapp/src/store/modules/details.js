import { addCourse, getCourse } from '../../api/details.js'


export default {
    namespaced: true,
    state: {
        uidArr: []
    },
    mutations: {
        SET_UIDARR(state, list) {
            state.uidArr = list
            console.log(state.uidArr)
        }
    },
    actions: {
        async addCourse({ state, commit }, data) {
            console.log(data)
            let r = await addCourse(data)

        },
        async getCourse({ state, commit }, uid) {
            // console.log(uid)
            let r = await getCourse(uid)
            // console.log(r)
            commit("SET_UIDARR", r.data.data)
        }
    },
    getters: {}
}
