import { userLogin, getInfo, getLogon, upArea, upAccount, upPass, logoutFn } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/cookies'


export default {
    namespaced: true,
    state: {
        user: {},
        token: getToken()
    },
    mutations: {
        SET_USER(state, user) {
            state.user = user
        }
    },
    actions: {
        async userLogin({ commit }, loginObj) {
            let r = await userLogin({ ...loginObj })

            if (r.data.code == 20000) {
                console.log(r.data.data.token)
                commit('SET_USER', r.data.data)
                setToken(r.data.data.token)
            }
            return r
        },
        async getInfo({ state, commit }) {
            let r = await getInfo(state.token)
            if (r.data.code == 20000) commit('SET_USER', r.data.data);
            return r.data.code == 20000
        },
        resetToken() {
            removeToken()
        },
        async getLogon({ }, data) {
            let r = await getLogon({ ...data })
            console.log(r)
        },
        //修改个人信息
        async upAccount({ }, data) {
            let r = await upAccount(data);
            console.log(r)
        },
        async upArea({ }, data) {
            let r = await upArea(data)
            console.log(r)
        },
        async upPass({ }, data) {
            let r = await upPass(data)
        },
        async logoutFn({ }, token) {
            let r = await logoutFn(token)
        }
    },
    modules: {
    }
}