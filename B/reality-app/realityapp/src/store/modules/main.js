import { suggestData, sprintData, historyData, coreData, filterMajor } from '../../api/main'
import {setMajor} from '@/utils/cookies.js'


export default {
    namespaced: true,
    state: {
        suggestArr: [],//推荐
        sprintArr: [],//冲刺
        historyArr: [],//历史真题
        coreArr: [],//核心考点
        majorArr: [],//分类数组
    },
    mutations: {
        // 核心考点
        GET_CORE(state, { data }) {
            state.coreArr = data.data
        },
        // 历史真题
        GET_HISTORY(state, { data }) {
            state.historyArr = data.data
        },
        // 冲刺
        GET_SPRINT(state, { data }) {
            state.sprintArr = data.data
        },
        // 推荐
        GET_SUGGESTDATA(state, { data }) {
            state.suggestArr = data.data
        },
        // major
        SET_MAJORARR(state, { data }) {
            state.majorArr = data.data
        }
    },
    actions: {
        // 过滤major
        async filterMajorFn({ commit }, name) {
            let { data } = await filterMajor({ major: name })
            commit('SET_MAJORARR', data);
        },
        // 核心考点
        async getCoreData({ commit }) {
            let { data } = await coreData()
            commit('GET_CORE', data)
        },
        // 历史真题
        async getHistoryData({ commit }) {
            let { data } = await historyData()
            commit('GET_HISTORY', data)
        },
        // 冲刺
        async getSprintData({ commit }) {
            let { data } = await sprintData()
            commit('GET_SPRINT', data)
        },
        // 推荐
        async getSuggestData({ commit }) {
            let { data } = await suggestData()
            commit('GET_SUGGESTDATA', data)
        },
    },
}
