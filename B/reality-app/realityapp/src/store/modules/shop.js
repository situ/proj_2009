import { getShop, delShop, upnum } from '@/api/shop'
// import { getToken, setToken, removeToken } from '@/utils/cookies'

export default {
    namespaced: true,
    state: {
        shopList: [],
        numData: {},
        uid: null
    },
    mutations: {
        SET_SHOP(state, shop) {
            state.shopList = shop
            state.shopList.map((v, i) => {
                state.numData['item' + i] = v.num
            })
        },
        SET_UID(state, uid) {
            state.uid = uid
        }
    },
    actions: {
        async getShop({ commit, state }) {
            let r = await getShop(state.uid)
            commit('SET_SHOP', r.data.data)
            // console.log(r)
        },
        async delShop({ dispatch }, id) {
            let r = await delShop(id)
            console.log(r)
            dispatch('getShop')
        },
        async upnum({ dispatch }, data) {
            let r = await upnum(data);
            console.log(r)
            dispatch('getShop')
        }
    },
    modules: {
    }
}