import axios from 'axios'
// npm install axios --save
import qs from 'qs'
import { Promise } from 'core-js'

let request = axios.create({
    baseURL: 'http://localhost:2020',
    timeout: 5000,
})

request.interceptors.request.use(
    config => {
        config.method == 'post' ? config.data = qs.stringify({ ...config.data }) : config.params = { ...config.params };
        config.headers['Content-Type'] = 'application/X-www-form-urlencoded';
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

request.interceptors.response.use(
    response => {
        return response
    },
    error => {
        return Promise.reject(error)
    }
)

export default request