import Cookies from 'js-cookie'

const TokenKey = 'loginToken'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

const majorArr = 'majorArr'

export function getMajor() {
  return localStorage.getItem(majorArr)
}

export function setMajor(data) {
  return localStorage.setItem(majorArr,data)
}

export function removeMajor() {
  return localStorage.removeItem(majorArr)
}
