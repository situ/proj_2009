
let pageSize = 3;
let curpPage = 1;
let total = 0;
let allPage = 0;
let cookieGet = $.cookie('loginToken');
var baseUrl = "http://localhost:2020";

window.onload = function () {
    getDataList();
    isToken();
};

//获取数据
function getDataList() {
    var baseUrl = "http://localhost:2020";
    $.ajax({
        url: baseUrl + "/question/getQuestions",
        type: 'get',
        data: {
            pageSize: pageSize,
            currentPage: curpPage - 1,
        },
        success: function (res) {
            // tableDate = res.data;
            total = res.total[0].total
            localStorage.setItem("tableDate", JSON.stringify(res.data))
            renderTab(res.data);//渲染列表
            tabPage();//渲染分页
        }
    })
}

//渲染页面
function renderTab(data) {
    var baseUrl = "http://localhost:2020";
    // console.log(data)
    if (!data.length) {
        $('.nodataBox').show();
        $('.havedataBox').hide();
        $('.pageBig').css('display', 'none');
        $('.allresult').text(0);//搜索的结果
        return;
    } else {
        $('.nodataBox').hide();
        $('.havedataBox').show();
        $('.pageBig').css('display', 'block');
        $('.allresult').text(total);//搜索的结果
    }
    var curData = [];
    $(data).each(function (i, v) {
        curData.push(`<li class="cargLi">
                <a href="./listencourse.html">
                    <div class="cargLi_top">
                        <img src="${baseUrl}/uploads/${v.avatar}" alt="">
                        <i class="fontSize12 color000">1门课</i>
                    </div>
                    <div class="cargLi_bottom">
                        <span class="fontSize16 cargLi_bot_title">${v.name}</span>
                        <div class="cargLi_bot_footer flex">
                            <span class="fontSize16 colore74c3c">¥${v.price}</span>
                            <span class="fontSize12 color999">
                                <span>套餐</span>
                                <span>学习：${v.studyNum}</span>
                            </span>
                        </div>
                    </div>
                </a>
            </li>`)
    });
    $('.conterUl').html(curData.join(''));
}

// 分页
function tabPage() {
    allPage = Math.ceil(total / pageSize);//总页数
    // console.log(allPage + "总页数");
    $('.pageNum').prop('max', allPage)//给input设置最大输入页数

    if (allPage <= 5) {
        var pageList = '';
        for (var i = 0; i < allPage * 1; i++) {
            var classname = i + 1 == curpPage ? "Pageactive" : "";
            pageList += '<li class="' + classname + '" dataNum="' + (i + 1) + '">' + (i + 1) + '</li>';
        }
    } else if (curpPage - 2 <= 1) {
        console.log('前')
        var pageList = '';
        for (var i = 0; i < 5; i++) {
            var classname = i + 1 == curpPage ? "Pageactive" : "";
            pageList += '<li class="' + classname + '" dataNum="' + (i + 1) + '">' + (i + 1) + '</li>';
        }
    } else if (curpPage - 2 > 1 && allPage - curpPage >= 2) {
        // console.log('中间')
        var pageList = '';
        for (var i = curpPage * 1 - 2; i < curpPage * 1 + 2 + 1; i++) {
            var classname = i == curpPage ? "Pageactive" : "";
            pageList += '<li class="' + classname + '" dataNum="' + i + '">' + i + '</li>';
        }
    } else {
        // console.log('后')
        var pageList = '';
        for (var i = allPage * 1 - 4; i < allPage * 1 + 1; i++) {
            var classname = i == curpPage ? "Pageactive" : "";
            pageList += '<li class="' + classname + '" dataNum="' + i + '">' + i + '</li>';
        }
    }
    $('.pageNav').html(pageList);
}

// 标签页的点击事件
$('.pageNav').on('click', "li", function () {
    $(this).addClass('Pageactive').siblings().removeClass('Pageactive');
    curpPage = $(this).attr('datanum');
    curpPageChange();
});

// 下一页
$('.next').on("click", function () {
    if (curpPage == allPage) {
        return;
    }
    curpPage++;
    curpPageChange();
})

//上一页
$('.prev').on("click", function () {
    if (curpPage == 1) {
        return;
    }
    curpPage--;
    curpPageChange();
})

//第一页
$('.icon--1').on('click', function () {
    curpPage = 1;
    curpPageChange();
})

//最后一页
$('.icon--').on('click', function () {
    curpPage = allPage;
    curpPageChange();
})

// curpPage当前页放生变化触发
function curpPageChange() {
    if (curpPage == 1) {
        $('.prev').addClass('notallowed');
        $('.icon--1').addClass('notallowed');
    } else {
        $('.prev').removeClass('notallowed');
        $('.icon--1').removeClass('notallowed');
    }

    if (curpPage == allPage) {
        $('.next').addClass('notallowed');
        $('.icon--').addClass('notallowed');
    } else {
        $('.next').removeClass('notallowed');
        $('.icon--').removeClass('notallowed');
    }
    getDataList();
}
//input输入跳转页面
$('.pageJump').on('click', function () {
    if ($('.pageNum').val() > allPage) {
        alert('总页数：' + allPage + '条');
        return;
    }
    curpPage = $('.pageNum').val();
    getDataList();
    $('.pageNum').val('')
})

//搜索
$('.selectBtn').click(function () {
    var tableDate = JSON.parse(localStorage.getItem('tableDate'))
    var selectValue = $('.selectIPt').val().trim();
    if (!selectValue) {
        alert('请输入要搜索的内容');
        return;
    }
    var selectArr = tableDate.filter(function (v) {
        return v.major.indexOf(selectValue) > -1;

    })
    renderTab(selectArr)
    $('.allresult').text(selectArr.length);//搜索的结果
    curpPage = 0;
})
//搜索失焦
$('.selectIPt').blur(function () {
    var tableDate = JSON.parse(localStorage.getItem('tableDate'));
    if (!$('.selectIPt').val().trim()) {
        console.log(tableDate)
        renderTab(tableDate);
    }
})

//多重过滤
$('.tabBox').on("click", 'li', function () {
    var otherli = $(this).parent().parent().siblings().children('ul').find('.alltype');
    $(otherli).addClass('active');
    $(this).removeClass('active');
    var tableDate = JSON.parse(localStorage.getItem('tableDate'));
    var selectValue = $(this).text()
    var val;
    var selectArr;
    var classify = $(this).parent().siblings().text();
    switch (classify) {
        case "专业：":
            val = "major";
            break;
        case "类型：":
            val = "type";
            break;
        case "讲师：":
            val = "author";
            break;
        case "年份：":
            val = "year";
            break;
    }
    if (selectValue == "全部") {
        renderTab(tableDate)
    } else {
        selectArr = tableDate.filter(function (v) {
            return v[val].indexOf(selectValue) > -1;

        })
        renderTab(selectArr)
        $('.allresult').text(selectArr.length);//搜索的结果
    }
})



//点击讲师
$('.teacherlibox').on('click', function (event) {
    // $(this).css('backgroundColor','#F8F8F8');
    // $('.teacherpullBox').toggle();
    $(this).addClass('active');
    $('.teacherpullBox').toggleClass('showDiv');
    event.stopPropagation()
});
$(document).click(function () {
    // $('.teacherpullBox').css('display','none');
    $('.teacherlibox').removeClass('active');
    $('.teacherpullBox').removeClass('showDiv');
})

// 筛选
$('#typeUl li').on('click', function () {
    $(this).addClass('active').siblings().removeClass('active');
});
$('#majorUl li').on('click', function () {
    $(this).addClass('active').siblings().removeClass('active');
});
$('#teacherUl li').on('click', function () {
    $(this).addClass('active').siblings().removeClass('active');
});
$('#yeareUl li').on('click', function () {
    $(this).addClass('active').siblings().removeClass('active');
});

//底部hover
$('.iponePosition').hover(function () {
    $('.ewmphone').toggle()
})
$('.weixinPosition').hover(function () {
    $('.ewmWeixin').toggle()
})

//返回顶部
$('.goTop').hide();
$(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
        $('.goTop').fadeIn();
    } else {
        $('.goTop').fadeOut();
    }
});
$('.goTop').click(function () {
    console.log(1)
    $('html ,body').animate({ scrollTop: 0 }, 300);
})

// 退出登录，显示遮罩层
$('.logout').on('click', function () {
    $('.logout-wp').show()
})

$('.close').on('click', function () {
    $('.logout-wp').hide()
})

$('.define').on('click', function () {
    logoutFn(cookieGet)
    $.cookie('loginToken', '');
    $('.logout-wp').hide()
    isToken()
})

// 是否有token
function isToken() {
    if (!$.cookie('loginToken')) {
        $('.login').show()
        $('.logon').show()
        $('.userliBox').hide()
    } else {
        $('.login').hide()
        $('.logon').hide()
        $('.userliBox').show()
    }
}

//退出登录
function logoutFn(token) {
    $.ajax({
        url: baseUrl + '/user/logout',
        type: 'post',
        data: {
            token
        },
        success: function (res) {
            console.log(res)
        }
    })
}

// 导航条的点击事件
$('.teacherlibox').on('click', function () {
    $('.teacherpullBox').toggle()
})

// 用户的点击事件
$('.userliBox').on('click', function () {
    $('.userpullBox').toggle()
})

// 点击body导航条里的ol隐藏
$('body').on('click', function (e) {
    if (e.target.className != "teacherlibox") {
        $('.teacherpullBox').hide()
    }
    if (e.target.className != "userliBox") {
        $('.userpullBox').hide()
    }
})



