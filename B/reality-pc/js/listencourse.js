$(".listencTable li").click(function(){
    var index=$(this).index();
    console.log(index)
    $(this).addClass('listeractive').siblings().removeClass('listeractive');
   $('.listencTablexiangq li').eq(index).show().siblings().hide();
})

let cookieGet = $.cookie('loginToken');
var baseUrl = "http://localhost:2020";

isToken()

// 退出登录，显示遮罩层
$('.logout').on('click', function () {
    $('.logout-wp').show()
})

$('.close').on('click', function () {
    $('.logout-wp').hide()
})

$('.define').on('click', function () {
    logoutFn(cookieGet)
    $.cookie('loginToken', '');
    $('.logout-wp').hide()
    isToken()
})

   //退出登录
   function logoutFn(token) {
    $.ajax({
        url: baseUrl + '/user/logout',
        type: 'post',
        data: {
            token
        },
        success: function (res) {
            console.log(res)
        }
    })
}

// 是否有token
function isToken() {
    if (!$.cookie('loginToken')) {
        $('.login').show()
        $('.logon').show()
        $('.userliBox').hide()
    } else {
        $('.login').hide()
        $('.logon').hide()
        $('.userliBox').show()
    }
}

// 导航条的点击事件
$('.teacherlibox').on('click', function () {
    $('.teacherpullBox').toggle()
})

// 用户的点击事件
$('.userliBox').on('click', function () {
    $('.userpullBox').toggle()
})

// 点击body导航条里的ol隐藏
$('body').on('click', function (e) {
    if (e.target.className != "teacherlibox") {
        $('.teacherpullBox').hide()
    }
    if (e.target.className != "userliBox") {
        $('.userpullBox').hide()
    }
})