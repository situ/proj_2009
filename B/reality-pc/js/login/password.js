; (function (window, document, $, undefined) {

    if (!$.cookie('loginToken')) {
        location.href = "../../html/login.html"
        return
    }

    var baseUrl = 'http://localhost:2020',   //端口地址
        $pcLoginBtn = $('.ls-login-botton'),//确定按钮
        $pcUserName = $('.ls-login-username'),//旧密码input框
        $pcUserKey = $('.ls-login-userkey'),//新密码input框
        $logAlert = $('.login-alert'),     //弹框
        $logAlertSpan = $('.login-alert span'), //弹框文本
        cookieGet = $.cookie('loginToken');
    var id, loginKey;

    userinfo(cookieGet)


    $pcLoginBtn.on('click', function () {
        if ($pcUserName.val() && $pcUserKey.val()) {
            if ($pcUserName.val().length < 4 || $pcUserKey.val().length < 4) {
                alertFn('密码长度不得小于4')
                return
            } else {
                // console.log(id, loginKey)
                if ($pcUserName.val() != loginKey) {
                    alertFn('旧密码输入不正确')
                    return
                }
                if ($pcUserKey.val() == loginKey) {
                    alertFn('新密码和旧密码相同')
                    return
                }
                newPassword({ id, loginKey: $pcUserKey.val() })
                $logAlert.css({ "color": "#67c23a", "background-color": "#f0f9eb" })
                $logAlert.html('恭喜你修改密码成功')
                $logAlert.show()
                setTimeout(() => {
                    $logAlert.hide()
                    location.href = "../../html/login.html"
                }, 1500)
            }
        } else {
            alertFn('密码信息不得为空')
            return
        }
    })

    $('.form-item-ipt').each(function (i) {
        $('.form-item-ipt').eq(i).on('focus', function () {
            $logAlert.hide()
        })
    })

    function alertFn(val) {
        $logAlertSpan.html(val)
        $logAlert.show()
    }

    //修改密码
    function newPassword(data) {
        $.ajax({
            url: baseUrl + '/user/newPassword',
            type: 'post',
            data,
            success: function (res) {
                console.log(res)
            }
        })
    }

    //获取个人信息
    function userinfo(token) {
        $.ajax({
            url: baseUrl + '/user/info',
            type: 'get',
            data: {
                token
            },
            success: function (res) {
                id = res.data.id
                loginKey = res.data.loginKey
            }
        })
    }

    $('.logout-btn').on('click', function () {
        $('.logout-wp').show()
    })


    //退出登录
    function logoutFn(token) {
        $.ajax({
            url: baseUrl + '/user/logout',
            type: 'post',
            data: {
                token
            },
            success: function (res) {
                console.log(res)
            }
        })
    }

    $('.close').on('click', function () {
        $('.logout-wp').hide()
    })

    $('.define').on('click', function () {
        logoutFn(cookieGet)
        $.cookie('loginToken', '');
    })

    

}(window, document, jQuery))