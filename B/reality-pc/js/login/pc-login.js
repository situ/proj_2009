; (function (window, document, $, undefined) {
    var baseUrl = 'http://localhost:2020'
    var $pcLoginBtn = $('.ls-login-botton'),//登录按钮
        $pcUserName = $('.ls-login-username'),//账号input框
        $pcUserKey = $('.ls-login-userkey'),//密码input框
        $logAlert = $('.login-alert'),     //弹框
        $logAlertSpan = $('.login-alert span'); //弹框文本


    $pcLoginBtn.on('click', function () {
        if ($pcUserName.val() && $pcUserKey.val()) {
            if ($pcUserName.val().length < 4 || $pcUserKey.val().length < 4) {
                alertFn('账号或密码长度不得小于4')
            } else {
                console.log($pcUserName.val(), $pcUserKey.val())
                $.ajax({
                    url: baseUrl + '/user/login',
                    type: 'post',
                    data: {
                        loginName: $pcUserName.val(),
                        loginKey: $pcUserKey.val()
                    },
                    success: function (res) {
                        // console.log(res.data.token)
                        if (res.code !== 20000) {
                            alertFn('账号或者密码输入不正确')
                            return
                        }
                        $.cookie('loginToken', res.data.token, { expires: 7 })
                        if ($.cookie('loginToken') && $.cookie('loginToken').length == 32) {
                            location.href = "../html/index.html"
                        } else {
                            alertFn('本地不可以存取token')
                        }
                        $pcUserName.val('');
                        $pcUserKey.val('');
                    }
                })
            }
        } else {
            alertFn('账号或密码不得为空')
        }
    })

    $('.form-item-ipt').each(function (i) {
        $('.form-item-ipt').eq(i).on('focus', function () {
            $logAlert.hide()
        })
    })

    function alertFn(val) {
        $logAlertSpan.html(val)
        $logAlert.toggle()
    }


}(window, document, jQuery))