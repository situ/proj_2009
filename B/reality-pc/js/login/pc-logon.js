; (function (window, document, $, undefined) {
    var baseUrl = 'http://localhost:2020'
    var $pcLogonBtn = $('.ls-logon-botton'),//注册按钮
        $pcUserName = $('.ls-logon-username'),//账号input框  logon-password
        $pcUserPass = $('.ls-logon-password'),//密码input框  
        $pcUserKey = $('.ls-logon-userkey'),//确认密码input框
        $isTrues = $('#isTrues'),  //是否阅读
        $logAlert = $('.login-alert'),
        $logAlertSpan = $('.login-alert span');

    $pcLogonBtn.on('click', function () {
        if ($pcUserName.val() && $pcUserKey.val() && $pcUserPass.val()) {
            if ($pcUserName.val().length < 4 || $pcUserKey.val().length < 4) {
                alertFn('账号或密码长度不得小于4')
                return
            } else if ($pcUserKey.val() != $pcUserPass.val()) {
                alertFn('密码不一致请重新输入')
                return
            } else if (!$isTrues.prop('checked')) {
                alertFn('请您在阅读注册协议后同意并勾选')
                return
            } else {
                $.ajax({
                    url: baseUrl + '/user/saveUser',
                    type: 'post',
                    data: {
                        loginName: $pcUserName.val(),
                        loginKey: $pcUserPass.val()
                    },
                    success: function (res) {
                        if (res.code == 20000) {
                            // location.href = "../html/login.html?success=" + data
                            location.href = "../html/login.html"
                            $pcUserName.val('');
                            $pcUserKey.val('');
                            $pcUserPass.val('');
                        }
                    }
                })
            }
        } else {
            alertFn('请完整填写信息')
        }
    })

    $('.form-item-ipt').each(function (i) {
        $('.form-item-ipt').eq(i).on('focus', function () {
            $logAlert.hide()
        })
    })

    $isTrues.on('change', function (e) {
        if ($isTrues.prop('checked')) {
            $logAlert.hide()
        }
    })

    function alertFn(val) {
        $logAlertSpan.html(val)
        $logAlert.toggle()
    }

}(window, document, jQuery))