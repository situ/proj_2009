; (function (window, document, $, undefined) {

    if (!$.cookie('loginToken')) {
        location.href = "../../html/login.html"
        return
    }

    let cookieGet = $.cookie('loginToken'),
        baseUrl = 'http://localhost:2020',
        name = $('.name'),
        age = $('.age'),
        gender = $('[name=gender]:checked'),
        edu = $('#type');
    var userObj;

    var $pcLoginBtn = $('.ls-login-botton'),//确定按钮
        $pcUserName = $('.ls-login-username'),//旧密码input框
        $pcUserKey = $('.ls-login-userkey'),
        $logAlert = $('.login-alert'),     //弹框
        $logAlertSpan = $('.login-alert span'); //弹框文本
    var id, loginKey;
    userinfo(cookieGet)
    $pcUserName.val('')
    $pcUserKey.val('')



    // tab切换 
    $('.titleTop ul li').on('click', function () {
        let idx = $(this).index();
        $(this).addClass('onlineTitleActive').siblings().removeClass('onlineTitleActive')
        $('.onlineMain>div').eq(idx).fadeIn().siblings().fadeOut()
    })

    //编辑按钮
    $('.updateBtn').on('click', function () {
        let arr = [userObj.name, userObj.loginName, userObj.gender, userObj.age, userObj.edu, userObj.area];
        if (arr[4]) {
            $('#edu option[value=' + (arr[4] || '') + ']').attr('selected', true);
        }
        if (arr[2]) {
            $('[name=gender][value=' + (arr[2] || '') + ']').attr('checked', true);
        }
        $('#name').val(arr[0] || '');
        $('#age').val(arr[3] || '');
        // $('#edu option[value=' + (arr[4] || '') + ']').attr('selected', true);
        $('.logout-wp').show();
    })

    //三级联动
    let $sheng = $("#sheng"),
        datas = data,
        wxArr = [];

    $("#sheng").on("change", function () {
        var value = $(this);
        chang($("#shi"), datas, value)
    })

    $("#shi").on("change", function () {
        var value = $(this);
        chang($("#qu"), wxArr, value)
    })

    $("#qu").on("change", function () {
        if ($(this).val() == "0") {
            chang(1, 2, $(this))
        }
    })

    xrsel($sheng, datas)

    function chang(a, b, c) {
        if (c.val() == "0" || a == 1) {
            c.siblings().val("0")
            return;
        }
        wxArr = b.filter(function (v, i) {
            return v.value == c.val();
        })[0].children;
        xrsel(a, wxArr)
    }

    function xrsel(a, b) {
        var wxArr1 = [];
        wxArr1.push("<option value='0'>请选择</option>")
        for (var i = 0; i < b.length; i++) {
            wxArr1.push("<option data-city='", b[i].text, "' value=", b[i].value, ">", b[i].text, "</option>")
        }
        a.html(wxArr1.join(""));
    }

    //取消关闭按钮
    $('.close').on('click', function () {
        $('.logout-wp').hide()
        $('form').get(0).reset()
    })

    //个人中心确定按钮
    $('.define').on('click', function () {
        let sheng = $("#sheng option:checked").attr("data-city"),
            shi = $("#shi option:checked").attr("data-city"),
            qu = $("#qu option:checked").attr("data-city");
        let form = {
            id: userObj.id,
            name: name.val(),
            age: age.val(),
            gender: $('[name=gender]:checked').val(),
            edu: $('#edu option:checked').val(),
            area: [sheng, shi, qu]
        }
        if (
            name.val().length &&
            age.val().length &&
            $('[name=gender]:checked').val() &&
            $('#edu option:checked').val() &&
            sheng && shi && qu
        ) {
            account(form)
            xuanUser(form)
            let obj = { ...form }
            xgdizhi(obj)
            $('.logout-wp').hide()
            $logAlert.css({ "color": "#67c23a", "background-color": "#f0f9eb" })
            $logAlert.html('恭喜你修改成功')
            $logAlert.show()
            setTimeout(() => {
                $logAlert.hide()
                $logAlert.css({ "color": "#f40", "background-color": "#fff7eb" })
            }, 1500)
        }
        else {
            alert('请正确填写信息')
            return
        }
    })

    //input框输入时候让提示框消失
    $('.form-item-ipt').each(function (i) {
        $('.form-item-ipt').eq(i).on('focus', function () {
            $('.login-alert').hide()
        })
    })

    //修改密码确定
    $pcLoginBtn.on('click', function () {
        if ($pcUserName.val() && $pcUserKey.val()) {
            if ($pcUserName.val().length < 4 || $pcUserKey.val().length < 4) {
                alertFn('密码长度不得小于4')
                return
            } else {
                if ($pcUserName.val() != loginKey) {
                    $logAlert.css({ "color": "#f40", "background-color": "#fff7eb" })
                    alertFn('旧密码输入不正确')
                    return
                }
                if ($pcUserKey.val() == loginKey) {
                    alertFn('新密码和旧密码相同')
                    return
                }
                newPassword({ id, loginKey: $pcUserKey.val() })
                $logAlert.css({ "color": "#67c23a", "background-color": "#f0f9eb" })
                $logAlert.html('恭喜你修改密码成功')
                $logAlert.show()
                setTimeout(() => {
                    $logAlert.hide()
                    logoutFn(cookieGet)
                    $.cookie('loginToken', '');
                    location.href = "./login.html"
                    // location.href = "../../html/login.html"
                }, 1500)
            }
        } else {
            alertFn('密码信息不得为空')
            return
        }
    })

    //获取个人信息
    function userinfo(token) {
        $.ajax({
            url: baseUrl + '/user/info',
            type: 'get',
            data: {
                token
            },
            success: function (res) {
                userObj = res.data
                userObj.area = JSON.parse(userObj.area || '[]');
                id = res.data.id
                loginKey = res.data.loginKey
                if (userObj.area) {
                    $(`#sheng option[data-city=${userObj.area[0]}]`).attr('selected', true)
                    chang($("#shi"), datas, $('#sheng'))
                    $(`#shi option[data-city=${userObj.area[1]}]`).attr('selected', true)
                    chang($("#qu"), wxArr, $("#shi"))
                    $(`#qu option[data-city=${userObj.area[2]}]`).attr('selected', true)
                }
                xuanUser(userObj)
            }
        })
    }

    //退出登录
    function logoutFn(token) {
        $.ajax({
            url: baseUrl + '/user/logout',
            type: 'post',
            data: {
                token
            },
            success: function (res) {
                console.log(res)
            }
        })
    }

    //修改个人信息
    function account(data) {
        $.ajax({
            url: baseUrl + '/user/account',
            type: 'post',
            data,
            success: function (res) {
                console.log(res)
                userinfo(cookieGet)
            }
        })
    }

    //渲染函数
    function xuanUser(data) {
        let arr = [data.name, data.loginName, data.gender, data.age, data.edu, data.area]
        $('.usertext-right ul li').each(function (i) {
            $(this > 'span').text()
            $('.usertext-right ul li span').eq(i).text(arr[i])
        })
    }

    //修改地址
    function xgdizhi(data) {
        data.area = JSON.stringify(data.area)
        $.ajax({
            url: baseUrl + '/user/xgdizhi',
            type: 'post',
            data,
            success: function (res) {
                console.log(res)
                userinfo(cookieGet)
            }
        })
    }

    //提示框
    function alertFn(val) {
        $logAlertSpan.html(val)
        $logAlert.show()
    }

    //修改密码
    function newPassword(data) {
        $.ajax({
            url: baseUrl + '/user/newPassword',
            type: 'post',
            data,
            success: function (res) {
                console.log(res)
            }
        })
    }

}(window, document, jQuery))