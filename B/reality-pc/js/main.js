$(function () {
    var i = 0,
        timer,
        containerImgWid = $('.containerImg').width(),
        suggestArr = [],
        cookieGet = $.cookie('loginToken');

    $('.imgItem').width(containerImgWid)
    $('.imgItem>img').width(containerImgWid)
    $('.onlineMain>.dis_b').fadeIn().siblings().fadeOut()

    let baseUrl = 'http://localhost:2020'

    isToken()

    // 退出登录，显示遮罩层
    $('.logout').on('click', function () {
        $('.logout-wp').show()
    })

    $('.close').on('click', function () {
        $('.logout-wp').hide()
    })

    $('.define').on('click', function () {
        logoutFn(cookieGet)
        $.cookie('loginToken', '');
        $('.logout-wp').hide()
        isToken()
    })


    $.ajax({
        url: `${baseUrl}/question/mode`,
        type: 'get',
        timeout: 5000,
        success: ({ data }) => {
            suggestArr = data.data
            renderSuggestBoxFn()
        }
    })

    // 自动轮播
    $('.lunboBottom li').eq(i).addClass('lunboBottomActive').siblings().removeClass('.lunboBottomActive')    //轮播点设置样式
    timerFn()
    // 鼠标划入事件
    $('.lunboBottom li').on('mouseenter', function () {
        clearInterval(timer)
        let idx = $('.lunboBottom>li').index($(this))
        i = idx
        $(this).addClass('lunboBottomActive').siblings().removeClass('lunboBottomActive')
        $('.imgItems').animate({
            marginLeft: -containerImgWid * idx + 'px'
        }, 'slow')
    })
    // 鼠标离开事件
    $('.lunboBottom>li').on('mouseout', function () {
        timerFn()
    })
    // 向左的点击事件
    $('.goLeft').on('click', function () {
        clearInterval(timer)
        timer = null
        i = i - 1
        if (i == -1) {
            i = $('.titleTop ul li').length
        }
        $('.lunboBottom li').eq(i).addClass('lunboBottomActive').siblings().removeClass('lunboBottomActive')
        $('.imgItems').animate({
            marginLeft: -containerImgWid * i + 'px'
        }, 'slow')
    })
    // 向左的划出事件
    $('.goLeft').on('mouseout', function () {
        if (!timer) {
            timerFn()
        }
    })
    // 向右的点击事件
    $('.goRight').on('click', function () {
        clearInterval(timer)
        timer = null
        i = i + 1
        if (i > $('.titleTop ul li').length) {
            i = 0
        }
        $('.lunboBottom li').eq(i).addClass('lunboBottomActive').siblings().removeClass('lunboBottomActive')
        $('.imgItems').animate({
            marginLeft: -containerImgWid * i + 'px'
        }, 'slow')
    })
    // 向右的划出事件
    $('.goRight').on('mouseout', function () {
        if (!timer) {
            timerFn()
        }
    })

    // 导航条的点击事件
    $('.teacherlibox').on('click', function () {
        $('.teacherpullBox').toggle()
    })

    // 用户的点击事件
    $('.userliBox').on('click', function () {
        $('.userpullBox').toggle()
    })

    // 点击body导航条里的ol隐藏
    $('body').on('click', function (e) {
        if (e.target.className != "teacherlibox") {
            $('.teacherpullBox').hide()
        }
        if (e.target.className != "userliBox") {
            $('.userpullBox').hide()
        }
    })


    // tab切换  中、西医、健康管理师
    $('.titleTop ul li').on('click', function () {
        let idx = $(this).index();
        $(this).addClass('onlineTitleActive').siblings().removeClass('onlineTitleActive')
        $('.onlineMain>div').eq(idx).fadeIn().siblings().fadeOut()
    })

    // 点击推荐图书，跳转页面
    $('.suggestMain').on('click', '.suggestItem', function () {
        let id = $(this).attr('id');
        window.open(`./mainSuggest.html?bookId=${id}`, '_self')
    })


    //退出登录
    function logoutFn(token) {
        $.ajax({
            url: baseUrl + '/user/logout',
            type: 'post',
            data: {
                token
            },
            success: function (res) {
                console.log(res)
            }
        })
    }

    // 是否有token
    function isToken() {
        if (!$.cookie('loginToken')) {
            $('.login').show()
            $('.logon').show()
            $('.userliBox').hide()
        } else {
            $('.login').hide()
            $('.logon').hide()
            $('.userliBox').show()
        }
    }

    // 渲染推荐图书页面
    function renderSuggestBoxFn() {
        let arr = []
        for (let i = 0; i < suggestArr.length; i++) {
            arr.push(`
                <div class="masterItem suggestItem" id="${suggestArr[i].id}">
                    <div>
                        <div class="pad_t10_b10 mar_l10_r10 textAli_C">
                            <div class="suggestTitle">${suggestArr[i].name}</div>
                            <div class="margin_t20">
                                <span class="color_ffa200">
                                    <small class="font_12">￥</small><i class="font_14 fontStyle_inherit">${suggestArr[i].price}</i>
                                </span>
                                <s class="margin_l10 color_999 font_14">
                                    <span class="fontWb">￥</span><i class="fontStyle_inherit">${suggestArr[i].Originalprice}</i>
                                </s>
                            </div>
                            <div class="margin_t20 margin_b20">
                                <img class="book" src="../img/suggestBook.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            `)
        }
        $('.suggestMain').html(arr.join(''))
    }

    // 计时器
    function timerFn() {
        timer = setInterval(function () {
            i++
            if (i == $('.lunboBottom li').length) {
                i = 0
            }
            showImgItem()
        }, 2000)
    }

    // 展示图片
    function showImgItem() {
        $('.imgItems').animate({
            marginLeft: -containerImgWid * i + 'px'
        }, 'slow')
        $('.lunboBottom li').eq(i).addClass('lunboBottomActive').siblings().removeClass('lunboBottomActive')
    }

})