
let pageSize = 4
let currentPage = 0
let ress = undefined
let allYe = undefined
let cookieGet = $.cookie('loginToken');
let baseUrl = 'http://localhost:2020',
    sort = "desc",
    orderby = "id",
    keyword = "";

isToken()

// 退出登录，显示遮罩层
$('.logout').on('click', function () {
    $('.logout-wp').show()
})

$('.close').on('click', function () {
    $('.logout-wp').hide()
})

$('.define').on('click', function () {
    logoutFn(cookieGet)
    $.cookie('loginToken', '');
    $('.logout-wp').hide()
    isToken()
})

function logoutFn(token) {
    $.ajax({
        url: baseUrl + '/user/logout',
        type: 'post',
        data: {
            token
        },
        success: function (res) {
            console.log(res)
        }
    })
}

// 是否有token
function isToken() {
    if (!$.cookie('loginToken')) {
        $('.login').show()
        $('.logon').show()
        $('.userliBox').hide()
    } else {
        $('.login').hide()
        $('.logon').hide()
        $('.userliBox').show()
    }
}

// 用户的点击事件
$('.userliBox').on('click', function () {
    $('.userpullBox').toggle()
})
getList()
/// 获取数据
function getList() {
    console.log(keyword)
    $.ajax({
        url: 'http://localhost:2020/question/getQuestions',
        type: 'get',
        data: {
            pageSize,
            currentPage,
            sort,
            orderby,
            keyword
        },
        async: false,
        success(res) {
            // console.log(res.total[0].total)
            console.log(res)
            ress = res
            renderList()
            // renderPage()
        }
    })
}

//// 排序
$(".time").on("click", function () {
    if (sort == "desc") {
        sort = "asc"
    } else {
        sort = "desc"
    }
    orderby = "publishTime"
    getList()
})

$(".price").on("click", function () {
    if (sort == "desc") {
        sort = "asc"
    } else {
        sort = "desc"
    }
    orderby = "price"
    getList()
})


// renderList()
//// 渲染详情
function renderList() {
    console.log(ress)
    var arr = []
    for (var i = 0; i < ress.data.length; i++) {
        arr.push('<div class="cont-item">\
       <img src="http://localhost:2020/uploads/'+ ress.data[i].avatar + '"\
           alt="">\
       <div class="conent">\
           <div class="conent-item">\
               <span>'+ ress.data[i].name + '</span>\
               <p style="margin-top:15px">售价：\
                   <b>'+ ress.data[i].price + '</b>\
                   <span class="sp">定价：</span>\
                   <s style="color: #cccccc79;">999.0</s>\
               </p>\
           </div>\
           <div class="conent-brief">\
               <ul>\
                   <li>作 者：</li>\
                   <li>出版社：</li>\
                   <li>出版时间：</li>\
                   <li>分类：</li>\
               </ul>\
               <ul>\
                   <li>唯实教育考试研究院</li>\
                   <li>唯实教育内部专享</li>\
                   <li>'+ ress.data[i].publishTime + '</li>\
                   <li>'+ ress.data[i].type + '</li>\
               </ul>\
           </div>\
       </div>\
       <div class="conent-right">\
           <p class="lookXq" data-id='+ ress.data[i].id + '>查看详情</p>\
           <p class="buy" data-id='+ ress.data[i].id + '>立即购买</p>\
       </div>\
   </div>')
    }
    $(".cont-item-warp").html(arr.join(" "))
    $(".lookXq").on("click", function () {
        let id = $(this).attr("data-id")
        location.href = "./materialdetails.html?id=" + id
    })
    $(".buy").on("click", function () {
        let id = $(this).attr("data-id")
        location.href = "./materialdetails.html?id=" + id
    })
}


renderPage()
// 渲染分页
function renderPage() {
    var pageArr = [];
    // allYe = Math.ceil(res / pageB);
    let tol = ress.total[0].total / pageSize
    for (var i = 0; i < tol; i++) {
        pageArr.push("<li class='por'>", i + 1, "</li>");
    }
    $(".pageNav").html(pageArr.join(""));
    $(".pageNav li:first").addClass("Pageactive")
    $(".pageNav li").on("click", function () {
        $(this).addClass("Pageactive").siblings().removeClass("Pageactive")
    })
}

$(".pageNav li").on("click", function () {
    currentPage = $(this).html() - 1
    getList()
})

$(".prev").on("click", function () {
    if (currentPage == 0) return;
    currentPage--
    getList()
})

$(".next").on("click", function () {
    if (currentPage == ress.total[0].total) return;
    currentPage++
    getList()
})

$(".pageJump").on("click", function () {
    console.log(1)
    currentPage = $(".pageNum").val() - 1
    getList()
    // renderList()
})


$(".filter li").on("click", function () {
    let val = $(this).html()
    $(this).addClass("active").siblings().removeClass("active")
    keyword = val
    currentPage = 0
    if (val == "全部") {
        keyword = ""
    }
    getList()
    // renderList()
})

