let ress = undefined
let cookieGet = $.cookie('loginToken');
let baseUrl = 'http://localhost:2020';
let id = location.search.substring(4, 6)

$('.two-btn').on("click", function () {
    window.open("../html/open.html")
})

isToken()
// 退出登录，显示遮罩层
$('.logout').on('click', function () {
    $('.logout-wp').show()
})

$('.close').on('click', function () {
    $('.logout-wp').hide()
})

$('.define').on('click', function () {
    logoutFn(cookieGet)
    $.cookie('loginToken', '');
    $('.logout-wp').hide()
    isToken()
})

function logoutFn(token) {
    $.ajax({
        url: baseUrl + '/user/logout',
        type: 'post',
        data: {
            token
        },
        success: function (res) {
            console.log(res)
        }
    })
}

// 是否有token
function isToken() {
    if (!$.cookie('loginToken')) {
        $('.login').show()
        $('.logon').show()
        $('.userliBox').hide()
    } else {
        $('.login').hide()
        $('.logon').hide()
        $('.userliBox').show()
    }
}

// 用户的点击事件
$('.userliBox').on('click', function () {
    $('.userpullBox').toggle()
})
getList()
function getList() {
    $.ajax({
        url: 'http://localhost:2020/question/getDeail',
        type: 'get',
        data: {
            id
        },
        async: false,
        success(res) {
            // console.log(res)
            ress = res
        }
    })
}
renderList()
function renderList() {
    console.log(ress)
    let arr = []
    for (var i = 0; i < ress.data.data.length; i++) {
        arr.push('<div class="warp-img">\
        <img src="http://localhost:2020/uploads/'+ ress.data.data[i].avatar + '" alt="">\
    </div>\
    <div class="item-right">\
        <p class="listName">'+ ress.data.data[i].name + '</p>\
        <div class="mony">\
            <span >售价 ：\
                <b class="listPrice">'+ ress.data.data[i].price + '</b>\
            </span>\
            <p>定价：\
                <s>￥'+ ress.data.data[i].Originalprice + '</s>\
            </p>\
        </div>\
        <div class="conent-brief">\
            <ul>\
                <li>作 者：</li>\
                <li>出版社：</li>\
                <li>出版时间：</li>\
                <li>分类：</li>\
                <li>购买数量：</li>\
            </ul>\
            <ul>\
                <li>唯实教育考试研究院</li>\
                <li>唯实教育内部专享</li>\
                <li>'+ ress.data.data[i].publishTime + '</li>\
                <li>'+ ress.data.data[i].type + '</li>\
                <li class="num-li">\
                    <button class="btn-minus">-</button>\
                    <span class="num">1</span>\
                    <button class="btn-add">+</button>\
                    <span class="repertory-num">库存数量：\
                        <b class="studyNum">'+ ress.data.data[i].studyNum + '</b>\
                    </span>\
                </li>\
            </ul>\
        </div>\
        <div class="btn-warp">\
            <button class="first-btn">加入购物车</button>\
            <button class="two-btn">在线咨询</button>\
        </div>\
    </div>')
    }
    $(".conent-item").html(arr.join(" "))
}

$(".btn-minus").on("click", function () {
    let num = $(".num").html()
    if (num == 0) return
    num--
    $(".num").html(num)
})

$(".btn-add").on("click", function () {
    console.log(1)
    let num = $(".num").html()
    let studyNum = $(".studyNum").html()
    num++
    $(".num").html(num)
})

$(".first-btn").on("click", function () {
    let arr = []
    let obj = {
        bookId: id,
        num: $(".num").html()
    }
    arr.push(obj)
    localStorage.setItem("shoppingCar", JSON.stringify(arr))
})


