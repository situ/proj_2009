;
(function (window, document, $, undefined) {

    let cookieGet = $.cookie('loginToken');
    let baseUrl = 'http://localhost:2020';
    isToken()
    // 退出登录，显示遮罩层
    $('.logout').on('click', function () {
        $('.logout-wp').show()
    })

    $('.close').on('click', function () {
        $('.logout-wp').hide()
    })

    $('.define').on('click', function () {
        logoutFn(cookieGet)
        $.cookie('loginToken', '');
        $('.logout-wp').hide()
        isToken()
    })

    // 用户的点击事件
    $('.userliBox').on('click', function () {
        $('.userpullBox').toggle()
    })

    //退出登录
    function logoutFn(token) {
        $.ajax({
            url: baseUrl + '/user/logout',
            type: 'post',
            data: {
                token
            },
            success: function (res) {
                console.log(res)
            }
        })
    }

    // 是否有token
    function isToken() {
        if (!$.cookie('loginToken')) {
            $('.login').show()
            $('.logon').show()
            $('.userliBox').hide()
        } else {
            $('.login').hide()
            $('.logon').hide()
            $('.userliBox').show()
        }
    }

}(window, document, jQuery))