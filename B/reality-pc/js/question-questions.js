;
(function (window, document, $, undefined) {

    let tableData = [],
        pageSize = 10,
        currentPage = 1,
        total = 0;

    // 初始化
    getTableData()

    // get获取数据
    function getTableData(keyword) {
        // console.log(keyword)
        $.ajax({
            url: "http://localhost:2020/questions/testQuestionslist",
            type: 'get',
            data: {
                pageSize,
                currentPage:currentPage-1,
                keyword
            },
            success: function (res) {
                // console.log(res.data)
                tableData = res.data
                // console.log(res.total[0].total)
                total = res.total[0].total
                // renderPage(total)
                renderTableData(res.data)
            }
        })
    }

    // 渲染表格
    function renderTableData(data) {
        // console.log(data)
        var arr = [];
        for (var i = 0; i < data.length; i++) {
            arr.push(`
                <tr>
                    <td>${i + 1}</td>
                    <td>${data[i].title}</td>
                    <td>${data[i].cid}</td>
                    <td>${data[i].tips}</td>
                    <td>${data[i].created}</td>
                </tr>
            `)
        }
        $('tbody').html(arr.join(''))
        tabPage()
    }

    // 搜索
    $('#searchBtn').on('click', function () {
        // console.log(111)
        // console.log($('.form-input').val())
        getTableData($('.form-input').val())
    })

    // 分页
    function tabPage() {
        allPage = Math.ceil(total / pageSize);//总页数
        // console.log(allPage + "总页数");
        $('.pageNum').prop('max', allPage)//给input设置最大输入页数

        if (allPage <= 5) {
            var pageList = '';
            for (var i = 0; i < allPage * 1; i++) {
                var classname = i + 1 == currentPage ? "Pageactive" : "";
                pageList += '<li class="' + classname + '" dataNum="' + (i + 1) + '">' + (i + 1) + '</li>';
            }
        } else if (currentPage - 2 <= 1) {
            // console.log('前')
            var pageList = '';
            for (var i = 0; i < 5; i++) {
                var classname = i + 1 == currentPage ? "Pageactive" : "";
                pageList += '<li class="' + classname + '" dataNum="' + (i + 1) + '">' + (i + 1) + '</li>';
            }
        } else if (currentPage - 2 > 1 && allPage - currentPage >= 2) {
            // console.log('中间')
            var pageList = '';
            for (var i = currentPage * 1 - 2; i < currentPage * 1 + 2 + 1; i++) {
                var classname = i == currentPage ? "Pageactive" : "";
                pageList += '<li class="' + classname + '" dataNum="' + i + '">' + i + '</li>';
            }
        } else {
            // console.log('后')
            var pageList = '';
            for (var i = allPage * 1 - 4; i < allPage * 1 + 1; i++) {
                var classname = i == currentPage ? "Pageactive" : "";
                pageList += '<li class="' + classname + '" dataNum="' + i + '">' + i + '</li>';
            }
        }
        $('.pageNav').html(pageList);
    }

    // 标签页的点击事件
    $('.pageNav').on('click', "li", function () {
        $(this).addClass('Pageactive').siblings().removeClass('Pageactive');
        currentPage = $(this).attr('datanum');
        currentPageChange();
    });

    // 下一页
    $('.next').on("click", function () {
        if (currentPage == allPage) {
            return;
        }
        currentPage++;
        currentPageChange();
    })

    //上一页
    $('.prev').on("click", function () {
        if (currentPage == 1) {
            return;
        }
        currentPage--;
        currentPageChange();
    })

    //第一页
    $('.icon--1').on('click', function () {
        currentPage = 1;
        currentPageChange();
    })

    //最后一页
    $('.icon--').on('click', function () {
        currentPage = allPage;
        currentPageChange();
    })

    // currentPage当前页放生变化触发
    function currentPageChange() {
        if (currentPage == 1) {
            $('.prev').addClass('notallowed');
            $('.icon--1').addClass('notallowed');
        } else {
            $('.prev').removeClass('notallowed');
            $('.icon--1').removeClass('notallowed');
        }

        if (currentPage == allPage) {
            $('.next').addClass('notallowed');
            $('.icon--').addClass('notallowed');
        } else {
            $('.next').removeClass('notallowed');
            $('.icon--').removeClass('notallowed');
        }
        getTableData();
    }
    
    //input输入跳转页面
    $('.pageJump').on('click', function () {
        if ($('.pageNum').val() > allPage) {
            alert('总页数：' + allPage + '条');
            return;
        }
        currentPage = $('.pageNum').val();
        getTableData();
        $('.pageNum').val('')
    })

}(window, document, jQuery))