;
(function (window, document, $, undefined) {
    let questionData = [],
        optionsIndex = 0,
        ansArr = [],
        $logAlert = $('.login-alert'), //弹框
        $logAlertSpan = $('.login-alert span'), //弹框文本
        pageSize = 10,
        chongArr = [],
        chongIndex = [];

    getTableData()

    // get获取数据 
    function getTableData() {
        // console.log(location.search[location.search.length - 1] == 1)
        if (location.search[location.search.length - 1] == 1) {
            $('.prictitle').html('考点练习')
            $.ajax({
                url: "http://localhost:2020/questions/mode",
                type: 'get',
                data: {
                    pageSize
                },
                success: function (res) {
                    questionData = res.data.data
                    renderQuestion()
                }
            })
        } else {
            $('.prictitle').html('错题练习')
            questionData = JSON.parse(localStorage.getItem('mistakes') || '[]')
            // console.log(questionData)
            questionData.map(v => {
                // console.log(v)
                delete v.ans
            })
            if (!questionData.length) {
                $('.bank').html('')

                alertFn('目前没有错题')

                setTimeout(() => {
                    $logAlert.hide()
                    history.back()
                }, 1500)
            } else {
                renderQuestion()

            }
        }

    }

    // 渲染函数
    function renderQuestion() {
        var optionVal = JSON.parse(questionData[optionsIndex].options)

        var arr = [];

        arr.push(`
                <h3 class="font-20">
                    <span>#${optionsIndex + 1}.</span>
                    <span>${questionData[optionsIndex].title}</span>
                </h3>
                <ul>
            `)

        for (var i = 0; i < optionVal.length; i++) {
            arr.push(`
                    <li data-index="${optionVal[i].index}" class="answerli">
                        <input class="answeript" ${checkedFn(optionVal[i].index)} type="checkbox" value="${optionVal[i].index}">
                        <span>${optionVal[i].index}.</span>
                        <span>${optionVal[i].value}</span>
                    </li>
                `)
        }

        arr.push(`
                </ul>
            `)

        $('.quesmain').html(arr.join(''))
        renderQuesPageBtn()
    }

    function checkedFn(val) {
        // console.log(questionData[optionsIndex].ans)
        if (questionData[optionsIndex].ans) {
            if (questionData[optionsIndex].ans.indexOf(val) > -1) {
                // console.log(111)
                return 'checked'
            }
        } else {
            // console.log(222)
            return ''
        }
    }

    // 点选项
    $(".quesmain").on('click', '.answerli', function () {
        // console.log(optionsIndex)
        // console.log($(this).html())
        var index = $(this).attr('data-index');
        // console.log(index)
        // console.log(questionData[optionsIndex].answers)

        if (ansArr.indexOf(index) == -1) {
            ansArr.push(index)
        }

    })

    // 上一题
    $('#upques').on('click', function () {

        // console.log(2)
        optionsIndex--
        if (optionsIndex < 0) {
            optionsIndex = 0
        }
        $("#downques").html('下一题')
        if (questionData[optionsIndex].ans.length) {
            questionData[optionsIndex].ans.forEach((v, i) => {
                // console.log(v)
                $('.quesmain li input').attr('checked', true)
            })
        }
        ansArr = []
        renderQuestion()
    })

    // 下一题
    $('#downques').on('click', function () {
        // 点击交卷
        let rightAns = 0
        if (optionsIndex == questionData.length - 1) {

            let mistakesArr = JSON.parse(localStorage.getItem('mistakes') || '[]')
            ansArr = ansArr.sort()
            if (ansArr.length) {
                questionData[optionsIndex]['ans'] = ansArr
            }
            if (JSON.stringify(questionData[optionsIndex].ans) != questionData[optionsIndex].answers) {
                if (mistakesArr.indexOf(questionData[optionsIndex]) == -1) {
                    mistakesArr.push(questionData[optionsIndex])
                }
                localStorage.setItem('mistakes', JSON.stringify(mistakesArr))
            }
            questionData.map(v => {
                // console.log(v)
                if (JSON.stringify(v.ans) == v.answers) {
                    rightAns++
                }
            })
            // console.log(rightAns)
            if (location.search[location.search.length - 1] == 1) {
                alertFn('本次考点练习共得' + rightAns + '分')
            } else {
                // let quChong = questionData.filter(v=>{
                //     return mistakesArr.find(f=>f.answers==v.ans)
                // })
                questionData.map(v => {
                    if (chongArr.indexOf(v.id) != -1) {
                        if (chongIndex.indexOf(chongArr.indexOf(v.id)) == -1) {
                            chongIndex.push(chongArr.indexOf(v.id))
                        }
                        // console.log(chongIndex)
                    }

                })
                chongIndex.forEach(v => {
                    questionData.splice(v, 1)
                })

                localStorage.setItem('mistakes', JSON.stringify(questionData))
                // console.log(chongArr)
                alertFn('本次错题练习共得' + rightAns + '分')
            }

            setTimeout(() => {
                $logAlert.hide()
                history.back()
            }, 2000)
            return
        }

        let mistakesArr = JSON.parse(localStorage.getItem('mistakes') || '[]')
        ansArr = ansArr.sort()
        if (ansArr.length) {
            questionData[optionsIndex]['ans'] = ansArr
        }

        if (JSON.stringify(questionData[optionsIndex].ans) != questionData[optionsIndex].answers) {
            if (mistakesArr.indexOf(questionData[optionsIndex]) == -1) {
                mistakesArr.push(questionData[optionsIndex])
            }
            localStorage.setItem('mistakes', JSON.stringify(mistakesArr))
        }

        optionsIndex++
        if (optionsIndex == questionData.length - 1) {
            $("#downques").html('交卷')
        } else {
            $("#downques").html('下一题')
        }


        ansArr = []

        renderQuestion()
    })

    function alertFn(val) {
        $logAlertSpan.html(val)
        $logAlert.show()
    }

    // 渲染页码按钮
    function renderQuesPageBtn() {

        var arr = [];

        for (var i = 0; i < questionData.length; i++) {
            arr.push(`
                    <button data-index="${i}"  class="questionbtn btn col-ff ${backroundFn(i)}">${fillZero(i + 1)}</button>
                `)
        }

        // console.log(questionData)

        $('.quesbtn').html(arr.join(''))
    }

    // 页码按钮点击
    $('.quesbtn').on('click', '.questionbtn', function () {
        var index = $(this).attr('data-index') * 1;
        // console.log(index)

        let mistakesArr = JSON.parse(localStorage.getItem('mistakes') || '[]')
        ansArr = ansArr.sort()
        if (ansArr.length) {
            questionData[optionsIndex]['ans'] = ansArr
        }

        if (JSON.stringify(questionData[optionsIndex].ans) != questionData[optionsIndex].answers) {
            if (mistakesArr.indexOf(questionData[optionsIndex]) == -1) {
                mistakesArr.push(questionData[optionsIndex])
            }
            localStorage.setItem('mistakes', JSON.stringify(mistakesArr))
        }

        optionsIndex = index;

        if (optionsIndex == questionData.length - 1) {
            $("#downques").html('交卷')
        } else {
            $("#downques").html('下一题')
        }

        ansArr = []

        renderQuestion()
    })

    // 补零函数
    function fillZero(num) {
        return num < 10 ? "0" + num : num
    }

    // 按钮背景颜色
    function backroundFn(i) {
        if (i == optionsIndex) {
            return 'bgc-info'
        } else {
            // console.log(JSON.stringify(questionData[i].ans || '[]'))
            if (!JSON.stringify(questionData[i].ans)) {
                return 'bgc-warning'
            }
            if (JSON.stringify(questionData[i].ans) == questionData[i].answers) {
                if (chongArr.indexOf(questionData[i].id)) {
                    chongArr.push(questionData[i].id)
                }
                return 'bgc-success'
            } else {
                return 'bgc-danger'
            }
        }
    }


}(window, document, jQuery))