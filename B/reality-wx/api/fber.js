import request from "../utils/request"

//获取轮播图
export function getimg() {
  return request({
    url: '/img/img',
    method: "get",
    data: {}
  })
}

//随机获取两个列表
export function getTwoList() {
  return request({
    url: '/question/mode',
    method: "get",
    data: {
      pageSize: 2
    }
  })
}

//获取课程
export function getcurderList() {
  return request({
    url: '/question/getQuestions',
    method: "get",
    data: {}
  })
}

// 过滤major
export function getmajorList(major) {
  return request({
    url: '/question/filterMajor',
    method: "get",
    data: {
      major
    }
  })
}

//通过id获取值
export function getIdList(id) {
  return request({
    url: '/question/getDeail',
    method: "get",
    data: {
      id
    }
  })
}

//通过地址
export function getdizhi() {
  return request({
    url: '/xcxuser/getdizhi',
    method: "get",
    data: {}
  })
}

//获取购物车商品
export function getshop(id) {
  return request({
    url: '/question/getDeail',
    method: "get",
    data: {id}
  })
}