import shis from '../../utils/shis.js';
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';
var app = getApp()
// pages/newAddress/newAddress.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    form: {
      name: "",
      mobile: "",
      area: [],
      detail: "",
      openid: ""
    },
    show: false,
    areaList: shis,
    updataId: 0
  },
  //地区完成
  region(values) {
    // console.log(values);
    let arr = [];
    for (let i = 0; i < values.detail.values.length; i++) {
      arr.push(values.detail.values[i].name)
      this.setData({
        area: arr
      })
      this.data.form.area = arr
    }
    this.onClose()
  },
  //遮罩层显示
  showPopup() {
    this.setData({
      show: true
    });
  },
  //遮罩层隐藏
  onClose() {
    this.setData({
      show: false
    });
  },
  //保存
  baocun() {
    let url = "http://localhost:2020/xcxuser/saveAddress"
    let params = {
      name: this.data.form.name,
      mobile: this.data.form.mobile,
      detail: this.data.form.detail,
      area: JSON.stringify(this.data.form.area),
      openid: this.data.form.openid
    }
    if (this.data.updataId) {
      url = "http://localhost:2020/xcxuser/updataAddress";
      params = {
        id: this.data.updataId,
        name: this.data.form.name,
        mobile: this.data.form.mobile,
        detail: this.data.form.detail,
        area: JSON.stringify(this.data.form.area),
      }
      console.log(params)
    }
    wx.request({
      url: url,
      method: 'POST',
      data: params,
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: res => {
        console.log(res)
        if (this.data.updataId) {
          Toast.success('修改成功');
          wx.navigateTo({
            url: '../glarea/glarea',
          })
        } else {
          Toast.success('新增成功');
          wx.navigateTo({
            url: '../glarea/glarea',
          })
        }
      }
    })
  },
  //详细地址
  onChangeaddress(event) {
    // console.log(event.detail);
    this.data.form.detail = event.detail
  },
  //所在地区
  onChangeeregion(event) {
    this.setData({
      show: true
    })
  },
  //手机号
  onChangeMobile(event) {
    console.log(event.detail);
    this.data.form.mobile = event.detail
  },
  //收货人
  onChangeName(event) {
    // event.detail 为当前输入的值
    // console.log(event.detail);
    this.data.form.name = event.detail
  },

  getItem() {
    wx.request({
      url: 'http://localhost:2020/xcxuser/getAddressItem',
      data: {
        id: this.data.updataId
      },
      success: res => {
        // console.log(res.data.data[0])
        let item = {
          ...res.data.data[0]
        }
        item.area = JSON.parse(item.area)
        this.setData({
          form: item,
          area: item.area
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      updataId: options.sid
    })
    this.data.form.openid = app.globalData.openid
    console.log(app.globalData.openid)
    if (options.sid) {
      this.getItem()
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})