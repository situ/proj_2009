// pages/address/address.js
var app = getApp()
import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';

import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    radio: ''
  },

  //添加新地址
  gonew() {
    wx.navigateTo({
      url: '/pages/area/area',
    })
  },

  //修改默认值
  onChange(event) {
    // console.log(this.data.radio)
    // console.log(event.detail)
    wx.request({
      url: 'http://localhost:2020/xcxuser/updataMoren',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: {
        oldId: this.data.radio,
        newId: event.detail
      },
      success: res => {
        Toast.success('修改成功');
      }
    })
    this.setData({
      radio: event.detail,
    });
  },

  //获取地址
  getList() {
    wx.request({
      url: 'http://localhost:2020/xcxuser/getAddress',
      data: {
        openid: app.globalData.openid
      },
      success: res => {
        let data = [...res.data.data.data]
        let id = ''
        data.forEach(item => {
          item.area = JSON.parse(item.area).join(' ')
          item.moren == 1 && (id = item.id)
        });
        this.setData({
          list: data,
          radio: id
        })
        console.log(res)
      }
    })
  },

  //删除地址
  deleteFun(e) {
    // console.log(e.currentTarget.dataset.sid)
    Dialog.confirm({
        title: '提示',
        message: '您确定要删除该地址吗？',
      })
      .then(() => {
        wx.request({
          url: 'http://localhost:2020/xcxuser/deleteAddress',
          data: {
            id: e.currentTarget.dataset.sid
          },
          success: res => {
            this.getList()
            Toast.success('删除成功');
          }
        })
      })
      .catch(() => {
        Toast.success('已取消删除');
      });
    console.log(e.currentTarget.dataset.sid)
  },

  //修改地址
  updateFn(e) {
    console.log(e.currentTarget.dataset.sid)
    wx.navigateTo({
      url: '../area/area?sid=' + e.currentTarget.dataset.sid,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (!app.globalData.openid) {
      wx.navigateTo({
        url: '../login/login',
      })
    }
    console.log(app.globalData.openid)
    this.getList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})