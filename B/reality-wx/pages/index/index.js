//获取应用实例
const app = getApp()
let { getimg, getTwoList,getcurderList} =require('../../api/fber')

Page({
  data: {
    background:[],
    getTwoList:[],
    getcurder:[],
    getMajor:[],
    img:['../../image/homeimg/num1.png','../../image/homeimg/num2.png','../../image/homeimg/num3.png','../../image/homeimg/num4.png',]
  },
  jingpJump(e){
    let id=e.currentTarget.dataset.id;
    console.log(id)
    wx.navigateTo({
      url:"/pages/jingpjf/jingpjf?id="+id
    })
  },
  taocaiJump(options){
    let name=options.currentTarget.dataset.major
    wx.navigateTo({
      url: '/pages/taocai/taocai?navName='+name,
    })
  },
  qsbkJump(options){
    let id=options.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/qsbk/qsbk?id='+id,
    })
  },
  mycouserJump(){
    wx.navigateTo({
      url: "/pages/mycourser/mycourser",
    })
  },
  
  onLoad: async function (options) {
    let r=await getimg()
    let getTwo=await getTwoList()
    let getcurder=await getcurderList()
    let courserArr=getcurder.data
    let arr=[]
    let lastArr=[]
   
    courserArr.forEach(v=>{
          if(arr.indexOf(v.majorcid)<0){
            arr.push(v.majorcid)
          }
    })
    // console.log(arr)
    arr.forEach((v,i)=>{
        lastArr.push({'name':v,'img':this.data.img[i]})
    })
    // console.log(lastArr)
   
    this.setData({
        background:r.data.data,
        getTwoList:getTwo.data.data,
        getcurder:getcurder.data,
        getMajor:lastArr
    })
  }
})
