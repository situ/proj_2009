let { getIdList } =require('../../api/fber')

Page({
  data: {
      curObj:{},
      selectNum:1,
      isShow:false,
      show:false,
      id:''
  },
  showPopup() {
    
  },

  onClose() {
    this.setData({ show: false });
  },
  //计步器
  onChange(event) {
    // console.log(event.detail);
    this.setData({
      selectNum:event.detail,
    })
  },
  //详情下购买
  buyFun(){
    this.setData({ show: true });
    
  },
  //下单跳转页面
  toOrder(){
    let num=this.data.selectNum
    let id=this.data.id
    // console.log(num ,id)
    wx.navigateTo({
      url: '/pages/notarize/notarize?id='+id+'&&num='+num,
    })
  },

  masterwpClick(){
    this.setData({
      isShow:false
    })
  },

  dialogClick(e){
    e.stopPropagation();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    console.log(options.id)
    this.setData({
      id:options.id
    })
    let r=await getIdList(this.data.id)
    this.setData({
      curObj:r.data.data[0]
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})