// pages/mycourser/mycourser.js
Page({

  data: {
    isactive:"1",
    courserArr:[]
  },

  goindex(){
    console.log(1)
    wx.switchTab({
      url: '/pages/index/index',
    })
  },

  tabqieh(options){
    // console.log(options.currentTarget.dataset.num)
    this.setData({
      isactive:options.currentTarget.dataset.num
    })
  },
  subscriptionJump(options){
    let id=options.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/subscription/subscription?id='+id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let arr = wx.getStorageSync('dingyArr')
    this.setData({
      courserArr:arr
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})