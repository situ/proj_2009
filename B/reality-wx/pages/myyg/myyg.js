// pages/myyg/myyg.js
import {
  getshop
} from "../../api/fber"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shopList: [],
    result: [],
    isAll:false
  },

  goHome() {
    wx.switchTab({
      url: '../index/index',
    })
  },

  //单选
  onChanges(e) {
    this.setData({
      result: e.detail,
      isAll: e.detail.length == this.data.shopList.length
    })
    console.log(e.detail)
  },

  //全选
  onAllChange(e) {
    let arr = [];
    if (e.detail) {
      this.data.shopList.forEach(item => {
        arr.push(item.id + "")
      })
    }
    this.setData({
      isAll: e.detail,
      result: arr
    })
  },

  delBtn() {
    let arr = wx.getStorageSync('shop') || []
    arr = arr.filter(v => {
      return v.data != this.data.result.find(o => (o * 1) == v.data)
    })
    wx.setStorageSync('shop', arr)
    this.setData({
      shopList: []
    })
    this.init()
  },

  async init() {
    let arr = wx.getStorageSync('shop') || []
    let idArr = []
    if (arr.length) {
      for (var i = 0; i < arr.length; i++) {
        idArr.push(arr[i].data)
      }
      let res = await getshop(idArr.join(","))
      let wxArr = []
      wxArr = res.data.data.map((v, i) => {
        v["num"] = arr.find(o => o.data == v.id).num
        return v
      })
      this.setData({
        shopList: wxArr
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.init()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})