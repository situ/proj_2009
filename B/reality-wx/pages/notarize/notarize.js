var {
  getIdList,
  getdizhi
} = require('../../api/fber')
import Toast from '../../miniprogram_npm/@vant/weapp/toast/toast';
// pages/notarize/notarize.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imageURL: '../../image/homeimg/jingpin1.png',
    value: "",
    shopdata: [],
    shopnum: 0,
    zongprice: '',
    numshop: 0,
    dizhi: []
  },

  //提交订单
  save() {
    // console.log(this.data.shopdata)
    // console.log(this.data.numshop)
    let arr = wx.getStorageSync('shop') || []
    arr.push({
      data: this.data.shopdata.id,
      num: this.data.numshop
    })
    wx.setStorageSync('shop', arr)
    Toast.success('购买成功');
    setTimeout(function () {
      wx.switchTab({
        url: '../index/index',
      })
    }, 1500)
  },

  //跳转地址
  gotodz() {
    wx.navigateTo({
      url: '../glarea/glarea',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let dz = await getdizhi();
    dz.data.data = dz.data.data.filter(v => {
      return v.area = JSON.parse(v.area).join(',') || []
    })
    this.setData({
      dizhi: dz.data.data[0]
    })
    console.log(this.data.dizhi)
    let r = await getIdList(
      options.id
    )
    this.setData({
      shopdata: r.data.data[0],
      shopnum: r.data.data.length,
      zongprice: r.data.data[0].price * options.num,
      numshop: options.num
    })
    console.log(this.data.numshop)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})