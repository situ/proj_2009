import Dialog from '../../miniprogram_npm/@vant/weapp/dialog/dialog';
let { getIdList } =require('../../api/fber')

Page({
  data: {
    curObj:{},
    tabActive:'1'
  },

  //tab切换
  tabClick(options){
    this.setData({
      tabActive:options.currentTarget.dataset.num
    })
  },
  dingyueJump(){
    Dialog.confirm({
      title: '是否订阅',
    }).then(() => {
      let arr = wx.getStorageSync('dingyArr') || []
      let dingyObj=arr.find(v=>v.id==this.data.curObj.id)
      if(!dingyObj){
        arr.push(this.data.curObj)
      }else {
        wx.showToast({
          title: '已订阅该课程',
          icon: 'success',
          duration: 2000
        })
      }
      console.log(arr)
      wx.setStorageSync('dingyArr', arr)
       setTimeout(()=>{
        wx.navigateTo({
          url: '/pages/mycourser/mycourser',
        })
       },1000)
    })
    .catch(() => {
      // on cancel
    });
   
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
      let id=options.id
      console.log(id)
      let r=await getIdList(id)
      console.log(r)
      this.setData({
        curObj:r.data.data[0]
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})