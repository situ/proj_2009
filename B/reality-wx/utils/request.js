let baseUrl = "http://localhost:2020"

let request = ({ url, data, method }) => {
    return new Promise((resolve, reject) => {
        wx.request({
            url: baseUrl + url, //仅为示例，并非真实的接口地址
            data,
            method,
            header: {
                'content-type': 'application/x-www-form-urlencoded' // 默认值
            },
            success(res) {
                resolve(res.data)
            }
        })
    })
}

module.exports = request