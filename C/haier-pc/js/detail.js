(function (window, $) {
    // 滚动条滚到固定位置时tab切换转固定定位的参数
    let top = $(".tab_box").offset().top
    let userInfo = JSON.parse($.cookie('user-info') || '{}')
    // 获取数据

    let msg = $.cookie('haier-sp')
    console.log(msg)
    apply(JSON.parse(msg))
    // let baseUrl = 'http://localhost:2468/'
    // $.ajax({
    //     type: 'get',
    //     data: {
    //         cid: 2
    //     },
    //     url: baseUrl + 'product/classList',
    //     success(res) {
    //         let arr = res.data
    //         apply(arr[7])
    //     }
    // })

    function apply(data) {
        console.log(data);
        let baseUrl = 'http://localhost:2468/uploads/'

        $(".main_top_nav").append(`
            <a href="./home.html">首页</a>&nbsp;&gt;
            <a href="./productlist.html class="js_type">${data.type}</a>&nbsp;&gt;
            <span class="js_type">${data.vertion}</span>
        `)

        $(".main_top_product_big").append(`
            <img src="${baseUrl + data.imgs}" alt="">
            <div></div>
        `)

        $(".zoomdiv").append(`
            <img src="${baseUrl + data.imgs}" class="bigimg" alt="">
        `)

        $(".showbox ul li").eq(0).find("img").attr("src", baseUrl + data.imgs)

        $(".main_top_content_right_title").append(`
            <p>${data.title}</p>
            <span>${data.vertion}</span>
        `)

        $(".price").append(`
            <span>参考价</span>
            <i>&nbsp;&nbsp;￥&nbsp;</i>
            <span>${data.price}</span>
        `)

        $('.buy_btn p').eq(0).on('click', function () {
            if (userInfo.id) {
                let arr = JSON.parse($.cookie('haier-cart') || '[]')
                arr.push(data)
                $.cookie('haier-cart', JSON.stringify(arr))
                window.location.href = './shop.html'
            } else {
                location.href = "../view/denglu.html"
            }
        })

        $('.buy_btn p').eq(1).on('click', function () {
            if (userInfo.id) {
                let arr = JSON.parse($.cookie('haier-cart') || '[]')
                arr.push(data)
                $.cookie('haier-cart', JSON.stringify(arr))
                window.history.back()
            } else {
                location.href = "../view/denglu.html"
            }
        })

        // $(".js_type").html(data.type)
        // $(".js_vertion").html(data.vertion)

        // $(".main_top_product_big img").attr("src", baseUrl + data.imgs)
        // $(".zoomdiv img").attr("src", baseUrl + data.imgs)
        // $(".showbox ul li").eq(0).find("img").attr("src", baseUrl + data.imgs)

        // $(".main_top_content_right_title p").html(data.title)
        // $(".main_top_content_right_title span").html(data.vertion)
        // $(".price span").eq(1).html(data.price)
    }

    // 大图切换
    $(".showbox ul li").mouseenter(function () {
        let index = $(this).index()
        let currentSrc = $(this).find('img').attr("src")

        $(this).addClass("showbox_current").siblings().removeClass("showbox_current")
        $(".main_top_product_big img").attr("src", currentSrc)
        $(".zoomdiv img").attr("src", currentSrc)
    })

    // 放大镜盒子展示
    $(".main_top_product_big").mouseenter(function () {
        $(this).find('div').css("display", "block")
        $(".zoomdiv").css("display", "block")
    })

    // 放大镜盒子隐藏
    $(".main_top_product_big").mouseleave(function () {
        $(this).find('div').css("display", "none")
        $(".zoomdiv").css("display", "none")
    })

    // 移动放大镜
    $(".main_top_product_big").mousemove(function (event) {
        let littleBox = $(".main_top_product_big div")

        let littleBoxLeft = event.pageX - $(this).offset().left - littleBox.innerWidth() / 2;  // 小盒子left数值
        let littleBoxTop = event.pageY - $(this).offset().top - littleBox.innerHeight() / 2;   // 小盒子top数值

        if (littleBoxLeft >= $(this).innerWidth() - littleBox.innerWidth()) {              //最大宽度时
            littleBoxLeft = $(this).innerWidth() - littleBox.innerWidth();
        } else if (littleBoxLeft <= 0) {
            littleBoxLeft = 0;
        };

        if (littleBoxTop >= $(this).innerHeight() - littleBox.innerHeight()) {             //最大高度时
            littleBoxTop = $(this).innerHeight() - littleBox.innerHeight();
        } else if (littleBoxTop <= 0) {
            littleBoxTop = 0;
        };

        littleBox.css({ "left": littleBoxLeft + "px", "top": littleBoxTop + "px" })
        $(".zoomdiv img").css({ "left": -littleBoxLeft * 3 + "px", "top": -littleBoxTop * 3 + "px" })
    })

    // 缩略图切换 第一页
    $(".pageleft").click(function () {
        let beforeLeft = $(".showbox ul").css("left")
        let allWidth = $(".showbox ul").css("width")
        let showWidth = $(".showbox").css("width")

        // 足够展示一组数据时
        let currentLeft1 = parseFloat(beforeLeft) + parseFloat(showWidth)
        // let currentLeft1 = Math.abs(parseFloat(beforeLeft)) + parseFloat(showWidth)
        if (currentLeft1 < 0) {
            $(".showbox ul").animate({ left: currentLeft1 + "px" });
        }

        // 不够展示一组时
        let currentLeft2 = Math.abs(parseFloat(beforeLeft))
        if (currentLeft2 < parseFloat(showWidth) && currentLeft2) {
            $(".showbox ul").animate({ left: 0 + "px" });
            $(".pageleft span").addClass("disabled")
        }
        $(".pageright span").removeClass("disabled")
    })

    // 缩略图切换 第二页
    $(".pageright").click(function () {
        let beforeLeft = $(".showbox ul").css("left")
        let allWidth = $(".showbox ul").css("width")
        let showWidth = $(".showbox").css("width")

        // 足够再展示一组数据时
        let currentLeft1 = Math.abs(parseFloat(beforeLeft)) + parseFloat(showWidth)
        if (parseFloat(allWidth) - currentLeft1 > parseFloat(showWidth)) {
            $(".showbox ul").animate({ left: -currentLeft1 + "px" });
        }

        // 不够展示一组时
        let currentLeft2 = parseFloat(allWidth) - parseFloat(showWidth)
        if (currentLeft1 > parseFloat(allWidth) - parseFloat(showWidth)) {
            $(".showbox ul").animate({ left: -currentLeft2 + "px" });
            $(".pageright span").addClass("disabled")
        }
        $(".pageleft span").removeClass("disabled")
    })

    // tab切换
    $(".js_tab li").click(function () {
        let index = $(this).index()
        $(this).addClass("tab_current").siblings().removeClass("tab_current")
        $(".js_tab .tab_text").eq(index).addClass("tab_text").siblings().removeClass("tab_text")
        $(".js_tabShow .tab_content").eq(index).addClass("currentTab").siblings().removeClass("currentTab")
        $(document).scrollTop(top)
    })

    $(document).scroll(function () {
        let scroH = $(document).scrollTop();
        if (scroH >= top) {
            $(".tab_box").addClass("fixed")
            $(".buy_btnbox").css("display", "inline-block")
        } else {
            $(".tab_box").removeClass("fixed")
            $(".buy_btnbox").css("display", "none")
        }

    });
})(window, jQuery)