; (function () {
    var titleBox = $('.title-box'),
        xinxuanContent = $('.xinxuan_bottom .content'),
        paihangContent = $('.paihang_bottom .content')
        typeItems = $('.type-items'),
        typeBox = $('.type-box')
    let baseUrl = 'http://localhost:2468/'
    let leftMsg = {}
    //获取分类
    $.ajax({
        type: 'get',
        url: baseUrl + 'summary/list',
        success(res) {
            let arr = res.data
            arr.splice(6)
            titleFun(arr)
        }
    })
    //渲染分类列表
    function titleFun(arr) {
        let render = []
        arr.forEach(val => {
            render.push(`<li class="xinxuanLi" data-id="${val.id}"><span>${val.title}</span></li>`)
        });

        titleBox.html(render.join(''))
        $('.title-box .xinxuanLi').eq(0).addClass('cur')
        getTitle(arr[0].id)
        $('.xinxuanLi').on('click', function (e) {
            let id = e.currentTarget.dataset.id
            $(this).addClass('cur').siblings('li').removeClass('cur')
            $('.xinxuan_bottom .content').empty()
            getTitle(id)
        })
    }

    //获取分类商品
    function getTitle(id) {
        $.ajax({
            type: 'get',
            url: baseUrl + 'product/classList',
            data: {
                sid: id
            },
            success(res) {
                let arr = res.data
                renderXinxuan(arr)
                $('.xinxuan_box_pro_box').on('click', function (e) {
                    let id = e.currentTarget.dataset.id
                    let text = e.currentTarget.dataset.active
                    let msg = arr.find(val => val.id == id)
                    if (text) {
                        msg = leftMsg
                    }
                    console.log(msg)
                    window.location.href = './detail.html'
                    $.cookie('haier-sp', JSON.stringify(msg))
                })
            }
        })
    }
    //渲染分类商品
    function renderXinxuan(arr) {
        let arrs = [...arr]
        let renderLeft = []
        let renderRight = []
        renderLeft.push(` 
        <div class="xinxuan_left xinxuan_box_pro_box" data-id="${arrs[0].id}">
            <div class="xinxuan_box_pro_wrap">
            <div class="xinxuan_img">
                <img src="http://localhost:2468/uploads/${arrs[0].imgs}" alt="">
            </div>
            <div class="xinxuan_name">${arrs[0].title}</div>
            <div class="xinxuan_note">${arrs[0].vertion}</div>
            <div class="xinxuan_line"></div>
            </div>
        </div>
        <div class="title-right"></div>
        `)
        xinxuanContent.html(renderLeft.join(''))
        leftMsg = arrs[0]
        arrs.splice(0, 1)
        arrs.forEach(val => {
            renderRight.push(`<div class="xinxuan_right xinxuan_box_pro_box" data-id="${val.id}">
              <div class="xinxuan_box_pro_top" >
                <div class="xinxuan_img">
                   <img style="height: 180px;"src="http://localhost:2468/uploads/${val.imgs}" alt="">
                </div>
                <div class="xinxuan_name">${val.title}</div>
                <div class="xinxuan_note">${val.vertion}</div>
                <div class="xinxuan_line"></div>
              </div>
            </div>`)
        });
        $('.title-right').html(renderRight.join(''))
    }

    //获取类别
    $.ajax({
        type: 'get',
        url: baseUrl + 'classify/list',
        success(res) {
            let arr = res.data
            arr.splice(7)
            typeFun(arr)
            typeFn(arr)
        }
    })

    //导航渲染
    function typeFn(arr) {
        let render = []
        arr.forEach(val => {
            render.push(`<li data-id="${val.id}">${val.type}</li>`)
        })
        typeItems.html(render.join(''))
        $('.type-items li').on('click', function (e) {
            let id = e.currentTarget.dataset.id
            window.location.href = './productlist.html?id=' + id
        })
    }

    //渲染类别列表
    function typeFun(arr) {
        let render = []
        arr.forEach(val => {
            render.push(` <li class="paihangLi" data-id="${val.id}"><span>${val.type}</span></li>`)
        });
        typeBox.html(render.join(''))
        $('.type-box .paihangLi').eq(0).addClass('cur')
        getType(arr[0].id)
        $('.paihangLi').on('click', function (e) {
            let id = e.currentTarget.dataset.id
            $(this).addClass('cur').siblings('li').removeClass('cur')
            $('.paihang_bottom .content').empty()
            getType(id)
        })
    }

    //获取类别商品
    function getType(id) {
        $.ajax({
            type: 'get',
            url: baseUrl + 'product/classList',
            data: {
                cid: id
            },
            success(res) {
                let arr = res.data
                renderPaohang(arr)
                $('.paihang_item').on('click', function (e) {
                    let id = e.currentTarget.dataset.id
                    let msg = arr.find(val => val.id == id)
                    console.log(msg)
                    window.location.href = './detail.html'
                    $.cookie('haier-sp', JSON.stringify(msg))
                })
            }
        })
    }

    //渲染类别商品
    function renderPaohang(arr) {
        let arrs = [...arr]
        let render = []
        arrs.splice(3)
        // console.log(arrs)
        arrs.forEach(val => {
            render.push(` 
                <div class="paihang_item" data-id="${val.id}">
                    <div class="paihang_pro">
                        <div class="paihang_img">
                            <img src="http://localhost:2468/uploads/${val.imgs}" alt="">
                        </div>
                        <div class="paihang_right">
                            <div class="paihang_name">${val.title}</div>
                            <div class="paihang_xinghao">${val.vertion}</div>
                            <div class="paihang_ling">了解更多</div>
                            <div class="paihang_icon icon_1"></div>
                        </div>
                    </div>
                </div>`)
        })
        // console.log(render.join(''))
        paihangContent.html(render.join(''))
    }

    //个人中心
    $('.ren').on('click', function () {
        console.log(1)
    })

    //购物车
    $('.cart').on('click', function () {
        window.location.href = './gerenzhongxin.html'
    })



    $(".xinxuan_tab .xinxuanLi").click(function () {
        var $this = $(this),
            index = $this.index();
        $(this).addClass("cur").siblings().removeClass("cur");
        $(".xinxuan_bottom .content").eq(index).addClass("on").siblings().removeClass("on");
    });

    $(".paihangLi").click(function () {
        var $this = $(this),
            index = $this.index();
        $(this).addClass("cur").siblings().removeClass("cur");
        $(".paihang_bottom .content").eq(index).addClass("on").siblings().removeClass("on");
    });

    $(".nav_box div").mouseenter(function () {
        var $this = $(this),
            index = $this.index();
        $(this).addClass("active");
    });

    $(".nav_box div").mouseleave(function () {
        var $this = $(this),
            index = $this.index();
        $(this).removeClass("active")
    });

    $(".nav_box div").mouseenter(function () {
        var $this = $(this);
        $this.children("ul").css({ "display": "block" });
    })

    $(".nav_box div").mouseleave(function () {
        var $this = $(this);
        $this.children("ul").css({ "display": "none" })
    })


})()

