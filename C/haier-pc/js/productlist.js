(function (window, $) {
    let baseUrl = 'http://localhost:2468/',
        total = 0,//总条数
        pageSize = 8, //每页多少条
        currentPage = 1,//当前页
        totalPages = 0;//总页数;
    let ids= window.location.search.substring(4)

    // getArr()
    getAllClassify()

    function getAllClassify() { //获取所有产品类别
        $.ajax({
            type: "get",
            url: baseUrl + 'product/classList',
            data:{
                cid:ids
            },
            success(res) {
                let arr = res.data;
                // renderAllClassifyFn(arr)
                renderFn(arr)
                $(".titbox").children().eq(2).html(`共找到${arr.length}个产品`)
                $(".name").children().eq(0).html(arr[0].type)
                $(".titbox > span").html(arr[0].type)
            }
        })
    }


    // function getArr() { //获取所有产品列表
    //     $.ajax({
    //         type: 'get',
    //         url: baseUrl + 'product/list',
    //         success(res) {
    //             let arr = res.data;
    //             renderFn(arr)
    //             $(".titbox").children().eq(2).html(`共找到${arr.length}个产品`)
    //         }
    //     })
    // }

    // function renderAllClassifyFn(arr) { //渲染所有产品类别
    //     arr.forEach((v, i) => {
    //         $(".splb > li").eq(i).append(`<a>${v.type}</a>`)
    //     })
    // }

    // $(".splb > li").on("click", function () {
    //     let filterName = $(this).children().eq(1).html()
    //     $(".name").children().eq(0).html(filterName)
    //     $(".titbox > span").html(filterName)

    //     $.ajax({
    //         type: 'get',
    //         url: baseUrl + 'product/list',
    //         success(res) {
    //             let arr = res.data;
    //             let type = filterName;

    //             switch (type) {
    //                 case "空调": type = "1";
    //                     break;
    //                 case "冰箱": type ="2";
    //                     break;
    //                 case "洗衣机": type ="3";
    //                     break;
    //                 case "热水器":type = "4";
    //                     break;
    //                 case "电视": type ="5";
    //                     break;
    //                 case "油烟机":type = "6";
    //                     break;
    //                 case "燃气灶": type ="7";
    //                     break;
    //                 case "酒柜": type = "8";
    //                     break;
    //                 default:
    //                     break;
    //             }
    //             let filterArr = arr.filter((v) => {
    //                 return v.cid == type
    //             })
    //             renderFn(filterArr)
    //             $(".titbox").children().eq(2).html(`共找到${filterArr.length}个产品`)
    //         }
    //     })
    // })

    // function filterFn () { //筛选，渲染商品列表

    // }

    function renderFn(arr) { //渲染当前页商品列表及分页按钮

        let pageArr = [],//操作后拿到当前页的arr
            renderArr = [],//渲染当前页的arr(类型为字符串数组)
            pageBtnArr = [];//渲染分页按钮按钮的arr

        total = arr.length;
        totalPages = Math.ceil(total / pageSize)

        for (let i = currentPage * pageSize - pageSize; i < currentPage * pageSize; i++) {
            if (i == total) {
                break
            } else {
                pageArr.push(arr[i])
            }
        }

        for (let p = 0; p < totalPages; p++) {
            if (p + 1 == currentPage) {
                pageBtnArr.push(`
                        <li class="current">${p + 1}</li>
                    `)
            } else {
                pageBtnArr.push(`
                        <li>${p + 1}</li>
                    `)
            }
        }

        if (currentPage != 1) { //下一页按钮
            $(".previouspage").css({
                "color": "black"
            })
        } else {
            $(".previouspage").css({
                "color": "#d4d4d4"
            })
        }

        if (currentPage != totalPages) { //下一页按钮
            $(".nextpage").css({
                "color": "black"
            })
        } else {
            $(".nextpage").css({
                "color": "#d4d4d4"
            })
        }

        if (pageArr.length) {
            $(".nodatabox").css({
                "display": "none"
            })
            $(".leftmain").css({
                "display": "block"
            })
            pageArr.forEach(v => {
                renderArr.push(`
                    <li>
                        <div class="listbox" data-id="${v.id}">
                            <a>
                                <img src="http://localhost:2468/uploads/${v.imgs}" alt="">
                            </a>
                            <div class="textbox">
                                <a href="">${v.title}</a>
                                <p>${v.vertion}</p>
                            </div>
                            <p class="jiage">
                                <span>参考价:</span>
                                <span>${v.price}</span>
                            </p>
                            <p class="interaction">
                                <a href="">1</a>
                                <span>家电商在售</span>
                                <span class="iconfont icon-pinglun"></span>
                                <a href="">288</a>
                            </p>
                        </div>
                    </li>
                `)
            });
        } else {
            $(".leftmain").css({
                "display": "none"
            })
            $(".nodatabox").css({
                "display": "block"
            })
        }

        $(".leftmain > ul").html(renderArr.join(""))
        $(".page > ul").html(pageBtnArr.join(""))

        $('.listbox').on('click',function(e){
            let id = e.currentTarget.dataset.id
            let msg = arr.find(val=>val.id == id)
            window.location.href = './detail.html'
            // console.log(msg)
            $.cookie('haier-sp', JSON.stringify(msg))
        })


        $(".page > ul > li").on("click", function (e) { //第几页按钮
            currentPage = e.target.innerHTML * 1
            getAllClassify()
            // getArr()
        })


    }

    $(".nextpage").on("click", function () { //下一页
        if (currentPage != totalPages) {
            currentPage++
            getAllClassify()
            // getArr()
        }
    })

    $(".previouspage").on("click", function () { //上一页
        if (currentPage != 1) {
            currentPage--
            getAllClassify()
            // getArr()
        }
    })


    $(".zh > li").click(function () { //升序降序按钮
        $(".action").removeClass("action")
        $(this).addClass("action")
        if ($(this).index()) {

            if ($(this).children().css("display") == "inline") {
                if ($(this).children().html() == "&nbsp;↓") {
                    $(this).children().html("&nbsp;↑")
                } else {
                    $(this).children().html("&nbsp;↓")
                }
            } else {
                $(this).siblings().children().css({
                    "display": "none"
                })
                $(this).siblings().children().html("&nbsp;↓")
                $(this).children().css({
                    "display": "inline"
                })
            }
        } else {
            $(this).siblings().children().css({
                "display": "none"
            })
            $(this).siblings().children().html("&nbsp;↓")
        }
    })


    $(".menubar").mouseover(function () { //面包屑鼠标滑入
        $(".menubar").css({
            "background-color": "#fffdfc",
            "border": "1px solid #fffdfc"
        });

        $(".kong").css({
            "display": "block"
        })

        $(".splb ").css({
            "display": "block"
        })

        $(".extend").html("∧")

        $(".name").css({
            "color": "#005aaa"
        })
    });

    $(".menubar").mouseout(function () {//面包屑鼠标滑出
        $(".menubar").css({
            "background-color": "#fff",
            "border": "1px solid #e4e4e4",
        });

        $(".kong").css({
            "display": "none"
        })

        $(".splb ").css({
            "display": "none"
        })

        $(".extend").html("∨")

        $(".name").css({
            "color": "black"
        })


    })

    $(".filterone").mouseover(function () { //筛选项只有一项时的鼠标滑入
        $(this).css({
            "background-color": "#fffdfc",
            "border": "1px solid #fffdfc",
            "color": "#005aaa"
        });

        $(".filteronekong").css({
            "display": "block"
        })

        $(".oneoptions").css({
            "display": "block",
            "border": "1px solid #fffdfc"
        })

        $(".filteroneextend").html("∧")
    })

    $(".filterone").mouseout(function () { //筛选项只有一项时的鼠标滑出
        $(this).css({
            "background-color": "#fff",
            "border": "1px solid #e4e4e4",
            "color": "black"
        });

        $(".filteronekong").css({
            "display": "none"
        })

        $(".oneoptions").css({
            "display": "none",
            "border": "1px solid #ececec"
        })

        $(".filteroneextend").html("∨")
    })

    $(".oneoptions > li").mouseover(function () {//筛选项只有一项时的鼠标滑入弹出盒子中的li滑入
        $(this).css({
            "color": "#005aaa"
        })
    })

    $(".oneoptions > li").mouseout(function () {//筛选项只有一项时的鼠标滑入弹出盒子中的li划出
        $(this).css({
            "color": "black"
        })
    })

    $(".filterdetails").mouseover(function () {//筛选项为多项时的鼠标滑入
        $(this).css({
            "border": "1px solid #005aaa"
        })
    })

    $(".filterdetails").mouseout(function () {//筛选项为多项时的鼠标滑出
        $(this).css({
            "border": "1px solid #e4e4e4"
        })
    })

    $(".emptyfilter").mouseover(function () {//清空筛选的鼠标滑入
        $(this).css({
            "border": "1px solid #005aaa",
            "color": "#005aaa"
        })
    })

    $(".emptyfilter").mouseout(function () {//清空筛选的鼠标滑出
        $(this).css({
            "border": "1px solid #e4e4e4",
            "color": "black"
        })
    })

    $(".nohiddenchilden").mouseover(function () { //筛选项只有一项时的鼠标滑入
        $(this).css({
            "color": "#005aaa"
        })
    })

    $(".nohiddenchilden").mouseout(function () { //筛选项只有一项时的鼠标滑出
        $(this).css({
            "color": "black"
        })
    })

    $(".filteroptionschilden").mouseover(function () { //筛选项为多项时的鼠标滑入
        $(this).css({
            "color": "#005aaa"
        })
    })

    $(".filteroptionschilden").mouseout(function () { //筛选项为多项时的鼠标滑出
        $(this).css({
            "color": "black"
        })
    })

    $(".optionscheckbtn").click(function () { //多项时多选按钮显示，此为多选按钮点击
        $(".btnshow").css({ "display": "none" })
        $(".optionscheckbox").css({ "display": "block" })
    })

    $(".optionsb_cancel").click(function () { //多选按钮弹出层中的取消按钮
        $(".btnshow").css({ "display": "block" })
        $(".optionscheckbox").css({ "display": "none" })

        $(".optionsipt").prop("checked", false);
        $(".optionsb_confirm").css({
            "background-color": "#ededed"
        })
    })

    $(".optionsb_confirm").click(function () { //多选按钮弹出层中的确定按钮
        console.log("确定")
    })

    $(".optionsipt").click(function () { //多选按钮弹出层中的checkbox
        let checkedlength = document.querySelectorAll(".optionsipt:checked").length
        if (checkedlength) {
            $(".optionsb_confirm").css({
                "background-color": "#005aaa"
            })
        } else {
            $(".optionsb_confirm").css({
                "background-color": "#ededed"
            })
        }
    })

    $(".other").mouseenter(function () {//其他选项滑入
        $(this).css({
            "background-color": "#fffdfc",
            "box-shadow": "0 2px 5px rgba(0,0,0,.1)"
        })

        $(this).children().eq(0).css({
            "color": "#005aaa"
        })

        $(this).children().eq(1).html("∧").css({
            "color": "#005aaa"
        })

        $(this).children().eq(2).css({
            "display": "block"
        })

        $(".othercheckbox").not($(this).children()).css({
            "display": "none"
        })
    })

    $(".other").mouseleave(function () { //其他选项滑出
        $(this).css({
            "background-color": "#fff",
            "box-shadow": "none"
        })

        $(this).children().eq(0).css({
            "color": "black"
        })

        $(this).children().eq(1).html("∨").css({
            "color": "black"
        })

        $(this).children().eq(2).css({
            "display": "none"
        })

        $(".otheript").prop("checked", false);
        $(".otherb_confirm").css({
            "background-color": "#ededed"
        })
    })

    $(".otherhoverul li").mouseover(function () {//其它选项滑过后盒子中的li滑入
        $(this).css({
            "color": "#005aaa"
        })
    })

    $(".otherhoverul li").mouseout(function () { //其它选项滑过后盒子中的li划出
        $(this).css({
            "color": "black"
        })
    })

    $(".othercheckbtn").click(function () {//其它选项滑过后盒子中的多选按钮
        $(this).parent().parent().css({ "display": "none" })
        $(this).parent().parent().next().css({
            "display": "block"
        })
    })

    $(".otherb_cancel").click(function () { //其他选项点击多选后盒子中的取消
        $(".othercheckbox").css({ "display": "none" })

        $(".otheript").prop("checked", false);
        $(".otherb_confirm").css({
            "background-color": "#ededed"
        })
    })

    $(".otherb_confirm").click(function () { //其他选项点击多选后盒子中的确定
        console.log("确定")
    })

    $(".otheript").click(function () { //其他选项点击多选后盒子中的checkbox
        let checkedlength = document.querySelectorAll(".otheript:checked").length
        if (checkedlength) {
            $(".otherb_confirm").css({
                "background-color": "#005aaa"
            })
        } else {
            $(".otherb_confirm").css({
                "background-color": "#ededed"
            })
        }
    })

    $(".pricefilter > div").mouseenter(function () {
        $(this).css({
            "background-color": "#fffdfc"
        })

        $(".pricebtn").css({
            "display": "block"
        })
    })

    $(".pricefilter > div").mouseleave(function () {
        $(this).css({
            "background-color": "#fff"
        })

        $(".pricebtn").css({
            "display": "none"
        })
    })
})(window, jQuery)