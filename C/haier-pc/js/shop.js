; (function () {
    var li = document.getElementsByClassName('dt')[0];
    var li1 = document.getElementsByClassName('dt2')[0];
    var li2 = document.getElementsByClassName('dt3')[0];
    var li3 = document.getElementsByClassName('dt4')[0];
    var jdcheckbox = document.getElementsByClassName('jdcheckbox');
    var s = '';

    $('.cart-item-list').on('change', '.jdcheckbox', function () {
        var $th = $(this);
        var y = $(this).attr("data-bind")
        s = y
        for (let i = 0; i < $th.length; i++) {
            if ($th[i].checked == true) {
                $('.price em').html($(this).parent().parent().parent().parent().find('strong[class*=str1]').text())
            } else {
                $('.price em').html(0)
            }
        }
    })

    var ipt
    $('.cart-item-list').on('change', $('.jdcheckbox'), function () {
        ipt = $(this).find('input[class*=jdcheckbox]');
        let bool = []
        for (let i = 0; i < ipt.length; i++) {
            if (ipt[i].checked === true) {
                bool.push(ipt[i].checked)
            }
        }
        if (bool.length == ipt.length) {
            jdcheckbox[0].checked = true
            jdcheckbox[jdcheckbox.length - 1].checked = true
            setTotal()

        } else {
            jdcheckbox[0].checked = false
            jdcheckbox[jdcheckbox.length - 1].checked = false
        }
    })

    $('.remove-batch').click(function () {
        if (ipt === undefined) {
            alert('请选择要删除的商品!')
        } else {
            for (let i = 0; i < ipt.length; i++) {
                if (ipt[i].checked == true) {
                    // var $this = ipt[i].attr("data-bind");
                    arr = arr.filter(function (a) {
                        return a.id != s
                    });
                    $('.price em').html(0)
                    findAllDepts()
                    jdcheckbox[0].checked = false
                    jdcheckbox[jdcheckbox.length - 1].checked = false
                }
            }
        }
    })

    jdcheckbox[0].onchange = function () {
        for (var i = 0; i < jdcheckbox.length; i++) {
            if (jdcheckbox[0].checked) {
                jdcheckbox[i].checked = jdcheckbox[0].checked
                setTotal()
            } else {
                jdcheckbox[i].checked = false
                $('.price em').html(0)
            }
        }
    }

    jdcheckbox[jdcheckbox.length - 1].onchange = function () {
        for (var i = 0; i < jdcheckbox.length; i++) {
            if (jdcheckbox[jdcheckbox.length - 1].checked) {
                jdcheckbox[i].checked = jdcheckbox[jdcheckbox.length - 1].checked
                setTotal()
            } else {
                jdcheckbox[i].checked = false
                $('.price em').html(0)
            }
        }
    }

    function Fun(li) {
        li.children[2].style = 'display:block;'
        li.style = ''
        li.children[0].style = '';
    }

    function Fun1(li) {
        li.children[2].style = 'display:none;'
        li.style = ''
        li.children[0].style = '';
    }

    li.onmouseover = function () {
        Fun(li)
    }

    li.onmouseout = function () {
        Fun1(li)
    }

    li1.onmouseover = function () {
        Fun(li1)
    }

    li1.onmouseout = function () {
        Fun1(li1)
    }

    li2.onmouseover = function () {
        Fun(li2)
    }

    li2.onmouseout = function () {
        Fun1(li2)
    }


    li3.onmouseover = function () {
        Fun(li3)
    }

    li3.onmouseout = function () {
        Fun1(li3)
    }

    var msg = $.cookie('haier-cart')
    var arr = [...JSON.parse(msg)]
    function findAllDepts() {
        var a = []
        for (var i = 0; i < arr.length; i++) {
            a.push('<div class="cart-tbody">',
                '<div class="item-list">',
                '<div class="item-single item-item">',
                '<div class="item-form">',
                '<div class="cell p-checkbox">',
                '<div class="cart-checkbox">',
                '<input p-type="1801383750_1" type="checkbox"name="checkItem" value="" data-bind=', arr[i].id, ' class="jdcheckbox">',
                '</div>',
                '</div>',
                '<div class="cell p-goods">',
                '<div class="goods-item">',
                '<div class="p-img">',
                '<a href="" class="J_zyyhq_1801383750" pnc="false" pncs="">',
                '<img class="imgs" alt = "中保（ZhB）41%草甘膦异丙胺盐水剂 园林小区庭院农田果园铁路通用烂根除草剂 1000克/瓶" src = "http://localhost:2468/uploads/' + arr[i].imgs + '">',
                '</a >',
                '</div >',
                '<div class="item-msg">',
                '<div class= "p-name" >',
                '<a href="">' + arr[i].title + '</a>',
                '</div>',
                '</div >',
                '<div class="p-extend p-extend-new">',
                '<span class="promise" style="display:none"></span>',
                '</div>',
                '</div >',
                '</div >',
                '</div >',
                ' <div class="cell p-props p-props-new">',
                ' <div class="props-txt"></div>',
                ' </div>',
                '<div class="cell p-price p-price-new" style="margin-left: 30px;">',
                '<p class="plus-switch">',
                '<strong class="str">' + arr[i].price + '</strong>',
                '</p>',
                ' <div>',
                '<div class="clr"></div>',
                '</div>',
                '<p class="mt5" jj>',
                '</p>',
                '<p class="mt5" bt></p>',
                '</div>',
                '<div class="cell p-quantity" style="margin-left: 35px;">',
                '<div class="quantity-form">',
                '<a class="decrement disabled" id="decrement_156373_1801383750_1_1">-</a>',
                '<input autocomplete="off" type="text" class="itxt" value="1" id="changeQuantity_156373_1801383750_1_1_0" minnum="1">',
                '<a class="increment" id="increment_156373_1801383750_1_1_0">+</a>',
                '</div>',
                '<div class="ac ftx-03 quantity-txt" _stock="stock_1801383750">有货</div>',
                ' </div>',
                '<div class="cell p-sum" style="margin-left: 10px;">',
                '<strong class="str1">' + arr[i].price + '</strong>',
                ' </div>',
                '<div class="cell p-ops" style="margin-left: 25px;">',
                '<a data-idx=', arr[i].id, ' class="cart-remove" >删除</a>',
                '</div>',
                '</div>',
                '</div >',
                '<div class="item-extra mb10"></div>',
                '</div >',
                '</div > ')
        }
        $('.cart-item-list').html(a.join(''))
    }

    findAllDepts()

    //加减数量
    $('.cart-item-list').on('click', '.increment', function (e) {
        var t = $(this).parent().find('input[class*=itxt]');
        var v = $(this).parent().parent().prev().find('strong[class*=str]').text() * 1
        t.val(parseInt(t.val()) + 1)
        var mny = t.val() * 1 * v;
        $(this).parent().parent().next().find('strong[class*=str]').html(mny)
        // $('.price em').html(mny)
        for (let i = 0; i < jdcheckbox.length; i++) {
            if (jdcheckbox[i].checked == true) {
                setTotal()
            }
        }
    })

    $('.cart-item-list').on('click', '.decrement', function () {
        var t = $(this).parent().find('input[class*=itxt]');
        var v = $(this).parent().parent().prev().find('strong[class*=str]').text() * 1
        t.val(parseInt(t.val()) - 1)
        var mny = t.val() * 1 * v;
        $(this).parent().parent().next().find('strong[class*=str]').html(mny)
        if (t.val(0)) {
            t.val(1)
            $(this).parent().parent().next().find('strong[class*=str]').html(v)
        }
        for (let i = 0; i < jdcheckbox.length; i++) {
            if (jdcheckbox[i].checked == true) {
                setTotal()
            }
        }
    })


    // 价钱
    function setTotal() {
        var summer = 0;
        $('.cart-item-list').on('click', '.item-list', function () {
            var target = $(this);
            for (let i = 0; i < target.length; i++) {
                var $tr = target.eq(i);
                var my = $tr.find('strong[class*=str1]').text()
                summer += parseFloat(my)
            }
            $('.price em').html(summer)
            // totalNum = summer
        })
        $('.item-list').click()
        summer = 0
    }


    //删除
    $('.cart-item-list').on('click', '.cart-remove', function () {
        var $this = $(this).attr("data-idx");
        arr = arr.filter(function (a) {
            return a.id != $this
        });
        msg = JSON.stringify(arr)
        findAllDepts()
    })

    $('.cleaner-opt').click(function () {
        arr = []
        findAllDepts()
    })

    $('.submit-btn').click(function () {
        var obj = []
        if ($('.price em').html().trim() != '¥0.00') {
            obj.push({ orderDetail: JSON.stringify(arr),total: $('.price em').html(),orderTime: new Date().toLocaleDateString(),status: 0 })
            console.log(obj[0])
            $.ajax({
                type:'POST',
                url:'http://localhost:2468/order/addList',
                data:obj[0],
                dataType:'json',
                success:function(data){
                    console.log(data)
                    $.cookie('haier-cart', null)
                    window.location.href = './home.html'
                }
            })
        }
    })
}())


