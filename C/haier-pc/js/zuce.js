; (function (window, document, $, undefined) {
    var baseUrl = 'http://localhost:2468';
    var $zcBtn = $('.btn'),  //注册按钮
        $shoujiipt = $('.shouji1'),
        $mimaipt = $('.mima1'),
        $mimasipt = $('.mimas1'),
        $isTrues = $('#isTrues'),  //是否阅读
        $alert = $('.alert'),
        $alertSpan = $('.alert span'),
        imgFilename = ''
    //上传图片
    $('.img-upload').on('change',function () {
        let fd = new FormData()
        fd.append('file',document.getElementById("imgs").files[0])
        let xhr = new XMLHttpRequest()
        xhr.open('post',baseUrl + '/upload/upload')
        xhr.send(fd)
        xhr.onload = function(){
            var res;
            if( this.status == 200 || readyState == 4 ){
                res = JSON.parse(this.responseText)
                imgFilename = res.data
            }
        }
        // $.ajax({
        //     url:baseUrl + '/upload/upload',
        //     type:'post',
        //     data:fd,
        //     success(res){
        //         console.log(res)
        //     }
        // })
    })

    $zcBtn.on('click', function () {
        if ($shoujiipt.val() && $mimaipt.val() && $mimasipt.val()) {
            if ($shoujiipt.val().length < 11) {
                alertFun('手机号码不得小于11位')
                return
            } else if ($mimaipt.val().length < 6) {
                alertFun('密码不小于6位')
                return
            } else if ($mimaipt.val() != $mimasipt.val()) {
                alertFun('密码不一致请重新输入')
                return
            } else if (!$isTrues.prop('checked')) {
                alertFun('请您在阅读注册协议后同意并勾选')
                return
            } else {
                $.ajax({
                    url: baseUrl + '/user/register',
                    type: 'post',
                    data: {
                        phoneNum: $shoujiipt.val(),
                        loginKey: $mimasipt.val(),
                        avatar:imgFilename
                    },
                    success: function (res) {
                        console.log(res)
                        if (res.code == 20000) {
                            location.href = "../view/denglu.html"
                            $shoujiipt.val('');
                            $mimaipt.val('');
                            $mimasipt.val('');
                        }
                    }
                })
            }
        } else {
            alertFun('请填写信息')
            setTimeout(() => {
                $alert.hide()
            }, 1000);
        }
    })

    $('.item').each(function (i) {
        $('.item').eq(i).on('focus', function () {
            $alert.hide()
        })
    })

    $isTrues.on('change', function (e) {
        if ($isTrues.prop('checked')) {
            $alert.hide()
        }
    })

    function alertFun(val) {
        $alertSpan.html(val)
        $alert.toggle()
    }

}(window, document, jQuery))