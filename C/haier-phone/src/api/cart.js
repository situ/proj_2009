import request from '../utils/axios'

export function getRanList() {
    return request({
        url: "product/ranList",
        method: "get"
    })
}

export function getList() {
    return request({
        url: "product/list",
        method: "get"
    })
}

export function addOrder(data){
    return request({
        url: "order/addList",
        method: "post",
        data
    })
}