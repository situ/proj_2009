import request from '../utils/axios'

export function getSummary() {
    return request({
        url: "summary/list",
        method: "get"
    })
}

export function getXinxuan(params) {
    return request({
        url: "product/classList",
        method: "get",
        params
    })
}
export function getClassify() {
    return request({
        url: "classify/list",
        method: "get"
    })
}