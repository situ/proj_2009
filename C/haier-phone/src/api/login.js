import request from '@/utils/axios'

//登录
export let getLogin = (data) => {
    return request({
        url: "user/login",
        method: "post",
        data
    })
}

//注册
export let getLogon = (data) => {
    return request({
        url: "user/register",
        method: "post",
        data
    })
}

//获取用户信息
export let getInfo = (params) => {
    console.log(params)
    return request({
        url: "user/getInfo",
        method: "get",
        params
    })
}