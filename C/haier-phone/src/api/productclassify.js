import request from "@/utils/axios.js"

export let getClassify = (data) => {
    return request({
        url: "classify/list",
        method: "get"
    })
}