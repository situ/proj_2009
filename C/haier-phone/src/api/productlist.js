import request from "@/utils/axios.js"

export let getProductClassifyData = (data) => {
    return request({
        url: "product/classList",
        method: "get",
        params: {
            ...data
        }
    })
}