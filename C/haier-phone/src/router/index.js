import Vue from "vue"
import VueRouter from "vue-router"

Vue.use(VueRouter)
// import ProductList from "@/views/ProductList"
// import Detail from "@/views/Detail"

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: () => import('../views/Home.vue')
        },
        {
            path: '/cart',
            name: 'Cart',
            component: () => import('../views/Cart')
        },
        {
            path: '/denglu',
            name: 'Denglu',
            component: () => import('../views/login/Denglu')
        },
        {
            path: '/zuce',
            name: 'Zuce',
            component: () => import('../views/login/Zuce')
        },
        {
            path: '/user',
            name: 'User',
            component: () => import('../views/login/User')
        },
        {
            path: "/productlist",
            // component : ()=>import("@/views/ProductList")
            component: resolve => require(['@/views/ProductList'], resolve)
        },
        {
            path: "/detail",
            // component : ()=>import("@/views/Detail")
            component: resolve => require(['@/views/Detail'], resolve)
        },
        {
            path: "/productclassify",
            // component : ()=>import("@/views/productclassify")
            component: resolve => require(['@/views/ProductClassify'], resolve)
        }
    ]
})