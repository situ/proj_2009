import Vue from "vue"
import Vuex from "vuex"
import cart from './modules/cart'
import productlist from "@/store/modules/productlist"
import productclassify from "@/store/modules/productclassify"
import home from "@/store/modules/home"
import login from "@/store/modules/login"

Vue.use(Vuex)


let state = {}

let mutations = {}

let actions = {}

let getters = {}

let modules = {
    productlist,
    cart,
    productclassify,
    home,
    login
}

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters,
    modules
})