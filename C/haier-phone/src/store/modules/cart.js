import { getRanList, getList ,addOrder} from '../../api/cart'
import {getCart,removeCart} from '../../utils/cart'

export default {
    namespaced: true,
    state: {
        testArr: [],
        testLis: [],
        totalNum: 0,
        totalPrice: 0
    },
    mutations: {
        SET_LIST(state, arr) {
            let lis = [...arr];
            lis.map(val => {
                val.num = 1
                val.checked = true
                return val
            });
            state.testArr = lis
        },
        SET_RAN(state, arr) {
            state.testLis = arr
        },
        SET_ALLNUM(state, arr) {
            let lis = [...arr];
            let list = lis.filter(val => val.checked == true)
            state.totalNum = list.reduce((total, v) => {
                return total + v.num;
            }, 0);
        },
        SET_ALLPRICE(state, arr) {
            let lis = [...arr];
            let list = lis.filter(val => val.checked == true)
            state.totalPrice = list.reduce((total, v) => {
                if (v.checked) {
                    return total + v.num * v.price;
                }
            }, 0);
        }
    },
    actions: {
        async getRanList({ commit }) {
            let r = await getRanList()
            commit('SET_RAN', r.data)
        },
        async getList({ commit }) {
            // let r = await getList()
            let arr = getCart()
            console.log(arr)
            commit('SET_LIST', arr)
            commit('SET_ALLNUM', arr)
            commit('SET_ALLPRICE', arr)
        },
        async addOrder({},data){
            let r = await addOrder(data)
            console.log(r)
        }
    },
    getters: {

    }
}