import { getSummary, getXinxuan, getClassify } from '../../api/home'

export default {
    namespaced: true,
    state: {
        summaryArr: [],
        xinxuanArr: [],
        classifyArr: [],
        paihangArr: []
    },
    mutations: {
        GETSUM(state, data) {
            data.splice(6)
            state.summaryArr = data
        },
        GETXINXUAN(state, data) {
            data.splice(7)
            state.xinxuanArr = data
        },
        GETPAIHANG(state, data) {
            let newData = data.splice(1, 3)
            state.paihangArr = newData
        },
        GETCLASS(state, data) {
            state.classifyArr = data
        }
    },
    actions: {
        async getSummary({ commit }) {
            let a = await getSummary()
            commit("GETSUM", a.data)
        },
        async getClassify({ commit }) {
            let c = await getClassify()
            // console.log(c.data)
            commit("GETCLASS", c.data)
        },
        async getXinxuan({ commit }, id) {

            let b = await getXinxuan({ sid: id })
            // console.log(b.data)
            commit("GETXINXUAN", b.data)
        },

        async getPaihang({ commit }, id) {
            let d = await getXinxuan({ cid: id })
            // console.log(d.data)
            commit("GETPAIHANG", d.data)
        }
    },
    getters: {

    }
}