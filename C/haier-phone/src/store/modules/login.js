import { getLogin, getInfo, getLogon } from '@/api/login'
import { getToken, setToken, removeToken } from '../../utils/cookie'
import { setInfo, removeInfo } from '../../utils/user'
import { removeCart } from '../../utils/cart'

export default {
    namespaced: true,
    state: {
        user: {},
        token: getToken()
    },
    mutations: {

    },
    actions: {
        async getLogin({ state,commit }, login) {
            let r = await getLogin({ ...login })
            // console.log(r)
            if (r.code == 20000) {
                setToken(r.data.token)
                state.token = r.data.token
            }
            return r
        },
        async getInfo({ state, commit }) {
            console.log(state.token)
            let r = await getInfo({ token: state.token })
            setInfo(r.data[0])
            if (r.code == 20000);
            return r.code == 20000
        },
        logout() {
            removeToken()
            removeInfo()
            removeCart()
        },
        resetToken() {
            removeToken()
        },
        async getLogon({ }, data) {
            let r = await getLogon({ ...data })
            console.log(r)
        },
    },
    modules: {}
}