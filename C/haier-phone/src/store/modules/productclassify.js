import Vue from "vue"
import Vuex from "vuex"
import { getClassify } from "@/api/productclassify"
Vue.use(Vuex)

let state = {
    classify : []
}

let mutations = {
    'GET_CLASSIFY'(state,data){
        data.splice(7)
        state.classify = data
    }
}

let actions = {
    async getClassify({commit}) {
        let r = await getClassify()
        commit("GET_CLASSIFY",r.data)
    }
}

let getters = {}


export default {
    namespaced : true,
    state,
    mutations,
    actions,
    getters
}