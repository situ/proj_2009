import axios from 'axios'
import qs from 'qs'

let request = axios.create({
    baseURL: 'http://localhost:2468/',       // 路径开头部分
    // timeout: 5000                         // 请求超时时间
})

// 前台向后台发送数据时 封装拦截器
request.interceptors.request.use(
    config => {
        config.method == 'post' ? config.data = qs.stringify({
            ...config.data
        }) : config.params = {
            ...config.params
        }
        config.headers['Content-Type'] = 'application/X-www-form-urlencoded';
        return config
    },
    err => {
        return Promise.reject(err)
    }
)

// 后台向前台返回数据时 封装拦截器
request.interceptors.response.use(
    response => {
        return response.data
    },
    error => {
        return Promise.reject(error)
    }
)

export default request