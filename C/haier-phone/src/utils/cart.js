import Cookie from 'js-cookie'

export function setCart(obj){
    let arr = getCart()
    arr.push(obj)
    Cookie.set('haier-cart',arr)
}

export function getCart(){
   return JSON.parse(Cookie.get('haier-cart') || '[]') 
}

export function removeCart(obj){
    Cookie.remove('haier-cart')
}