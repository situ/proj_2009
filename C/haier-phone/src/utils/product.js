import Cookie from 'js-cookie'

export function setMsg(obj) {
    Cookie.set('haier-sp', obj)
}

export function getMsg(obj) {
    return JSON.parse(Cookie.get('haier-sp') || '{}')
}

export function removeMsg(obj) {
    Cookie.remove('haier-sp')
}