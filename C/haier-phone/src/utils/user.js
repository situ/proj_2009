import Cookie from 'js-cookie'

export function setInfo(obj){
    Cookie.set('user-info',obj)
}

export function getInfo(){
   return JSON.parse(Cookie.get('user-info') || '{}') 
}

export function removeInfo(){
    Cookie.remove('user-info')
}