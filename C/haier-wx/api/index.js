let request = require("../utils/request")

let getSummary = () => {
  return request({
    url: "summary/list",
    method: "get"
  })
}

let getXinxuan = (data) => {
  return request({
    url: "product/classList",
    method: "get",
    data
  })
}
let getClassify = () => {
  return request({
    url: "classify/list",
    method: "get"
  })
}


module.exports = {
  getSummary,
  getXinxuan,
  getClassify
}