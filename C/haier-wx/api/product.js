let request = require("../utils/request")

//获取产品类别
let getProductClassify = (data) =>{
  return request({
      url : "classify/list",
      method :"get"
  })
}

module.exports = {
  getProductClassify
}