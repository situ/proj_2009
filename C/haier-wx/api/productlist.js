let request = require("../utils/request")

//根据产品类别获取产品列表
let getProductList = (data) =>{
  return request({
      url : "product/classList",
      method :"get",
      data
  })
}

module.exports = {
  getProductList
}