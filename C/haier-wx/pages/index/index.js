let {
  getSummary,
  getXinxuan,
  getClassify
} = require("../../api/index")

Page({
  /**
   * 页面的初始数据
   */


  data: {
    background: ['../../imgs/home/bjt1.webp', '../../imgs/home/bjt2.webp', '../../imgs/home/bjt3.webp', '../../imgs/home/bjt4.webp'],
    backgrounds: ['../../imgs/home/lbt1.png', '../../imgs/home/lbt2.png', '../../imgs/home/lbt3.png', '../../imgs/home/lbt4.png', '../../imgs/home/lbt5.png', '../../imgs/home/lbt6.png', '../../imgs/home/lbt7.png'],
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    interval: 2000,
    duration: 500,
    summaryArr: [],
    xinxuanArr: [],
    classifyArr: [],
    paihangArr: [],
    active: 0,
    ids: 1,
    idc: 1
  },

  //跳转详情
  goDetail(event){
    let msg = JSON.stringify(event.currentTarget.dataset.item)
    // console.log(msg)
    wx.setStorageSync('haier-sp', msg)
    wx.navigateTo({
      url: '/pages/detail/detail',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let r = await getSummary()
    let y = await getClassify()
    let t = await getXinxuan({
      sid: this.data.ids
    })
    let q = await getXinxuan({
      cid: this.data.idc
    })
    this.setData({
      summaryArr: r.data.splice(0,6),
      xinxuanArr: t.data,
      classifyArr: y.data.splice(0,7),
      paihangArr: q.data.splice(1, 3)
    })
  },

  async onXinxuanClick(event) {
    this.data.ids = this.data.summaryArr[event.detail.index].id;
    let w = await getXinxuan({
      sid: this.data.ids
    })
    this.setData({
      xinxuanArr: w.data
    })
  },
  async onPaihangClick(event) {
    this.data.idc = event.detail.index + 1;
    let w = await getXinxuan({
      cid: this.data.idc
    })
    this.setData({
      paihangArr: w.data.splice(1, 3)
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})