// pages/my/my.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var appInst = getApp()
    if(!appInst.globalData.userInfo) {
      wx.navigateTo({
        url: '/pages/logins/logins',
      })
    } else {
      console.log("已登录")
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var appInst = getApp()
    if(appInst.globalData.userInfo) {
      this.setData({
        avatar:appInst.globalData.userInfo.avatarUrl || "",
        nickName : appInst.globalData.userInfo.nickName || ""
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  dl() {
    wx.navigateTo({
      url: '/pages/logins/logins',
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})