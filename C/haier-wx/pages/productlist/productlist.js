// pages/productlist/productlist.js
let { getProductList } = require("../../api/productlist")
Page({

  /**
   * 页面的初始数据
   */
  data: {
      isShow : "none",
      isFixed : "",
      isW : "",
      isTop:"",
      isAciton: 0,
      productListData:[]
  },
  //跳转详情
  goDetail(event){
    let msg = JSON.stringify(event.currentTarget.dataset.item)
    wx.setStorageSync('haier-sp', msg)
    wx.navigateTo({
      url: '/pages/detail/detail',
    })
  },
  //tab切换
  barClk:function(event) {
    let index = event.target.dataset.index
      this.setData({
        isAciton : index
      })
  },
  // 监听页面滚动事件
  onPageScroll:function(e){
    if (e.scrollTop.toFixed(0) * 1 > 230) {
      this.setData({
        isFixed : "fixed",
        isW : "92%",
        isTop:"0px"
      })
    } else {
      this.setData({
        isFixed : "",
        isW : "",
        isTop:""
      })
    }
    if (e.scrollTop.toFixed(0) * 1 > 500) {
      this.setData({
        isShow : "flex"
      })
    } else {
      this.setData({
        isShow : "none"
      })
    }
  },
  //点击回到顶部
  backTop:function() {
    if (wx.pageScrollTo) {
      wx.pageScrollTo({
        scrollTop: 0
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    let r = await getProductList({cid:options.id})
    this.setData({
      productListData : r.data
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})