let baseurl = "http://localhost:2468/"

let request = ({ url, data, method }) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: baseurl + url,
      data,
      method,
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success(res) {
        // console.log(res)
        resolve(res.data)
      }
    })
  })
}

module.exports = request