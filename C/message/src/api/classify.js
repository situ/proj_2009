import request from '@/utils/request'
import qs from 'qs'

export function getClassList(active){
    return request({
        url:active?'classify/list':'summary/list',
        method:'get'
    })
}

export function saveClassify({active,form}){
    // console.log(form)
    return request({
        method:'post',
        url:active?'classify/updateList':'summary/updateList',
        data:qs.stringify(form)
    })
}

export function addClassify({active,form}){
    return request({
        method:'post',
        url:active?'classify/addList':'summary/addList',
        data:qs.stringify(form)
    })
}

export function removeFun({active,id}){
    return request({
        method:'get',
        url:active?'classify/delList':'summary/delList',
        params:{id}
    })
}