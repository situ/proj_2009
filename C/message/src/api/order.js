import request from '@/utils/request'
import qs from 'qs'

export function getList(params) {
    return request({
        url: 'order/list',
        method: 'get',
        params
    })
}

export function saveList(data) {
    return request({
        url: data.id ? 'order/updateList' : 'order/addList',
        method: 'post',
        data: qs.stringify(data)
    })
}

export function deleteList(id) {
    return request({
        url: 'order/delList',
        method: 'get',
        params: {
            id
        }
    })
}