import request from '@/utils/request'
import qs from 'qs'

export function getList(params) {
  return request({
    url: 'product/list',
    method: 'get',
    params
  })
}

export function saveList(data) {
  return request({
    url: data.id?'product/update':'product/save',
    method: 'post',
    data:qs.stringify(data)
  })
}

export function deleteList(params) {
  return request({
    url: 'product/delete',
    method: 'get',
    params
  })
}
