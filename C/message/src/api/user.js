import request from '@/utils/request'
import qs from 'qs'

export function login(data) {
  return request({
    url: 'user/login',
    method: 'post',
    data:qs.stringify(data)
  })
}

export function getInfo(token) {
  return request({
    url: 'user/getInfo',
    method: 'get',
    params: { token }
  })
}

export function logout(token) {
  return request({
    url: 'user/logout',
    method: 'post',
    data:qs.stringify(token)
  })
}
