import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },

  {
    path: '/example',
    component: Layout,
    redirect: '/example/product',
    name: 'Example',
    meta: { title: '商品管理', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'product',
        name: 'Product',
        component: () => import('@/views/product/index'),
        meta: { title: 'Product', icon: 'table' }
      },
      {
        path: 'classify',
        name: 'Classify',
        component: () => import('@/views/classify/index'),
        meta: { title: 'Classify', icon: 'tree' }
      },
      {
        path: 'summary',
        name: 'Summary',
        component: () => import('@/views/classify/index'),
        meta: { title: 'Summary', icon: 'tree' }
      }
    ]
  },

  {
    path: '/order',
    component: Layout,
    children: [{
      path: 'index',
      name: 'Order',
      component: () => import('@/views/order/index'),
        meta: { title: 'Order', icon: 'el-icon-document' }
    }]
  },
  // {
  //   path: '/users',
  //   component: Layout,
  //   redirect: '/users/index',
  //   name: 'users',
  //   meta: { title: '用户管理', icon: 'el-icon-collection' },
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'UsersList',
  //       component: () => import('../views/users/index'),
  //       meta: { title: '未审核用户', iocn: 'el-iocn-s-data' }
  //     }, {
  //       path: 'index2',
  //       name: 'UsersList2',
  //       component: () => import('../views/users/index'),
  //       meta: { title: '正式用户', iocn: 'el-icon-truck' }
  //     }
  //   ]
  // },
  // {
  //   path: '/permission',
  //   component: Layout,
  //   redirect: '/permission/index',
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Permission',
  //       component: () => import('@/views/permission/index.vue'),
  //       meta: { title: '权限管理', icon: 'el-icon-s-custom' }
  //     }
  //   ]
  // },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

// export const asyncRoutes = [
//   {
//     path: '/permission',
//     component: Layout,
//     redirect: '/permission/index',
//     children: [
//       {
//         path: 'index',
//         name: 'Permission',
//         component: () => import('@/views/permission/index.vue'),
//         meta: { title: '权限管理', icon: 'el-icon-s-custom' }
//       }
//     ]
//   }
// ]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
