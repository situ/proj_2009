import {getClassList,saveClassify, addClassify, removeFun} from '../../api/classify'

export default {
    namespaced: true,
    state:{
        classifyArr: [],
        classifyLis:[]
    },
    mutations:{
        setClassify(state, arr) {
            let data = arr.map(val => {
                val.isInput = false
                return val
            });
            state.classifyArr = [...state.classifyArr, ...data]
        },
        SETLIS(state,arr){
            state.classifyLis = arr
        }
    },
    actions:{
        async getClassList({commit,state},active){
            let {data} = await getClassList(active)
            let obj = {
                isInput: true,
                isFirst: true,
            }
            obj[active?'type':'title']=''
            state.classifyArr = [obj]
            commit('setClassify', data)
            commit('SETLIS',data)
            return data
        },
        async saveClassify({ commit, dispatch },{active,form}) {
            console.log(active,form)
            await saveClassify({active,form})
            dispatch('getClassList',active)
        },
        async addClassify({ dispatch }, {active,form}) {
            await addClassify({active,form})
            dispatch('getClassList',active)
        },
        async deleteFun({ dispatch },{active,id}) {
            // console.log(active,id)
            await removeFun({active,id})
            dispatch('getClassList',active)
        }
    },
    getters:{
        typeList(state){
            console.log(state)
        }
    }
}