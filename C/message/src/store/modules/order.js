import { getList, saveList, deleteList } from '../../api/order'

export default {
    namespaced: true,
    state: {
        data: [],
        total: 0,
        pages: {
            currentPage: 1,
            pagesize: 5,
            keyword: '',
            order: 'id',
            by: 'asc'
        }
    },
    mutations: {
        SET_GETDATE(state, res) {
            state.data = res.data.map(val=>{
                if(val.status == '0'){
                    val.status = '未结账'
                }else {
                    val.status = '已结账'
                }
                val.orderDetail = JSON.parse(val.orderDetail)
                return val
            })
            state.total = res.total
        },
        SET_KEYWORD(state, keyword) {
            state.pages.keyword = keyword
        },
        SET_ORDER(state, { prop, order }) {
            if (order == 'ascending') {
                state.pages.order = prop
                state.pages.by = 'asc'
            } else if (order == 'descending') {
                state.pages.order = prop
                state.pages.by = 'desc'
            } else {
                state.pages.order = 'id'
                state.pages.by = 'desc'
            }
        },
        SET_CURRENTPAGE(state, n) {
            state.pages.currentPage = n
        },
    },
    actions: {
        async getDate({ commit, state }) {
            let pages = { ...state.pages }
            pages.currentPage = pages.currentPage - 1
            let r = await getList(pages)
            commit('SET_GETDATE', r)
        },
        async saveList({ dispatch }, data) {
            let r = await saveList(data)
            dispatch('getDate')
        },
        async deleteList({ dispatch }, id) {
            let r = await deleteList(id)
            dispatch('getDate')
        }
    },
    getters: {}
}