import {getList,saveList,deleteList} from '../../api/product'

export default {
    namespaced: true,
    state:{
        proList:[],
        pages: {
            currentPage: 1,
            pageSize: 20,
            keyword: '',
            order: 'id',
            by: 'asc'
        },
        total: 0
    },
    mutations:{
        SET_LIST(state,arr){
            state.proList = arr
        },
        SET_TOTAL(state, n) {
            state.total = n
        },
        SET_CURRENTPAGE(state,n){
            state.pages.currentPage = n
        },
        SET_KEYWORD(state,val){
            state.pages.keyword = val
        },
        SET_ORDER(state,{prop,order}){
            if(order == 'ascending'){
                state.pages.order = prop
                state.pages.by = 'asc'
            }else if(order == 'descending') {
                state.pages.order = prop
                state.pages.by = 'desc'
            }else {
                state.pages.order = 'id'
                state.pages.by = 'desc'
            }
        }
    },
    actions:{
        async getList({commit,state}){
            let pages = { ...state.pages }
            pages.currentPage = pages.currentPage - 1
            let res = await getList(pages)
            commit('SET_LIST',res.data)
            commit('SET_TOTAL', res.total)
        },
        async saveList({dispatch},data){
            let r = await saveList(data)
            dispatch('getList')
        },
        async deleteList({dispatch},ids){
            let r = await deleteList(ids)
            dispatch('getList')
        }
    },
    getters:{}
}