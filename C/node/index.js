let express = require('express');
let app = express();

let user = require('./modules/user')
let classify = require('./modules/classify')
let roles = require('./modules/roles')
let product = require('./modules/product')
let upload = require('./modules/upload')
let summary = require('./modules/summary')
let order = require('./modules/order')

app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Accept,X-Token,x-access-token");
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});

app.get('/uploads/*', function (req, res) {
    // console.log(req.url)
    res.sendFile(__dirname + '/' + req.url)
})

app.use('/classify', classify)
app.use('/user', user)
app.use('/roles', roles)
app.use('/product', product)
app.use('/upload', upload)
app.use('/summary', summary)
app.use('/order', order)


let server = app.listen(2468, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("应用实例，访问地址为 http://%s:%s", host, port)

})
