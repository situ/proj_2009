let express = require('express');
let db = require('../db.js')
let router = express.Router()
let bodyParser = require('body-parser');
let urlencodedParser = bodyParser.urlencoded({ extended: false })

router.get('/list', async function (req, res) {
    let sql = `select * from classify order by id asc`;
    let r = await db.doSql(sql)
    res.send(db.getData(r))
})

router.post('/addList', urlencodedParser, async function (req, res) {
    let { type } = req.body
    let sql = `insert into classify (type) values ('${type}')`;
    let r = await db.doSql(sql)
    res.send(db.addData(r))
})

router.post('/updateList', urlencodedParser, async function (req, res) {
    let { id, type } = req.body
    let sql = `update classify set type='${type}' where id='${id}'`
    let r = await db.doSql(sql)
    res.send(db.updateData(r))
})

router.get('/delList', async function (req, res) {
    let { id }= req.query
    let sql = `delete from classify where id in(${id})`;
    let r = await db.doSql(sql)
    res.send(db.deleteData(r))
})

module.exports = router