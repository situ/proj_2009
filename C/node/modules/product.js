var express = require('express'); //引入框架
var db = require('../db.js')
var router = express.Router();

// post 的第二个参数
var bodyParser = require('body-parser');  //nodejs 使用 body-parser 获取网页内容 
var urlencodedParser = bodyParser.urlencoded({
    extended: false
})

//获取商品信息
router.get('/list', async function (req, res) {
    let { currentPage = 0, pagesize = 20, order = 'id', by = 'asc', keyword } = req.query

    let sql = `select p.id,p.title,p.vertion,p.price,p.imgs,p.cid,p.sid,c.type,s.title as classify from product as p,classify as c,summary as s where p.cid=c.id and p.sid=s.id`;
    let sqla = `select count(*) as total from product as p,classify as c,summary as s where p.cid=c.id and p.sid=s.id`;

    if (keyword) {
        sql += ` and p.title like '%${keyword}%'`
        sqla += ` and p.title like '%${keyword}%'`
    }
    if (order) {
        sql += ` order by p.${order} ${by}`
    }
    sql += ` limit ${currentPage * pagesize},${pagesize}`
    // console.log(sql)
    let p1 = await db.doSql(sql)
    let p2 = await db.doSql(sqla)
    res.send(db.hand(p1, p2[0].total))
})

//根据分类获取数据
router.get('/classList', async function (req, res) {
    let { currentPage = 0, pagesize = 10, order = 'id', by = 'asc', keyword, cid,sid } = req.query
    console.log(req.query,555)
    console.log(cid,123)
    let sql = ''
    let sqla = ''
    if (cid) {
        sql = `select p.id,p.title,p.vertion,p.price,p.imgs,c.type from product as p,classify as c where p.cid='${cid}' and p.cid=c.id`;
        sqla = `select count(*) as total from product as p,classify as c where p.cid=c.id`;
    } 
    
    if (sid) {
        // console.log(1)
        sql = `select p.id,p.title,p.vertion,p.price,p.imgs,s.title as type from product as p,summary as s where p.sid='${sid}' and p.sid=s.id `;
        sqla = `select count(*) as total from product as p,summary as s where p.sid=s.id`;
    }

    if (keyword) {
        sql += ` p.title like '%${keyword}%'`
        sqla += ` and p.title like '%${keyword}%'`
    }
    if (order) {
        sql += ` order by p.${order} ${by}`
    }
    sql += ` limit ${currentPage * pagesize},${pagesize}`
    let p1 = await db.doSql(sql)
    let p2 = await db.doSql(sqla)
    res.send(db.hand(p1, p2[0].total))
})

//获取随机商品
router.get('/ranList', async function (req, res) {
    let {num = 6} = req.query

    let sql = `select p.id,p.title,p.vertion,p.price,p.imgs,p.cid,p.sid,c.type,s.title as classify from product as p,classify as c,summary as s where p.cid=c.id and p.sid=s.id`;
    sql += ` order by rand() limit ${num}`
    // console.log(sql)
    let p1 = await db.doSql(sql)
    res.send(db.getData(p1))
})

router.post('/save', urlencodedParser, async function (req, res) {
    let { title, vertion, price, imgs, cid = 8, sid=7} = req.body
    let sql = `insert into product (title,vertion,price,imgs,cid,sid) values ('${title}','${vertion}','${price}','${imgs}','${cid}','${sid}')`;
    let r = await db.doSql(sql)
    res.send(db.addData(r))
})

router.post('/update', urlencodedParser, async function (req, res) {
    let { id, title, vertion, price, imgs, cid, sid } = req.body
    let sql = ''
    // if (cid) {
        sql = `update product set title='${title}',vertion="${vertion}",price='${price}',imgs='${imgs}',cid='${cid}',sid='${sid}' where id='${id}'`
    // } else if (sid) {
    //     sql = `update product set title='${title}',vertion="${vertion}",price='${price}',imgs='${imgs}',sid='${sid}' where id='${id}'`
    // }
    let r = await db.doSql(sql)
    res.send(db.updateData(r))
})

router.get('/delete', async function (req, res) {
    let { id } = req.query
    let sql = `delete from product where id in(${id})`;
    let r = await db.doSql(sql)
    res.send(db.deleteData(r))
})



module.exports = router