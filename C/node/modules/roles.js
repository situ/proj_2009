let express = require('express');
let db = require('../db.js')
let router = express.Router()
let bodyParser = require('body-parser');
let urlencodedParse = bodyParser.urlencoded({ extended: false })

router.get('/rolesList',async (req,res)=>{
    let {roles} = req.query
    let sql = `select items from roles where role in ('${roles}')`
    let r = await db.doSql(sql)
    res.send(db.getData(r[0].items))
})

//角色
router.post('/save-permission',urlencodedParse,async (req,res)=>{
    let {id,items} = req.body
    let sql =`update roles set items=? where id=?`
    let r = await db.doSql(sql,[items,id])
    res.send(db.updateData(r))
})

router.get('/getRoutesByRoles ',async (req,res)=>{
    let sql = `select * from roles`
    let r = await db.doSql(sql)
    res.send(db.getData(r))
})

router.post('/save',urlencodedParse,async (req,res)=>{
    let { role,title } = req.body
    let sql = `insert into roles (role,title) values ('${role}','${title}')`
    let r = await db.doSql(sql)
    res.send(db.addData(r))
})

router.post('/updateRole',urlencodedParse,async (req,res)=>{
    let {id,role,title} = req.body
    let sql =`update roles set role='${role}',title='${title}' where id='${id}'`
    let r = await db.doSql(sql)
    res.send(db.updateData(r))
})

router.get('/rolesDelete',async (req,res)=>{
    let {id} = req.query
    let sql = `delete from roles where id='${id}'`
    let r = await db.doSql(sql)
    res.send(db.deleteData(r))
})

module.exports = router