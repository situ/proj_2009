let express = require('express');
let db = require('../db.js')
let router = express.Router()
let bodyParser = require('body-parser');
let urlencodedParser = bodyParser.urlencoded({ extended: false })

router.get('/list', async function (req, res) {
    let sql = `select * from summary order by id asc`;
    let r = await db.doSql(sql)
    res.send(db.getData(r))
})

router.post('/addList', urlencodedParser, async function (req, res) {
    let { title } = req.body
    let sql = `insert into summary (title) values ('${title}')`;
    let r = await db.doSql(sql)
    res.send(db.addData(r))
})

router.post('/updateList', urlencodedParser, async function (req, res) {
    let { id, title } = req.body
    let sql = `update summary set title='${title}' where id='${id}'`
    let r = await db.doSql(sql)
    res.send(db.updateData(r))
})

router.get('/delList', async function (req, res) {
    let { id }= req.query
    let sql = `delete from summary where id in(${id})`;
    let r = await db.doSql(sql)
    res.send(db.deleteData(r))
})

module.exports = router