let express = require('express')
let router = express.Router()
let multer = require('multer')

var uploads = multer({ dest: 'uploads/' })


//配置diskStorage来控制文件存储的位置以及文件名字等
var storage = multer.diskStorage({
    //确定图片存储的位置
    destination: function (req, file, cb) {
        cb(null, './uploads')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname)
    }
});

var upload = multer({storage: storage});

//接收上传图片请求的接口
router.post('/upload',upload.single('file') , function (req, res, next) {
                    //多个图片 upload.array('file',[10])   //多个文件 FormData() e.forRach(e=>{fd.apped('file',e)})
    console.log(req.file.filename)
    // var url = '/uploadImgs/' + req.file.filename
    res.json({
        code: 200,
        data: req.file.filename
    })
});


module.exports = router