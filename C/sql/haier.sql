-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2020-09-03 16:45:27
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `haier`
--

-- --------------------------------------------------------

--
-- 表的结构 `classify`
--

CREATE TABLE IF NOT EXISTS `classify` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `classify`
--

INSERT INTO `classify` (`id`, `type`) VALUES
(1, '空调'),
(2, '冰箱'),
(3, '洗衣机'),
(4, '热水器'),
(5, '电视'),
(6, '油烟机'),
(7, '燃气灶'),
(8, '');

-- --------------------------------------------------------

--
-- 表的结构 `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL COMMENT '商品名称',
  `vertion` varchar(50) NOT NULL COMMENT '版本',
  `price` int(10) NOT NULL COMMENT '价格',
  `imgs` varchar(1000) NOT NULL COMMENT '图片',
  `cid` int(20) NOT NULL COMMENT '分类',
  `sid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=126 ;

--
-- 转存表中的数据 `product`
--

INSERT INTO `product` (`id`, `title`, `vertion`, `price`, `imgs`, `cid`, `sid`) VALUES
(5, '先行者 3匹柜式变频空调', 'KFR-72LW/17EDS21AU1', 8599, '1599113731096zh-kfr.png', 1, 1),
(6, '10公斤纤合滚筒洗烘一体机', 'FAW10HD996LSU1', 8799, '1599114009907zh-faw.png', 3, 1),
(7, '海尔50英寸智能电视', '50T86', 3998, '1599114346103zh-50.png', 5, 1),
(8, '60升变频速热横式电热水器', 'EC6002-V5(U1)', 1399, '1599114392811zh-ec.png', 4, 1),
(9, '近吸式三速电子触摸按键吸油烟机', 'CXW-200-E900T6(J)', 2199, '1599114485394zh-cxw.png', 6, 1),
(10, '530升风冷变频十字对开门冰箱', 'BCD-530WDEAU1', 5999, '1599114591454jk-bcd.png', 2, 2),
(11, '节能风 1.5匹壁挂式变频空调', 'KFR-35GW/03JDM81A', 5999, '1599114672145jk-kfr.png', 1, 2),
(12, '16升微颗粒自清扫燃气热水器', 'JSQ31-16JN5(12T)', 2799, '1599114748515jk-jsq.png', 4, 2),
(13, '90升嵌入式光波巴氏消毒柜', 'ZQD90F-12LCS', 1599, '1599114834436jk-zqd.png', 8, 2),
(14, '9公斤免清洗波轮洗衣机', 'MS90-BZ976U1', 4599, '1599114940891jk-ms.png', 3, 2),
(15, '171瓶装酒柜', 'WS171', 4899, '1599115003187rq-ws.png', 8, 3),
(16, '518升冷藏冷冻转换柜', 'BC/BD-518HD', 3099, '1599115091439rq-bc.png', 2, 3),
(17, '306升风冷变频三门冰箱', 'BCD-306WDGRU1', 4399, '1599115182749rq-bcd..png', 2, 3),
(18, '157升家庭营养保鲜冰吧', 'DS0157DK', 4999, '1599115250383rq-ds.png', 8, 3),
(19, '欧式三速电子触摸按键吸油烟机', 'CXW-200-E800C6J', 2599, '1599115339959rq-cxw.png', 6, 3),
(20, '407升风冷变频多门冰箱', 'BCD-407WDECU1', 4799, '1599115415341yz-bcd.png', 2, 4),
(21, '13升零冷水燃气热水器', 'JSQ25-13WN3S(12T)', 4599, '1599115481953yz-jsq.png', 4, 4),
(22, '海尔50英寸智能电视', '50T86', 3998, '1599115556223yz-50.png', 5, 4),
(23, '8公斤紫水晶滚筒洗烘一体机', 'XQG80-HB14876LU1', 7299, '1599115621629yz-xqg.png', 3, 4),
(24, '灵动系列二速电子触摸按键吸油烟机', 'CXW-219-E900C10', 3099, '1599115676405zh-cxw.png', 6, 4),
(25, '431升风冷变频十字对开门冰箱', 'BCD-431WDCFU1', 8799, '1599115749043hkj-bcd.png', 2, 5),
(26, '40升5000W瞬热洗横式电热水器', 'ES40H-SMART5(U1)', 5499, '1599115914183hkj-es.png', 4, 5),
(27, '海尔65英寸智能电视', 'LU65C51', 3599, '1599115979653hkj-lu.png', 5, 5),
(28, '10公斤水晶滚筒洗烘一体机', 'EG10014HBD959GU1', 6999, '1599116037967hkj-eg.png', 3, 5),
(29, '自净芯系列三速电子-触摸按键吸油烟机', 'CXW-219-E900C13', 4399, '1599116099415hkj-cxw.png', 6, 5),
(30, '462升风冷变频十字对开门冰箱', 'BCD-462WDCI', 5599, '1599116177143ct-bcd.png', 2, 6),
(31, '10公斤纤合滚筒洗衣机', 'FAW10986LSU1', 5299, '1599116238915ct-faw.png', 3, 6),
(32, '自净芯三速电子-触摸按键吸油烟机', 'CXW-219-T3J07', 3299, '1599116296638ct-cxw.png', 6, 6),
(33, '12升水气双调燃气热水器', 'JSQ22-12J(12T)', 2799, '1599116357937ct-jsq.png', 4, 6),
(34, 'TAB-QS60S扫地机器人', 'TAB-QS60S', 2299, '1599116420461ct-tab.png', 8, 6),
(35, '477升风冷变频十字对开门冰箱', 'BCD-477WDPCU1', 3799, '1599121967391bingxiang1.png', 2, 7),
(36, '520升风冷变频对开门冰箱', 'BCD-520WDPD', 4099, '1599122031607bingxiang2.png', 2, 7),
(37, '223升风冷变频三门冰箱', 'BCD-223WDPT', 2519, '1599122076291bingxiang3.png', 2, 7),
(38, '138升冷藏冷冻转换柜', 'BD-138W', 3699, '1599122126656bingxiang4.png', 2, 7),
(39, '545升风冷变频十字对开门冰箱', 'BCD-545WFPB', 4299, '1599122167047bingxiang5.png', 2, 7),
(40, '216升直冷定频三门冰箱', 'BCD-216STPT', 1299, '1599122206895bingxiang6.png', 2, 7),
(41, '446升风冷变频多门冰箱', 'BCD-446WBCK', 4999, '1599122255942bingxiang7.png', 2, 7),
(42, '220升风冷定频两门冰箱', 'BCD-220WMGR', 2099, '1599122302976bingxiang8.png', 2, 7),
(43, '213升风冷定频三门冰箱', 'BCD-213WMPS', 1649, '1599122351326bingxiang9.png', 2, 7),
(44, '510升风冷变频对开门冰箱', 'BCD-510WDEM', 2749, '1599122391146bingxiang10.png', 2, 7),
(45, '405升风冷变频十字对开门冰箱', 'BCD-405WDSKU1', 4259, '1599122475304bingxiang11.png', 2, 7),
(46, '500升风冷变频十字对开门冰箱', 'BCD-500WDSKU1', 4959, '1599122519348bingxiang12.png', 2, 7),
(47, '505升风冷变频多门冰箱', 'BCD-505WDCNU1', 7999, '1599122557286bingxiang13.png', 2, 7),
(48, '9公斤变频滚筒洗烘一体机', 'XQG90-14HB30SU1JD', 4199, '1599122657543xiyiji1.png', 3, 7),
(49, '10公斤变频滚筒洗衣机', 'EG10012B509G', 2999, '1599122710007xiyiji2.png', 3, 7),
(50, '10公斤水晶变频洗烘一体机', 'EG10014HBD979U1', 6999, '1599122758914xiyiji3.png', 3, 7),
(51, '11公斤免清洗波轮洗衣机', 'MW110-BZ996U1', 5999, '1599122805985xiyiji4.png', 3, 7),
(52, '热泵式滚筒干衣机', 'GBN100-636', 5499, '1599122846321xiyiji5.png', 3, 7),
(53, '10公斤全自动波轮洗衣机', 'EB100Z139', 1599, '1599122891439xiyiji6.png', 3, 7),
(54, '10公斤双动力波轮洗衣机', 'EMS100BZ199U1', 3999, '1599122934309xiyiji7.png', 3, 7),
(55, '10公斤变频滚筒洗衣机', 'EG100B139S', 2999, '1599122979063xiyiji8.png', 3, 7),
(56, '10公斤变频滚筒洗烘一体机', 'EG100HB139S', 3999, '1599123024572xiyiji9.png', 3, 7),
(57, '8公斤全自动波轮洗衣机', 'EB80Z119', 999, '1599123071236xiyiji10.png', 3, 7),
(58, '3公斤壁挂滚筒洗衣机', 'XQGM30-BX701MYGU1', 2999, '1599123116858xiyiji11.png', 3, 7),
(59, '10公斤变频滚筒洗烘一体机', 'EG100HB129G', 2999, '1599123154967xiyiji12.png', 3, 7),
(60, '10公斤变频滚筒洗烘一体机', 'G100108HB12G', 2699, '1599123197125xiyiji13.png', 3, 7),
(61, '先行者 2匹柜式定频空调', 'KFR-50LW/08EDS33', 5599, '1599123248512kongtiao1.png', 1, 7),
(62, '致樽 3匹柜式变频空调', 'KFR-72LW/07UDP21AU1', 10999, '1599123320001kongtiao2.png', 1, 7),
(63, '节能风 1.5匹壁挂式变频空调', 'KFR-35GW/03JDM81A', 2699, '1599123379902kongtiao3.png', 1, 7),
(64, '劲铂 1.5匹壁挂式变频空调', 'KFR-35GW/81@AU1-Da', 2599, '1599123424643kongtiao4.png', 1, 7),
(65, '京喜 1.5匹壁挂式变频空调', 'HAS3503JDA(81)AU1', 3699, '1599123485736kongtiao5.png', 1, 7),
(66, '先行者 3匹柜式变频空调', 'KFR-72LW/07EDS83', 5599, '1599123538024kongtiao6.png', 1, 7),
(67, '先行者 2匹柜式变频空调', 'KFR-50LW/07EDS81U1', 5999, '1599123598133kongtiao7.png', 1, 7),
(68, '先行者 3匹柜式变频空调', 'KFR-72LW/07EDS81U1', 6999, '1599123642989kongtiao8.png', 1, 7),
(69, '净界 1.5匹壁挂式变频空调', 'KFR-35GW/07RCA81AU1', 4299, '1599123681810kongtiao9.png', 1, 7),
(70, '净界 1匹壁挂式变频空调', 'KFR-26GW/07RCA81AU1', 3999, '1599123727243kongtiao10.png', 1, 7),
(71, '节能风 1匹壁挂式变频空调', 'KFR-26GW/03JDM81A', 3199, '1599123776987kongtiao11.png', 1, 7),
(72, '星悦 大1匹壁挂式变频空调', 'KFR-26GW/03MYA83A', 3099, '1599123817499kongtiao12.png', 1, 7),
(73, '节能风 1.5匹壁挂式变频空调', 'KFR-35GW/03JDM81A', 3699, '1599123865133kongtiao13.png', 1, 7),
(74, '60升3D速热横式电热水器', 'EC6005-JX', 2799, '1599138762023reshuiqi1.png', 4, 7),
(75, '80升变频速热横式电热水器', 'EC8002-Q6(SJ)', 1399, '1599138814666reshuiqi2.png', 4, 7),
(76, '16升多频恒温燃气热水器', 'JSQ31-16JM6(12T)U1', 2549, '1599138868602reshuiqi3.png', 4, 7),
(77, '16升变频恒温燃气热水器', 'JSQ30-16J(12T)', 3299, '1599138919616reshuiqi4.png', 4, 7),
(78, '13升水气双调燃气热水器', 'JSQ25-13J(12T)', 2899, '1599138965596reshuiqi5.png', 4, 7),
(79, '10升水气双调燃气热水器', 'JSQ20-10J(12T)', 2499, '1599139023397reshuiqi6.png', 4, 7),
(80, '12升水气双调燃气热水器', 'JSQ22-12J(12T)', 2799, '1599139064220reshuiqi7.png', 4, 7),
(81, '13升水气双调燃气热水器', 'JSQ25-13GV3BD', 2199, '1599139123770reshuiqi8.png', 4, 7),
(82, '16升多频恒温燃气热水器', 'JSQ31-16JM6(12T)U1', 2549, '1599139190329reshuiqi9.png', 4, 7),
(83, '26kW高抗风压采暖炉', 'L1PB26-HT1(T)', 3799, '1599139231810reshuiqi10.png', 4, 7),
(84, '16升零冷水燃气热水器', 'JSQ31-16WJS2(12T)', 4799, '1599139293225reshuiqi11.png', 4, 7),
(85, '13升富氧蓝焰燃气热水器', 'JSQ25-13ZD2(12T)', 1999, '1599139339258reshuiqi12.png', 4, 7),
(86, '13升零冷水燃气热水器', 'JSQ25-13WN3S(12T)', 4599, '1599139382203reshuiqi13.png', 4, 7),
(87, '海尔75英寸智能电视', 'LU75C51', 7999, '1599139524836dianshi1.png', 5, 7),
(88, '海尔65英寸高清智能电视', 'LU65D31J', 2899, '1599139581389dianshi2.png', 5, 7),
(89, '海尔55英寸高清智能电视', 'LU55D31J', 2199, '1599139621604dianshi3.png', 5, 7),
(90, '海尔55英寸高清智能电视', 'LU55C71', 2999, '1599139666812dianshi4.png', 5, 7),
(91, '海尔55英寸高清智能电视', '55R3', 2199, '1599139709151dianshi5.png', 5, 7),
(92, '海尔58英寸高清智能电视', 'LU58J51', 2699, '1599139746840dianshi6.png', 5, 7),
(93, '海尔55英寸智能电视', 'LU55C31', 2299, '1599139786420dianshi7.png', 5, 7),
(94, '海尔55英寸智能电视', 'LU55C61', 2199, '1599139824647dianshi8.png', 5, 7),
(95, '海尔55英寸高清智能电视', '55V31', 2999, '1599139870790dianshi9.png', 5, 7),
(96, '海尔65英寸智能电视', 'LU65C61', 1899, '1599139917877dianshi10.png', 5, 7),
(97, '海尔55英寸智能电视', '55V51', 4499, '1599139963893dianshi11.png', 5, 7),
(98, '海尔55英寸智能电视', '55V52', 3999, '1599140004420dianshi12.png', 5, 7),
(99, '海尔49英寸智能电视', 'LE49H610G', 3598, '1599140062750dianshi13.png', 5, 7),
(100, '近吸式三速电子触摸按键吸油烟机', 'CXW-200-E900T6(J)', 3199, '1599140117678youyanji1.png', 6, 7),
(101, '欧式二速电子-触摸按键吸油烟机', 'CXW-219-T2903UD', 4199, '1599141024320youyanji2.png', 6, 7),
(102, '自净芯三速电子-触摸按键吸油烟机', 'CXW-219-T3J07', 3299, '1599141070367youyanji3.png', 6, 7),
(103, '侧吸式二速机械按键吸油烟机', 'CXW-200-E900C2S', 1199, '1599141116669youyanji4.png', 6, 7),
(104, '侧吸式二速电子-触摸按键吸油烟机', 'CXW-219-C3901', 4299, '1599141158227youyanji5.png', 6, 7),
(105, '侧吸式二速电子-触摸按键吸油烟机', 'CXW-200-EC202', 2399, '1599141205962youyanji6.png', 6, 7),
(106, '黄金旋吸三速电子-触摸按键吸油烟机', 'CXW-219-EC306', 4199, '1599141248042youyanji7.png', 6, 7),
(107, '近吸式三速电子-触摸按键吸油烟机', 'CXW-200-C395', 5199, '1599141292168youyanji8.png', 6, 7),
(108, '灵动三速电子-触摸按键吸油烟机', 'CXW-219-MA1C3', 3299, '1599141346161youyanji9.png', 6, 7),
(109, '自净芯系列三速电子-触摸按键吸油烟机', 'CXW-219-EC302', 3299, '1599141393745youyanji10.png', 6, 7),
(110, '自净芯系列三速电子-触摸按键吸油烟机', 'CXW-219-E900C13', 2599, '1599141435269youyanji11.png', 6, 7),
(111, '自净芯三速电子-触摸按键吸油烟机', 'CXW-219-C3J05', 3099, '1599141485083youyanji12.png', 6, 7),
(112, '欧式二速电子-触摸按键吸油烟机', 'CXW-219-T2902U9', 3299, '1599141533647youyanji13.png', 6, 7),
(113, '4.2kW台嵌两用钢化玻璃燃气灶', 'JZT-QE5B0(12T)', 699, '1599141624113ranqizao1.png', 7, 7),
(114, '4.5kW台嵌两用钢化玻璃燃气灶', 'JZT-QHA93C(12T)', 2599, '1599141717323ranqizao2.png', 7, 7),
(115, '4.1kW嵌入式熄火保护燃气灶', 'JZT-QE5B1(12T)', 1399, '1599141757404ranqizao3.png', 7, 7),
(116, '4.5kW台嵌两用钢化玻璃燃气灶', 'JZT-Q6BE(12T)', 1099, '1599141801322ranqizao4.png', 7, 7),
(117, '4.5kW台嵌两用钢化玻璃燃气灶', 'JZT-QE9B1(12T)', 1699, '1599141840125ranqizao5.png', 7, 7),
(118, '4.2kW台嵌两用钢化玻璃燃气灶', 'JZT-QE5B0(12T)', 699, '1599141884184ranqizao6.png', 7, 7),
(119, '4.5kW台嵌两用钢化玻璃燃气灶', 'JZT-QHA93C(12T)', 2599, '1599141930745ranqizao7.png', 7, 7),
(120, '4.2kW台嵌两用钢化玻璃燃气灶', 'JZT-QE3B5(12T)', 1599, '1599141971673ranqizao8.png', 7, 7),
(121, '4.1kW嵌入式钢化玻璃燃气灶', 'JZT-QE535(12T)', 1399, '1599142012154ranqizao9.png', 7, 7),
(122, '3.8kW嵌入式钢化玻璃燃气灶', 'JZY-QE3B(20Y)', 99, '1599142052031ranqizao10.png', 7, 7),
(123, '4.1kW嵌入式熄火保护燃气灶', 'JZT-QE5B1(12T)', 1399, '1599142135206ranqizao11.png', 7, 7),
(124, '4.0kW嵌入式熄火保护燃气灶', 'JZY-QE5B1(20Y)', 1399, '1599142176385ranqizao12.png', 7, 7),
(125, '4.0kW嵌入式钢化玻璃燃气灶', 'JZY-QE636B(20Y)', 799, '1599142230192ranqizao13.png', 7, 7);

-- --------------------------------------------------------

--
-- 表的结构 `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  `title` varchar(20) NOT NULL,
  `items` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `summary`
--

CREATE TABLE IF NOT EXISTS `summary` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 转存表中的数据 `summary`
--

INSERT INTO `summary` (`id`, `title`) VALUES
(1, '智慧家电'),
(2, '健康抑菌'),
(3, '人气爆款'),
(4, '颜值控'),
(5, '黑科技'),
(6, '成套臻选'),
(7, '');

-- --------------------------------------------------------

--
-- 表的结构 `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uid` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `token`
--

INSERT INTO `token` (`id`, `token`, `time`, `uid`) VALUES
(1, 'a95ce0c8673a432f38c118e39bc741bd', '2020-09-02 13:37:50', 1);

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `loginName` varchar(20) NOT NULL COMMENT '登录名',
  `loginKey` varchar(20) NOT NULL COMMENT '密码',
  `phoneNum` int(30) NOT NULL COMMENT '手机号',
  `email` varchar(300) NOT NULL COMMENT '邮箱',
  `roles` varchar(10) NOT NULL COMMENT '角色',
  `avatar` varchar(100) NOT NULL COMMENT '头像',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '审核状态',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `name`, `loginName`, `loginKey`, `phoneNum`, `email`, `roles`, `avatar`, `status`, `time`) VALUES
(1, '123', 'admin', '111111', 23, '2314580508@qq.com', 'admin', '', 1, '2020-09-02 13:26:11');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
